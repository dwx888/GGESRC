// dllmain.cpp : 定义 DLL 应用程序的入口点。
//#include "stdafx.h"
#include "Luaclient.h"
#include "Luaserver.h"
#include "Luaagent.h"


#pragma comment(lib, "../lib/lua51")
#pragma comment(lib, "../lib/HPSocket_static")

extern "C" __declspec(dllexport)
	int luaopen_luahp_client(lua_State* L) 
{
	REG_HPClient(L);
	return 1;
}
extern "C" __declspec(dllexport)
	int luaopen_luahp_server(lua_State* L) 
{
	REG_HPServer(L);
	return 1;
}
extern "C" __declspec(dllexport)
	int luaopen_luahp_agent(lua_State* L) 
{
	REG_HPAgent(L);
	return 1;
}
