#include "common.h"

#include "LuaServer.h"
#include "LuaClient.h"
#include "LuaAgent.h"
#include "thread.h"

#include <conio.h>
extern "C" {
#include "lua_zlib.c"
#include "lbase64.c"
#include "struct.c"
#include "serialize.c"
}

lua_State *			L	= NULL ;
int					g_ref	= 0;
CRITICAL_SECTION	G_CS;//许可

BOOL G_LOOP = FALSE;


int gge_preload(lua_State *L, lua_CFunction f, const char *name)
{
	lua_getglobal(L, "package");
	lua_getfield(L, -1, "preload");
	lua_pushcfunction(L, f);
	lua_setfield(L, -2, name);
	lua_pop(L, 2);
	return 0;
}

BOOL WINAPI HandlerRoutine( DWORD dwCtrlType ){//控制台消息
	if( CTRL_CLOSE_EVENT == dwCtrlType || CTRL_SHUTDOWN_EVENT == dwCtrlType){
		EnterCriticalSection(&G_CS);
		lua_rawgeti(L, LUA_REGISTRYINDEX, g_ref);
		lua_pushstring(L,"close");
		lua_call(L,1,0);
		LeaveCriticalSection(&G_CS);
		G_LOOP = FALSE;

		return TRUE;
	}else if (CTRL_C_EVENT == dwCtrlType)
	{
		return TRUE;
	}
	return FALSE;
}
int Start(lua_State *L)
{
	g_ref = luaL_ref(L, LUA_REGISTRYINDEX);//全局回调
	G_LOOP = TRUE;
	return 0;
}
int main(int argc, char *argv[]) 
{ 
	setvbuf(stdout, NULL,_IONBF,0);  
	SetConsoleCtrlHandler( HandlerRoutine, TRUE );
	L = ELuna::openLua();
	gge_preload(L,REG_HPServer,"gge_hpserver");
	gge_preload(L,REG_HPClient,"gge_hpclient");
	gge_preload(L,REG_HPAgent,"gge_hpagent");
	gge_preload(L,REG_THREAD,"gge_thread");

	gge_preload(L,luaopen_zlib,"zlib");				//zlib
	gge_preload(L,luaopen_base64,"base64");			//base64
	gge_preload(L,luaopen_struct,"struct");			//struct(string.pack)
	gge_preload(L,luaopen_serialize,"serialize");

	InitializeCriticalSection(&G_CS);
	EnterCriticalSection(&G_CS);
	lua_newtable(L);
	//版本
	lua_pushstring(L,GGESVR_VERSION);
	lua_setfield(L,-2,"version");
	//许可证
	lua_pushlightuserdata(L,&G_CS);
	lua_setfield(L,-2,"cs");
	//启动函数
	lua_pushcfunction(L,Start);
	lua_setfield(L,-2,"start");
	//参数 命令行
	lua_pushstring(L,"command");
	lua_newtable(L);

	for (int i=1;i<argc;i++){
		lua_pushinteger(L,i);
		lua_pushstring(L,argv[i]);
		lua_settable(L,-3);
	}
	lua_settable(L,-3);
	lua_setglobal(L,"__gge");
 	luaL_loadbuffer(L,(char *)luaJIT_BC_main,luaJIT_BC_main_SIZE,"GGESVR_MAIN");
	//luaL_loadfile(L,"I:/GGELUA2018/GGENET/main/main.lua");
	lua_call(L,0,0);
	LeaveCriticalSection(&G_CS);
	
	while(G_LOOP){
		EnterCriticalSection(&G_CS);
		if (_kbhit())
		{
			lua_rawgeti(L, LUA_REGISTRYINDEX, g_ref);
			lua_pushinteger(L,_getche());
			lua_call(L,1,1);
			G_LOOP=!lua_toboolean(L,-1);
			lua_pop(L,1);
		}else{
			lua_rawgeti(L, LUA_REGISTRYINDEX, g_ref);
			lua_call(L,0,1);
			G_LOOP=!lua_toboolean(L,-1);
			lua_pop(L,1);
		}
		LeaveCriticalSection(&G_CS);
		Sleep(10);
	}

	ELuna::closeLua(L);
	return 0; 
} 
