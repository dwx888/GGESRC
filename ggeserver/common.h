#pragma once
//#define _WINSOCKAPI_
//#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <windows.h>

#include "ELuna.h"
#include "LuaMian.h"

extern lua_State *		L ;
extern CRITICAL_SECTION G_CS;//����
//extern int				g_ref;

extern "C"{
	int luaopen_zlib(lua_State * L);
	int luaopen_base64(lua_State * L);
	int luaopen_struct(lua_State * L);
	int luaopen_serialize(lua_State * L);
}
int gge_preload(lua_State *L, lua_CFunction f, const char *name);

#define GGESVR_VERSION "1.0b4"
