#include "thread.h"

LuaThread::LuaThread(const char* MainPath, const char* PackPath):m_hThread(NULL),m_list(NULL),m_sleep(10){
	m_ref = luaL_ref(L, LUA_REGISTRYINDEX);
	m_L = ELuna::openLua();
	gge_preload(m_L,REG_HPServer,"gge_hpserver");
	gge_preload(m_L,REG_HPClient,"gge_hpclient");
	gge_preload(m_L,REG_HPAgent,"gge_hpagent");

	gge_preload(m_L,luaopen_zlib,"zlib");				//zlib
	gge_preload(m_L,luaopen_base64,"base64");			//base64
	gge_preload(m_L,luaopen_struct,"struct");			//struct(string.pack)
	gge_preload(m_L,luaopen_serialize,"serialize");
	InitializeCriticalSection(&m_cs);
	EnterCriticalSection(&m_cs);
	lua_newtable(m_L);
	//线程许可
	lua_pushlightuserdata(m_L,&m_cs);
	lua_setfield(m_L,-2,"cs");
	lua_setglobal(m_L,"__gge");
	luaL_loadbuffer(m_L,(char *)luaJIT_BC_main,luaJIT_BC_main_SIZE,"GGESVR_MAIN");
	//luaL_loadfile(m_L,"I:/GGELUA2018/GGENET/main/main.lua");
	lua_pushboolean(m_L,1);//isthread
	lua_pushstring(m_L,MainPath);//main
	lua_pushstring(m_L,PackPath);//main
	lua_pushlightuserdata(m_L,this);
	lua_pushcclosure(m_L,Return,1);//线程返回
	lua_call(m_L,4,1);
	m_tref = luaL_ref(m_L, LUA_REGISTRYINDEX);//线程回调
	LeaveCriticalSection(&m_cs);
}

LuaThread::~LuaThread(void){
	if (m_hThread)
	{
		m_sleep = -1;
		WaitForSingleObject(m_hThread, INFINITE);
		CloseHandle(m_hThread);
	}
	luaL_unref(L,LUA_REGISTRYINDEX,m_ref);
	lua_close(m_L);
	DeleteCriticalSection(&m_cs);
	if (m_list)
		delete []m_list;
}

DWORD WINAPI LuaThread::ThreadProc(void *pParam){
	LuaThread* t = (LuaThread*)pParam;
	PCRITICAL_SECTION m_cs = &t->m_cs;
	lua_State *m_L  = t->m_L;
	int m_tref = t->m_tref;
	tqueue* m_list = t->m_list;
	int m_maxlist  = t->m_maxlist;
	while(true)
	{
		EnterCriticalSection(m_cs);
		for (int i=0;i<m_maxlist;i++)
		{
			if (m_list[i].ok)
			{
				lua_rawgeti(m_L,LUA_REGISTRYINDEX, m_tref);//消息
				lua_pushstring(m_L,"__tMessage");
				lua_pushlstring(m_L,m_list[i].str.c_str(),m_list[i].str.length());
				lua_pushlightuserdata(m_L,m_list[i].ptr);
				lua_call(m_L,3,0);
				m_list[i].ok = false;
			}
			if (t->m_sleep < 0)break;
		}
		lua_rawgeti(m_L,LUA_REGISTRYINDEX, m_tref);//循环
		lua_call(m_L,0,0);
		LeaveCriticalSection(m_cs);
		if (t->m_sleep < 0){
			break;
		} else
			Sleep(t->m_sleep);
	}
	return 0;
}
bool LuaThread::Start(int v){
	if (!m_hThread)
	{
		m_maxlist = v;
		m_list = new tqueue[v];
		for (int i=0;i<m_maxlist;i++)
			m_list[i].ok=false;
		DWORD dwThread;
		m_hThread = CreateThread(NULL,0,ThreadProc,(LPVOID)this,0,&dwThread);
		//EnterCriticalSection(&m_cs);
		//lua_rawgeti(m_L,LUA_REGISTRYINDEX, m_tref);
		//lua_pushstring(m_L,"__tMessage");
		//lua_pushstring(m_L,"'启动'");
		//lua_call(m_L,2,0);
		//LeaveCriticalSection(&m_cs);
		return !dwThread;
	}
	return false;
}
void LuaThread::Stop(){
	if (m_hThread)
	{
		m_sleep = -1;
		WaitForSingleObject(m_hThread, INFINITE);
		CloseHandle(m_hThread);
		delete []m_list;
		m_hThread = NULL;
		m_list = NULL;
		//EnterCriticalSection(&m_cs);
		//lua_rawgeti(m_L,LUA_REGISTRYINDEX, m_tref);
		//lua_pushstring(m_L,"__tMessage");
		//lua_pushstring(m_L,"'停止'");
		//lua_call(m_L,2,0);
		//LeaveCriticalSection(&m_cs);
	}
}
bool LuaThread::Send(string v,void* p){
	if (m_hThread)
	{
		for (int i=0;i<m_maxlist;i++)
		{
			if (!m_list[i].ok)
			{
				m_list[i].str = v;
				m_list[i].ptr = p;
				m_list[i].ok = true;
				return true;
			}
		}
	}
	return false;
}
int LuaThread::Return(lua_State *Ls){//返回消息
	size_t len;
	const char* str;
	int argc = lua_gettop(Ls);
	LuaThread* t = (LuaThread*)lua_touserdata(Ls,lua_upvalueindex(1));
	EnterCriticalSection(&G_CS);
	lua_rawgeti(L,LUA_REGISTRYINDEX, t->m_ref);
	lua_getfield(L,-1,"OnMessage");
	lua_pushvalue(L,-2);//self
	for (int i=1;i<=argc;i++)
	{
		switch (lua_type(Ls,i))
		{
		case LUA_TBOOLEAN:
			lua_pushboolean(L,lua_toboolean(Ls,i));break;
		case LUA_TNUMBER:
			lua_pushnumber(L,lua_tonumber(Ls,i));break;
		case LUA_TSTRING:
			str=lua_tolstring(Ls,i,&len);
			lua_pushlstring(L,str,len);break;
		case LUA_TLIGHTUSERDATA:
			lua_pushlightuserdata(L,(void*)lua_topointer(Ls,i));break;
		default: 
			lua_pushnil(L);break;
		}
	}
		lua_call(L,argc+1,-1);//self+...
	//-----------------------------------------------------------
	argc = lua_gettop(L);
	for (int i=2;i<=argc;i++)//第1是前面push的self
	{
		switch (lua_type(L,i))
		{
		case LUA_TBOOLEAN:
			lua_pushboolean(Ls,lua_toboolean(L,i));break;
		case LUA_TNUMBER:
			lua_pushnumber(Ls,lua_tonumber(L,i));break;
		case LUA_TSTRING:
			str=lua_tolstring(L,i,&len);
			lua_pushlstring(Ls,str,len);break;
		case LUA_TLIGHTUSERDATA:
			lua_pushlightuserdata(Ls,(void*)lua_topointer(L,i));break;
		default: 
			lua_pushnil(Ls);break;
		}
	}
		lua_pop(L,argc);//弹掉self和主返回
	LeaveCriticalSection(&G_CS);
		return argc-1;//实际返回self-1
}
int REG_THREAD(lua_State * L){
	const char* gname = "__GGEThread";
	ELuna::registerClass<LuaThread>(L, gname, ELuna::constructor<LuaThread,const char *,const char *>);

	ELuna::registerMethod<LuaThread>(L, "Start", &LuaThread::Start);
	ELuna::registerMethod<LuaThread>(L, "Stop", &LuaThread::Stop);
	ELuna::registerMethod<LuaThread>(L, "Send", &LuaThread::Send);
	ELuna::registerMethod<LuaThread>(L, "SetSleep", &LuaThread::SetSleep);


	return 1;
}