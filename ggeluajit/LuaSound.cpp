#include "LuaSound.h"


LuaSound::~LuaSound(){
	GGE_RELEASE(m_snd);
}
bool LuaSound::Sound_LoadFile(const char *filename){
	m_snd = gge::Sound_Load(filename);
	return m_snd!=NULL;
}
bool LuaSound::Sound_LoadPtr(UINT ptr, UINT size){
	m_snd = gge::Sound_Load((char*)ptr,size);
	return m_snd!=NULL;
}
bool LuaSound::Sound_LoadStr(LuaString str){
	m_snd = gge::Sound_Load(str.str,str.len);
	return m_snd!=NULL;
}
void LuaSound::Play(){
	if(m_snd)
		m_snd->Play();
}
void LuaSound::Pause() { 
	if(m_snd)
		m_snd->Pause();
}
void LuaSound::Resume() { 
	if(m_snd)
		m_snd->Resume();
}
void LuaSound::Stop(){ 
	if(m_snd)
		m_snd->Stop();
}
bool LuaSound::IsPlaying(){
	if(m_snd)
		return m_snd->IsPlaying();
	return false;
}
float LuaSound::GetDuration(){
	if(m_snd)
		return m_snd->GetDuration();
	return 0;
}
void LuaSound::SetLoop(bool loop){
	if (m_snd)
		m_snd->SetLoop(loop);
}
bool LuaSound::IsLoop(){
	if(m_snd)
		return m_snd->IsLoop();
	return false;
}

void LuaSound::SetVolume(float v) { 
	if(m_snd)
		m_snd->SetVolume(v);
}
float LuaSound::GetVolume() { 
	if(m_snd)
		return m_snd->GetVolume();
	return 0;
}
void LuaSound::SetPitch(float v) { 
	if(m_snd)
		m_snd->SetPitch(v);
}
float LuaSound::GetPitch() { 
	if(m_snd)
		return m_snd->GetPitch();
	return 0;
}
void LuaSound::SetRelativeToListener(bool bRelative) { 
	if(m_snd)
		m_snd->SetRelativeToListener(bRelative);
}
bool LuaSound::IsRelativeToListener(){
	if(m_snd)
		return m_snd->IsRelativeToListener();
	return false;
}
void LuaSound::SetPosition(float x,float y,float z) { 
	if(m_snd)
		m_snd->SetPosition(ggeVector3(x,y,z));
}
float LuaSound::GetPosition(lua_State* L) { 
	if (m_snd)
	{
		ggeVector3 r = m_snd->GetPosition();
		lua_pushnumber(L,r.x);
		lua_pushnumber(L,r.y);
		return r.z;
	}
	return 0;
}
void LuaSound::SetVelocity(float x,float y,float z) { 
	if(m_snd)
		m_snd->SetVelocity(ggeVector3(x,y,z));
}
float LuaSound::GetVelocity(lua_State* L) { 
	if (m_snd)
	{
		ggeVector3 r = m_snd->GetVelocity();
		lua_pushnumber(L,r.x);
		lua_pushnumber(L,r.y);
		return r.z;
	}
	return 0;
}
void LuaSound::SetDirection(float x,float y,float z) { 
	if(m_snd)
		m_snd->SetDirection(ggeVector3(x,y,z));
}
float LuaSound::GetDirection(lua_State* L) { 
	if (m_snd)
	{
		ggeVector3 r = m_snd->GetDirection();
		lua_pushnumber(L,r.x);
		lua_pushnumber(L,r.y);
		return r.z;
	}
	return 0;
}
void LuaSound::SetCone(float innerAngle, float outerAngle, float outerVolume) {
	if(m_snd)
		m_snd->SetCone(innerAngle,outerAngle,outerVolume);
}
float LuaSound::GetCone(lua_State* L) {
	float innerAngle,outerAngle,outerVolume;
	if(m_snd)
		m_snd->GetCone(innerAngle,outerAngle,outerVolume);
	lua_pushnumber(L,innerAngle);
	lua_pushnumber(L,outerAngle);
	return outerVolume;
}
void LuaSound::SetMinDistance(float distance) {
	if(m_snd)
		m_snd->SetMinDistance(distance);
}
float LuaSound::GetMinDistance(){ 
	if(m_snd)
		return m_snd->GetMinDistance();
	return 0;
}
void LuaSound::SetMaxDistance(float distance) {
	if(m_snd)
		m_snd->SetMaxDistance(distance);
}
float LuaSound::GetMaxDistance(){ 
	if(m_snd)
		return m_snd->GetMaxDistance();
	return 0;
}
void LuaSound::SetRolloffFactor(float factor) {
	if(m_snd)
		m_snd->SetRolloffFactor(factor);
}
float LuaSound::GetRolloffFactor(){ 
	if(m_snd)
		return m_snd->GetRolloffFactor();
	return 0;
}
//================================================
int LuaSound::GetRefCount(){ 
	if (m_snd)
		return m_snd->GetRefCount();
	return 0;
}
void LuaSound::AddRef(){
	if(m_snd)
		m_snd->AddRef();
}
bool LuaSound::Release(){
	if (m_snd)
	{
		if (m_snd->GetRefCount()==1){
			m_snd->Release();
			m_snd = NULL;
			return true;
		}
		m_snd->Release();
	}

	return false;
}
int REG_GGE_Sound(lua_State * L){
	const char* gname = "__ggeSound";
	ELuna::registerClass<LuaSound>(L, gname,  ELuna::constructor<LuaSound>);
	ELuna::registerMethod<LuaSound>(L, "Sound_LoadFile",		&LuaSound::Sound_LoadFile);
	ELuna::registerMethod<LuaSound>(L, "Sound_LoadPtr",			&LuaSound::Sound_LoadPtr);
	ELuna::registerMethod<LuaSound>(L, "Sound_LoadStr",			&LuaSound::Sound_LoadStr);
	ELuna::registerMethod<LuaSound>(L, "Play",					&LuaSound::Play);
	ELuna::registerMethod<LuaSound>(L, "Pause",					&LuaSound::Pause);
	ELuna::registerMethod<LuaSound>(L, "Resume",				&LuaSound::Resume);
	ELuna::registerMethod<LuaSound>(L, "Stop",					&LuaSound::Stop);
	ELuna::registerMethod<LuaSound>(L, "IsPlaying",				&LuaSound::IsPlaying);
	ELuna::registerMethod<LuaSound>(L, "GetDuration",			&LuaSound::GetDuration);
	ELuna::registerMethod<LuaSound>(L, "SetLoop",				&LuaSound::SetLoop);
	ELuna::registerMethod<LuaSound>(L, "IsLoop",				&LuaSound::IsLoop);

	ELuna::registerMethod<LuaSound>(L, "SetVolume",				&LuaSound::SetVolume);
	ELuna::registerMethod<LuaSound>(L, "GetVolume",				&LuaSound::GetVolume);
	ELuna::registerMethod<LuaSound>(L, "SetPitch",				&LuaSound::SetPitch);
	ELuna::registerMethod<LuaSound>(L, "GetPitch",				&LuaSound::GetPitch);

	ELuna::registerMethod<LuaSound>(L, "SetRelativeToListener", &LuaSound::SetRelativeToListener);
	ELuna::registerMethod<LuaSound>(L, "IsRelativeToListener",	&LuaSound::IsRelativeToListener);
	ELuna::registerMethod<LuaSound>(L, "SetPosition",			&LuaSound::SetPosition);
	ELuna::registerMethod<LuaSound>(L, "GetPosition",			&LuaSound::GetPosition,3);
	ELuna::registerMethod<LuaSound>(L, "SetVelocity",			&LuaSound::SetVelocity);
	ELuna::registerMethod<LuaSound>(L, "GetVelocity",			&LuaSound::GetVelocity,3);
	ELuna::registerMethod<LuaSound>(L, "SetDirection",			&LuaSound::SetDirection);
	ELuna::registerMethod<LuaSound>(L, "GetDirection",			&LuaSound::GetDirection,3);
	ELuna::registerMethod<LuaSound>(L, "SetCone",				&LuaSound::SetCone);
	ELuna::registerMethod<LuaSound>(L, "GetCone",				&LuaSound::GetCone,3);
	ELuna::registerMethod<LuaSound>(L, "SetMinDistance",		&LuaSound::SetMinDistance);
	ELuna::registerMethod<LuaSound>(L, "GetMinDistance",		&LuaSound::GetMinDistance);
	ELuna::registerMethod<LuaSound>(L, "SetMaxDistance",		&LuaSound::SetMaxDistance);
	ELuna::registerMethod<LuaSound>(L, "GetMaxDistance",		&LuaSound::GetMaxDistance);
	ELuna::registerMethod<LuaSound>(L, "SetRolloffFactor",		&LuaSound::SetRolloffFactor);
	ELuna::registerMethod<LuaSound>(L, "GetRolloffFactor",		&LuaSound::GetRolloffFactor);

	ELuna::registerMethod<LuaSound>(L, "GetRefCount",			&LuaSound::GetRefCount);
	ELuna::registerMethod<LuaSound>(L, "AddRef",				&LuaSound::AddRef);
	ELuna::registerMethod<LuaSound>(L, "Release",				&LuaSound::Release);
	ELuna::registerMethod<LuaSound>(L, "GetP",					&LuaSound::GetP);
	ELuna::registerMethod<LuaSound>(L, "SetP",					&LuaSound::SetP);

	return 1;
}