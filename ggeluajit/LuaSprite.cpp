#include "LuaSprite.h"


LuaSprite::LuaSprite(){
	m_Filter	= 0;
	m_AlphaAdd  = 0;
	m_rotation	= 0;
	m_hscale	= 1;
	m_vscale	= 1;
	m_stretchw	= 0;
	m_stretchh	= 0;
	m_spr		= NULL;
	m_x = 0.1f;m_y = 0.1f;
}
LuaSprite::~LuaSprite(){
	GGE_RELEASE(m_spr);
}
bool LuaSprite::Sprite_Create(void* texture, float x, float y, float width, float height){
	m_spr = gge::Sprite_Create((gge::ggeTexture *)texture, x, y, width, height);
	return m_spr!=NULL;
}
void LuaSprite::Copy(void* spr){
	if (m_spr)
		m_spr->Copy((gge::ggeSprite*)spr);
}
void* LuaSprite::Clone(){
	if (m_spr)
		return m_spr->Clone();
	return 0;
}
//-------------------------------------------------------------
void LuaSprite::Render(float x, float y) { 
	if (m_spr)
		m_spr->Render(x,y);
}
void LuaSprite::RenderEx(float x, float y, float rotation, float hscale, float vscale) { 
	if (m_spr)
		m_spr->RenderEx(x,y,rotation,hscale,vscale);
}
void LuaSprite::RenderStretch(float x1, float y1, float x2, float y2) {
	if (m_spr)
		m_spr->RenderStretch(x1,y1,x2,y2);
}
void LuaSprite::Render4V(float x0, float y0, float x1, float y1, float x2, float y2, float x3, float y3) {
	if (m_spr)
		m_spr->Render4V(x0,y0,x1,y1,x2,y2,x3,y3);
}
//-------------------------------------------------------------
void LuaSprite::SetPosition2(float x, float y) {
	if (m_spr){
		m_x = x;m_y = y;
		if (m_rotation || m_hscale!=1 || m_vscale!=1)
			m_spr->SetPositionEx(x,y,m_rotation,m_hscale,m_vscale);
		else if (m_stretchw || m_stretchh)
			m_spr->SetPositionStretch(x,y,x+m_stretchw,y+m_stretchh);
		else
			m_spr->SetPosition(x,y);
	}
}
void LuaSprite::SetPosition(float x, float y) {
	m_spr->SetPosition(x,y);
}
void LuaSprite::SetPositionEx(float x, float y, float rotation, float hscale, float vscale) {
	if (m_spr)
		m_spr->SetPositionEx(x,y,rotation,hscale,vscale);
}
void LuaSprite::SetPositionStretch(float x1, float y1, float x2, float y2) {
	if (m_spr)
		m_spr->SetPositionStretch(x1,y1,x2,y2);
}
void LuaSprite::SetPosition4V(float x0, float y0, float x1, float y1, float x2, float y2, float x3, float y3) {
	if (m_spr)
		m_spr->SetPosition4V(x0,y0,x1,y1,x2,y2,x3,y3);
}
void LuaSprite::RenderPosition() { 
	if (m_spr){
		m_spr->RenderPosition();
		if (m_AlphaAdd)
		{
			m_spr->SetBlendMode(m_spr->GetBlendMode()|gge::BLEND_ALPHAADD);
			m_spr->SetColor(m_AlphaAdd);
			m_spr->RenderPosition();
			m_spr->SetBlendMode(m_spr->GetBlendMode()^gge::BLEND_ALPHAADD);
			m_spr->SetColor(0xffffffff);
		}
	}
}
//-------------------------------------------------------------
void LuaSprite::SetTexture(void* texture) {
	if (m_spr)
		m_spr->SetTexture((gge::ggeTexture *)texture);
}
void* LuaSprite::GetTexture(){
	if (m_spr){
		gge::ggeTexture* tex = m_spr->GetTexture();
		if (tex)
			tex->AddRef();
		return tex;
	}
	return NULL;
}
void LuaSprite::SetTextureRect(float x, float y, float width, float height) {
	if (m_spr)
		m_spr->SetTextureRect(x,y,width,height);
}

void LuaSprite::SetColor(UINT color, int i) {
	if (m_spr)
		m_spr->SetColor(color,i);
}
void LuaSprite::SetZ(float z, int i) {
	if (m_spr)
		m_spr->SetZ(z,i);
}
void LuaSprite::SetBlendMode(int blend) {
	if (m_spr){
		m_spr->SetBlendMode(blend|m_Filter);
	}
}
void LuaSprite::SetHotSpot(float x, float y) {
	if (m_spr)
		m_spr->SetHotSpot(x,y);
}
void LuaSprite::SetFlip(bool bX, bool bY, bool bHotSpot) {
	if (m_spr)
		m_spr->SetFlip(bX,bY,bHotSpot);
}

float LuaSprite::GetTextureRect(lua_State *L){ 
	float x,y,width,height;
	m_spr->GetTextureRect(&x,&y,&width,&height);
	lua_pushnumber(L,x);
	lua_pushnumber(L,y);
	lua_pushnumber(L,width);
	return height;
}

UINT LuaSprite::GetColor(int i) {
	if (m_spr)
		return m_spr->GetColor(i);
	return 0;
}
float LuaSprite::GetZ(int i) {
	if (m_spr)
		return m_spr->GetZ(i);
	return 0;
}
int LuaSprite::GetBlendMode() {
	if (m_spr)
		return m_spr->GetBlendMode();
	return 0;
}
float LuaSprite::GetHotSpot(lua_State *L){ 
	float x,y;
	m_spr->GetHotSpot(&x,&y);
	lua_pushnumber(L,x);
	return y;
}
bool LuaSprite::GetFlip(lua_State *L){ 
	bool bX,bY;
	m_spr->GetFlip(&bX,&bY);
	lua_pushboolean(L,bX);
	return bY;
}
void LuaSprite::GetBoundingBox(void* rect){ 
	if (m_rotation || m_hscale!=1 || m_vscale!=1)
		m_spr->GetBoundingBoxEx((gge::ggeRect*)rect,m_x,m_y,m_rotation,m_hscale,m_vscale);
	else
		m_spr->GetBoundingBox((gge::ggeRect*)rect,m_x,m_y);
}
void LuaSprite::GetBoundingBoxEx(void* rect){ 
	m_spr->GetBoundingBoxEx((gge::ggeRect*)rect,m_x,m_y,m_rotation,m_hscale,m_vscale);
}
float LuaSprite::GetWidth() {
	if (m_spr)
		return m_spr->GetWidth();
	return 0;
}
float LuaSprite::GetHeight() {
	if (m_spr)
		return m_spr->GetHeight();
	return 0;
}
void* LuaSprite::GetQuad() {
	if (m_spr)
		return &m_spr->GetQuad();
	return 0;
}
//====================================================
int LuaSprite::GetRefCount(){ 
	if (m_spr)
		return m_spr->GetRefCount();
	return 0;
}
void LuaSprite::AddRef(){ 
	if (m_spr)
		m_spr->AddRef();
}
bool LuaSprite::Release(){ 
	if (m_spr)
	{
		if (m_spr->GetRefCount()==1){
			m_spr->Release();
			m_spr = NULL;
			return true;
		}
		m_spr->Release();
	}
	return false;
}
int REG_GGE_Sprite(lua_State *L){
	const char* gname = "__ggeSprite";
	ELuna::registerClass<LuaSprite>(L, gname, ELuna::constructor<LuaSprite>);
	ELuna::registerMethod<LuaSprite>(L, "Sprite_Create",		&LuaSprite::Sprite_Create);
	ELuna::registerMethod<LuaSprite>(L, "Sprite_Pointer",		&LuaSprite::Sprite_Pointer);

	//ELuna::registerMethod<LuaSprite>(L, "Copy",					&LuaSprite::Copy);
	ELuna::registerMethod<LuaSprite>(L, "Clone",				&LuaSprite::Clone);
	ELuna::registerMethod<LuaSprite>(L, "Render",				&LuaSprite::Render);
	ELuna::registerMethod<LuaSprite>(L, "RenderEx",				&LuaSprite::RenderEx);
	ELuna::registerMethod<LuaSprite>(L, "RenderStretch",		&LuaSprite::RenderStretch);
	ELuna::registerMethod<LuaSprite>(L, "Render4V",				&LuaSprite::Render4V);

	ELuna::registerMethod<LuaSprite>(L, "RenderPosition",		&LuaSprite::RenderPosition);
	ELuna::registerMethod<LuaSprite>(L, "SetPosition2",			&LuaSprite::SetPosition2);
	ELuna::registerMethod<LuaSprite>(L, "SetPosition",			&LuaSprite::SetPosition);
	ELuna::registerMethod<LuaSprite>(L, "SetPositionEx",		&LuaSprite::SetPositionEx);
	ELuna::registerMethod<LuaSprite>(L, "SetPositionStretch",	&LuaSprite::SetPositionStretch);
	ELuna::registerMethod<LuaSprite>(L, "SetPosition4V",		&LuaSprite::SetPosition4V);

	ELuna::registerMethod<LuaSprite>(L, "SetTexture",			&LuaSprite::SetTexture);
	ELuna::registerMethod<LuaSprite>(L, "SetTextureRect",		&LuaSprite::SetTextureRect);

	ELuna::registerMethod<LuaSprite>(L, "SetColor",				&LuaSprite::SetColor);
	ELuna::registerMethod<LuaSprite>(L, "SetZ",					&LuaSprite::SetZ);
	ELuna::registerMethod<LuaSprite>(L, "SetBlendMode",			&LuaSprite::SetBlendMode);
	ELuna::registerMethod<LuaSprite>(L, "SetHotSpot",			&LuaSprite::SetHotSpot);
	ELuna::registerMethod<LuaSprite>(L, "SetFlip",				&LuaSprite::SetFlip);
	
	ELuna::registerMethod<LuaSprite>(L, "GetTexture",			&LuaSprite::GetTexture);
	ELuna::registerMethod<LuaSprite>(L, "GetTextureRect",		&LuaSprite::GetTextureRect,4);

	ELuna::registerMethod<LuaSprite>(L, "GetColor",				&LuaSprite::GetColor);
	ELuna::registerMethod<LuaSprite>(L, "GetZ",					&LuaSprite::GetZ);
	ELuna::registerMethod<LuaSprite>(L, "GetBlendMode",			&LuaSprite::GetBlendMode);
	ELuna::registerMethod<LuaSprite>(L, "GetHotSpot",			&LuaSprite::GetHotSpot,2);
	ELuna::registerMethod<LuaSprite>(L, "GetFlip",				&LuaSprite::GetFlip,2);
	ELuna::registerMethod<LuaSprite>(L, "GetWidth",				&LuaSprite::GetWidth);
	ELuna::registerMethod<LuaSprite>(L, "GetHeight",			&LuaSprite::GetHeight);
	ELuna::registerMethod<LuaSprite>(L, "GetQuad",				&LuaSprite::GetQuad);
	ELuna::registerMethod<LuaSprite>(L, "GetBoundingBox",		&LuaSprite::GetBoundingBox);
	//ELuna::registerMethod<LuaSprite>(L, "GetBoundingBoxEx",		&LuaSprite::GetBoundingBoxEx);

	ELuna::registerMethod<LuaSprite>(L, "GetP",					&LuaSprite::GetP);
	ELuna::registerMethod<LuaSprite>(L, "SetP",					&LuaSprite::SetP);
	ELuna::registerMethod<LuaSprite>(L, "GetRefCount",			&LuaSprite::GetRefCount);
	ELuna::registerMethod<LuaSprite>(L, "AddRef",				&LuaSprite::AddRef);
	ELuna::registerMethod<LuaSprite>(L, "Release",				&LuaSprite::Release);
	ELuna::registerMethod<LuaSprite>(L, "__tostring",			&LuaSprite::__tostring);

	ELuna::registerMethod<LuaSprite>(L, "SetRotation",			&LuaSprite::SetRotation);
	ELuna::registerMethod<LuaSprite>(L, "SetScale",				&LuaSprite::SetScale);
	ELuna::registerMethod<LuaSprite>(L, "SetStretch",			&LuaSprite::SetStretch);
	ELuna::registerMethod<LuaSprite>(L, "SetTextureFilter",		&LuaSprite::SetTextureFilter);
	ELuna::registerMethod<LuaSprite>(L, "IsTextureFilter",		&LuaSprite::IsTextureFilter);
	ELuna::registerMethod<LuaSprite>(L, "SetAlphaAdd",			&LuaSprite::SetAlphaAdd);
	ELuna::registerMethod<LuaSprite>(L, "IsAlphaAdd",			&LuaSprite::IsAlphaAdd);
	ELuna::registerMethod<LuaSprite>(L, "GetRotation",			&LuaSprite::GetRotation);
	ELuna::registerMethod<LuaSprite>(L, "GetScale",				&LuaSprite::GetScale,2);
	ELuna::registerMethod<LuaSprite>(L, "GetStretch",			&LuaSprite::GetStretch,2);

	ELuna::registerMethod<LuaSprite>(L, "GetPosition",			&LuaSprite::GetPosition,2);
	return 1;
}