#pragma once
#include "ELuna.h"
#include "ggeAnimation.h"

class LuaAnimation
{
public:
	LuaAnimation();
	~LuaAnimation();
	void		SetTexture(void* texture, int frames, float width, float height);
	//void		Copy(gUInt ani);
	void*		Clone();
	void		Play();
	void		Stop();
	void		Resume();
	void		Update(float dt);
	bool		IsPlaying();

	void		SetMode(int mode);
	int			GetMode();
	void		SetSpeed(int v);
	int			GetSpeed();
	void		SetFrame(int v);
	int			GetFrame();

	void		Render2(float x, float y);
	void		Render(float x, float y);
	void		RenderEx(float x, float y, float rotation, float hscale, float vscale);
	void		RenderStretch(float x1, float y1, float x2, float y2);
	void		Render4V(float x0, float y0, float x1, float y1, float x2, float y2, float x3, float y3);

	void		AddFrameSprite(void* spr,int n);
	void*		GetFrameSprite(int n);
	void		DelFrameSprite(int n) ;
	int			GetFrameCount() ;
	float		GetWidth();
	float		GetHeight();
	void		GetBoundingBox(void* rect);
	void		GetBoundingBoxEx(void* rect);

	void		SetColor(UINT color, int i) ;
	void		SetZ(float z, int i);
	void		SetBlendMode(int blend);
	void		SetHotSpot(float x, float y);
	void		SetFlip(bool bX, bool bY, bool bHotSpot);

	UINT		GetColor(int i) ;
	float		GetZ(int i) ;
	int			GetBlendMode();
	float		GetHotSpot(lua_State *L);
	bool		GetFlip(lua_State *L);

	void		SetP(void* v){Release();m_ani = (gge::ggeAnimation*)v;};
	void*		GetP(){return m_ani;};
	void		AddRef(){m_ani->AddRef();};
	int			GetRefCount(){return m_ani->GetRefCount();};
	bool		Release();

	void		SetRotation(float v){m_rotation=v;}
	void		SetScale(float h,float v){m_hscale=h;m_vscale=v;}
	void		SetStretch(float w,float h){m_stretchw=w;m_stretchh=h;}

	void		SetTextureFilter(bool v){m_Filter=v?gge::BLEND_TEXFILTER:gge::BLEND_NOTEXFILTER;SetBlendMode(GetBlendMode());}//����
	bool		IsTextureFilter(){return m_Filter==gge::BLEND_TEXFILTER;}
	void		SetAlphaAdd(UINT v){m_AlphaAdd=v;}//����
	bool		IsAlphaAdd(){return m_AlphaAdd!=0;}
	float		GetRotation(){return m_rotation;}
	float		GetScale(lua_State* L){lua_pushnumber(L,m_hscale);return m_vscale;}
	float		GetStretch(lua_State* L){lua_pushnumber(L,m_stretchw);return m_stretchh;}
	float		GetPosition(lua_State* L){lua_pushnumber(L,m_x);return m_y;}

	void		SetLoop(bool loop){m_loop = loop?gge::ANI_LOOP:gge::ANI_NOLOOP;SetMode(m_ani->GetMode());}
	bool		IsLoop(){return m_loop==gge::ANI_LOOP;}
private:
	gge::ggeAnimation* m_ani;
	int m_loop;
	float m_hotx;
	float m_hoty;
	float m_x,m_y;
	float m_rotation;
	float m_hscale;
	float m_vscale;
	float m_stretchw;
	float m_stretchh;
	int m_Filter;
	UINT m_AlphaAdd;
};

int REG_GGE_Animation(lua_State * L);