#pragma once
#include "common.h"
#include "ggesound.h"
#include "ggemath.h"

using namespace gge;
using namespace ELuna;
class LuaSound
{
public:
	LuaSound():m_snd(NULL){};
	~LuaSound();
	bool	Sound_LoadFile(const char *filename);
	bool	Sound_LoadPtr(UINT ptr, UINT size);
	bool	Sound_LoadStr(LuaString str);
	void	Play();
	void	Pause() ;
	void	Resume() ;
	void	Stop();

	bool	IsPlaying();
	float	GetDuration();
	void	SetLoop(bool bLoop);
	bool	IsLoop();

	void	SetVolume(float v) ;
	float	GetVolume();
	void	SetPitch(float v) ;
	float	GetPitch();

	void	SetRelativeToListener(bool bRelative);
	bool	IsRelativeToListener();
	void	SetPosition(float x,float y,float z);
	float	GetPosition(lua_State* L);
	void	SetVelocity(float x,float y,float z);
	float	GetVelocity(lua_State* L);
	void	SetDirection(float x,float y,float z);
	float	GetDirection(lua_State* L);
	void	SetCone(float innerAngle, float outerAngle, float outerVolume);
	float	GetCone(lua_State* L);
	void	SetMinDistance(float distance);
	float	GetMinDistance();
	void	SetMaxDistance(float distance);
	float	GetMaxDistance();
	void	SetRolloffFactor(float factor);
	float	GetRolloffFactor();

	const char*		__tostring(){return "ggeSound";}
	void	SetP(void* v){Release();m_snd = (gge::ggeSound*)v;}
	void*	GetP(){return m_snd;}
	int		GetRefCount();
	void	AddRef();
	bool	Release();
private:
	gge::ggeSound* m_snd;
};