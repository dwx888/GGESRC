#pragma once
#include "common.h"
#include "ggemath.h"

using namespace gge;
class LuaVector3 :
	public ggeVector3
{
public:
	LuaVector3(float x,float y,float z):ggeVector3(x,y,z){}
	LuaVector3(ggeVector3 v);
	//~LuaVector3(void);
	float		Dot(LuaVector3 v);
	LuaVector3	Cross(LuaVector3 v);
	//void		Normalize();
	float		Length() ;
	float		LengthSquared() ;
	float		Angle(LuaVector3 v) ;
	//void		Clamp(float max);
	const char*		__tostring(){return "ggeVector3";}
	void		print(){printf("%f,%f,%f",x,y,z);}
};

