#pragma once
#include "common.h"
#include "ggemath.h"

using namespace gge;
class LuaRect:
	public ggeRect
{
public:
	LuaRect(){};
	//LuaRect(float _x1, float _y1, float _x2, float _y2) : ggeRect(_x1,_y1,_x2,_y2) {}
	LuaRect(ggeRect* v);
	float	GetXY(lua_State *L);
	float	GetWH(lua_State *L);
	void	SetXY(float _x1, float _y1, float _x2, float _y2);
	void	SetWH(float x, float y, float width, float height);
	void	SetSize(float width, float height);
	void	SetHotSpot(float x, float y);
	float	GetHotSpot(lua_State *L);
	void	SetRect(LuaRect v);
	
	void	Clear();
	bool	IsClean();
	void	Move(float x, float y);
	void	ClipWith(LuaRect rect);
	bool	TestPoint(float x, float y);
	bool	Intersect(LuaRect rect);

	const char*		__tostring(){return "ggeRect";}
	void*	GetP(){return this;}
	void	SetP(void*);
	//bool	eq(LuaRect rect);
private:

	float m_x;
	float m_y;
	float m_hotx;
	float m_hoty;
};

