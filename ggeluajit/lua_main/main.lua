-- @Author: baidwwy
-- @Date:   2018-02-24 07:02:08
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-08-17 12:40:12
local _isThread,_tMain,_pMain,_tReturn = ...
__gge.isthread = _isThread
local ffi = require("ffi")
ffi.getptr = function (p)return tonumber(ffi.cast("intptr_t",p))end
local loadstring = loadstring
ffi.cdef[[
    typedef struct {
        uint32_t flag;
        uint32_t total;
        uint32_t offset;
    }bdf_head;
    typedef struct {
        uint32_t crc32;
        uint32_t offset;
        uint32_t len;
        uint8_t xor[4];
    }bdf_list;
    int     _access(const char*, int);
    int     GetModuleFileNameA(void*,void*,int);
    int     GetCurrentDirectoryA(int,void*);
    bool    SetCurrentDirectoryA(const char*);
    bool    SetDllDirectoryA(const char*);
    int     MultiByteToWideChar(int ,int ,const char* ,int ,void* ,int);
    int     WideCharToMultiByte(int ,int ,void* ,int ,void* ,int ,void*,int);

    void*   FindResourceA(void*,const char*,const char*);
    int     SizeofResource(void*,void*);
    void*   LoadResource(void*,void*);
    char*   LockResource(void*);

    int     MessageBoxA(void *, const char*, const char*, int);
]]
--(msvcrt*.dll), kernel32.dll, user32.dll and gdi32.dll.
--BDF=======================================================================================
    local BDF = {}--资源
    function BDF:init(data)
        self.data = data
        self.list = {}
        local head = ffi.cast('bdf_head*',self.data)
        local off  = head.offset
        local num  = head.total
        local list = ffi.cast('bdf_list*',self.data+off)
        for i=0,num-1 do
            local crc32 = tostring(list[i].crc32)
            self.list[crc32]        = {
                offset = list[i].offset;
                len    = list[i].len
            }
        end
    end
    function BDF:access(path)
        return self.list[tostring(__gge.adler32(path))]
    end
    function BDF:readfile(path)
        local list = self.list[tostring(__gge.adler32(path))]
        local off,len = self.data+list.offset,list.len
        return ffi.string(off,len)
    end
--EX=======================================================================================
    local lz = require("zlib")
    __gge.crc32 = function (str)
        return lz.crc32(0,str)
    end
    __gge.adler32 = function (str)
        return lz.adler32(0,str)
    end
    __gge.compress = function (str)
        return lz.compress(str)
    end
    __gge.decompress = function (str)
        return lz.decompress(str)
    end
    __gge.uncompress = function (dest,destLen,source,sourceLen)
        if type(dest) == 'cdata' then dest = ffi.getptr(dest) end
        if type(source) == 'cdata' then source = ffi.getptr(source) end
        return lz.uncompress(dest,destLen,source,sourceLen)
    end
    __gge.enbase64 = function (str)
        return require("base64").encode(str)
    end
    __gge.debase64 = function (str)
        return require("base64").decode(str)
    end
--API=======================================================================================
    __gge.messagebox = function (msg,title,type)
        ffi.C.MessageBoxA(nil, tostring(msg), tostring(title) or '信息', type or 0)
    end
    __gge.utf8toansi = function (str,strlen)
        local slen = strlen or #str
        local len = ffi.C.MultiByteToWideChar(65001,0,str,slen,nil,0)
        local wbuf = ffi.new("wchar_t[?]",len+1)
        len = ffi.C.MultiByteToWideChar(65001,0,str,slen,wbuf,len)
        wbuf[len] = 0
        len = ffi.C.WideCharToMultiByte(0,0,wbuf,-1,nil,0,nil,0)
        local cbuf = ffi.new("char[?]",len+1)
        len = ffi.C.WideCharToMultiByte(0,0,wbuf,-1,cbuf,len,nil,0)
        cbuf[len] = 0
        return ffi.string(cbuf)
    end
    __gge.ansitoutf8 = function (str,strlen)
        local slen = strlen or #str
        local len = ffi.C.MultiByteToWideChar(0,0,str,slen,nil,0)
        local wbuf = ffi.new("wchar_t[?]",len+1)
        len = ffi.C.MultiByteToWideChar(0,0,str,slen,wbuf,len)
        wbuf[len] = 0
        len = ffi.C.WideCharToMultiByte(65001,0,wbuf,-1,nil,0,nil,0)
        local cbuf = ffi.new("char[?]",len+1)
        len = ffi.C.WideCharToMultiByte(65001,0,wbuf,-1,cbuf,len,nil,0)
        cbuf[len]=0
        return ffi.string(cbuf)
    end
 --LUA=======================================================================================
__gge.readfile = function (path)
    local file = io.open(path,"rb")
    if file then
        local str = file:read("*a")
        file:close()
        return str,#str
    end
end
__gge.writefile = function (path,data)
    local file = io.open(path,"wb")
    if file then
        file:write(data)
        file:close()
        return true
    end
    return false
end

    local tback = function (err)
        if __gge.traceback then
            return __gge.traceback(err) or debug.traceback(err,2)
        end
        return debug.traceback(err,2)
    end
__gge.xpcall = function (func,...)
    local ret = {xpcall(func, tback,...)}

    if ret[1] then
        return unpack(ret, 2,table.maxn(ret))
    end
    return false,unpack(ret, 2,table.maxn(ret))
end
__gge.dostring = function (str,path,env,...)
    local fun,err = loadstring(str, path)
    if fun then
        if env then setfenv(fun, env) end
        local ret = {__gge.xpcall(fun,...)}
        if ret[1] then
            return unpack(ret)
        end
        return path
    else
        __gge.traceback(err)
    end
end
__gge.dofileutf8 = function (path,env,...)
    if ffi.C._access(path,0)==0 then
        return __gge.dostring(__gge.utf8toansi(__gge.readfile(path)), path,env,...)
    end
    for i,v in ipairs(package.paths) do
        v = v:gsub("?",path)
        if ffi.C._access(v,0)==0 then
            return __gge.dostring(__gge.utf8toansi(__gge.readfile(v)), v,env,...)
        end
    end
end
__gge.doutf8 = function (str,env,...)
    return __gge.dostring(__gge.utf8toansi(str),'doutf8',env,...)
end
__gge.dobase64 = function (str,env,...)
    return __gge.dostring(__gge.debase64(str),"dobase64",env,...)
end
__gge.dozlibbase64 = function (str,env,...)
    str = __gge.decompress(__gge.debase64(str))
    return __gge.dostring(str,"dozlibbase64",env,...)
end
__gge.scriptaccess = function (path)
    if __gge.isdebug then
        return ffi.C._access(path..".lua",0)==0
    else
        return  BDF:access(path:lower())
    end
end
__gge.loadscript = function (name,env,...)
    if __gge.isdebug then--是否编译
        return __gge.dofileutf8(name,env,...)
    else
        for i,v in ipairs(package.paths) do
            v = v:gsub("?",name):gsub('\\','/'):lower()--小写
            if BDF:access(v) then
                return __gge.dostring(BDF:readfile(v),v,env,...)
            end
        end
        return __gge.dofileutf8(name,env,...)
    end
end
--=======================================================================================
--require
--=======================================================================================
    --脚本加载器
    local gge_loader = function (k)
        if rawget(package.loaded, k:lower()) then--如果小写已经存在
            return function (k)
                return rawget(package.loaded, k:lower())
            end
        end
        local r = __gge.loadscript(k)
        if r then
            return function (k)
                if k~=k:lower() then--非小写
                    rawset(package.loaded, k:lower(), r)
                end
                return r
            end
        end
    end
    table.insert(package.loaders, 1,gge_loader)
    --处理路径
    local path_conv = function (old)
        local path = {}
        for match in old:gmatch("([^/]+)") do
            if match=='..' then
                table.remove(path)
            elseif match~='.' then
                table.insert(path, match)
            end
        end
        local new = table.concat(path, "/")
        if new~=old then
            return gge_loader(new)
        end
    end
    table.insert(package.loaders,2, path_conv)
    --路径搜索配置
    package.path = nil
    package.paths = {}
    setmetatable(package, {
        __newindex=function (t,k,v)
            if k=='path' then
                for match in v:gmatch("([^;]+)") do
                    if __gge.isdebug then
                        table.insert(t.paths, (match:gsub("__gge.runpath",__gge.runpath,1)))
                    else
                        table.insert(t.paths, (match:gsub("__gge.runpath",'%.',1)))
                    end
                end
            else
                rawset(t, k, v)
            end
        end;
        __index=function (t,k)
            if k=='path' then
                return table.concat(t.paths, ";")
            end
        end})

function import(file,env,...)
    local ret
    env = setmetatable(env or {}, {__index=_G})
    ret = {__gge.loadscript(file,env,...)}
    return unpack(ret)
end
--=======================================================================================
do
    __gge.isgame = true
    io.stdout:setvbuf("no")
    local path = ffi.new("char[256]")
    ffi.C.GetModuleFileNameA(nil,path,256)
    __gge.runpath = ffi.string(path):match("(.+)\\[^\\]*%.%w+$"):gsub('\\','/')
    ffi.C.GetCurrentDirectoryA(256,path)
    __gge.curpath = ffi.string(path):gsub('\\','/')

    local hres = ffi.C.FindResourceA(nil,'DATA','GGELUA')
    if hres ~= nil then--是否编译
        local size = ffi.C.SizeofResource(nil,hres)
        local resok = false
        if size>0 then
            local hGlobal = ffi.C.LoadResource(nil,hres)
            if hGlobal ~= nil then
                BDF:init(ffi.C.LockResource(hGlobal))
                resok = true
            end
        end
        if not resok then
            __gge.messagebox("致命的错误！","GGELUA")
            return
        end
    else
        __gge.isdebug = true
        if not _tMain then
            print(">",__gge.curpath,__gge.version)
            local cmd = __gge.command
            if #cmd >1 and ffi.C._access(string.format('%s/main.lua',cmd[#cmd]),0)==0 then
                ffi.C.SetCurrentDirectoryA(cmd[#cmd])
            elseif ffi.C._access(string.format('%s/main.lua',cmd[#cmd-1]),0)==0 then
                print [["无法获取项目目录,必须回到"main.lua"标签,才可以运行项目."]]
                ffi.C.SetCurrentDirectoryA(cmd[#cmd-1])
            else
                print [["找不到main.lua文件,无法正常运行项目."]]
            end

        end
    end
    package.cpath = package.cpath ..(";__gge.runpath/模块/?.dll;__gge.runpath/lib/?.dll;./模块/?.dll;./lib/?.dll;"):gsub("__gge.runpath",__gge.runpath)
    if _tMain then
        package.path = _pMain
    else
        package.path  = ([[?.lua;.\?.lua;.\脚本\?.lua;.\脚本\?\初始化.lua;
                __gge.runpath\脚本\?.lua;
                __gge.runpath\脚本\?\初始化.lua;
                __gge.runpath\脚本\游戏端\?.lua;
                __gge.runpath\脚本\游戏端\?\初始化.lua;
                __gge.runpath\脚本\GGE库\?.lua;
                __gge.runpath\脚本\LUA库\?.lua;
                __gge.runpath\脚本\FFI库\?.lua;]]):gsub('\n%s*','')
    end

    require("gge公共")
    if _tMain then
        __gge.command = {_tMain}
        require(_tMain)
    else
        require("gge引擎")
        require('main')
    end
end

if _tMain then--thread
    function 返回消息(...)
        return _tReturn(...)
    end
    return function (msg,...)
        if msg == '__tMessage' then
            if 消息函数 then
                return __gge.xpcall(消息函数,...)
            end
        elseif 循环函数 then
            __gge.xpcall(循环函数)
        end
    end
end