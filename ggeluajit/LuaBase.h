#pragma once

#include "ggebase.h"
#include "common.h"
//#include "LuaTexture.h"
//using namespace ELuna;
bool Frame();
bool Exit();
bool FocusLost();
bool FocusGain();
bool Message(HWND h, UINT u, WPARAM w, LPARAM l);

class LuaBase
{
public:
	LuaBase(){}
	~LuaBase(){}

	bool		Engine_Create(lua_State* L);
	void		Engine_Release();
	bool		System_Initiate();
	bool		System_Start();
	void		System_Stop();
	void		System_SetStateCHAR(int state ,const char *value);
	void		System_SetStateINT(int state ,int value);
	void		System_SetStateBOOL(int state ,bool value);
	void		System_SetStateSYS(int state ,int value);
	void		System_SetStateFUN(int state);

	int			System_GetStateINT(int state);
	bool		System_GetStateBOOL(int state);
	const char*	System_GetStateCHAR(int state);
	int			System_GetStateFUN(int state);
	int			System_GetStateSYS();
	
	void		System_Log(const char *format);
	bool		System_Launch(const char *url);

	void		Resource_Load(const char *filename,lua_State* L);
	UINT		Resource_LoadTo(const char *filename,void* buf,UINT size);
	UINT		Resource_GetSize(const char *filename);
	bool		Resource_IsExist(const char *filename);
	void		Resource_Free(void* res);
	
	bool		Resource_AttachPack(const char *filename, const char *password);
	void		Resource_RemovePack(const char *filename);
	void		Resource_AddPath(const char *filename);

	void		Ini_SetFile(const char *filename);
	void		Ini_SetString(const char *section, const char *name, const char *value);
	const char*	Ini_GetString(const char *section, const char *name, const char *def_val);

	UINT		Timer_GetTime();
	UINT		Timer_GetTick();
	float		Timer_GetDelta();
	int			Timer_GetFPS();

	UINT		Random_Create();
	void		Random_Seed(int seed, UINT rid);
	int			Random_Int(int min, int max, UINT rid);
	float		Random_Float(float min, float max, UINT rid);

	void		Graph_Clear(UINT color);
	bool		Graph_BeginScene(void* texture);
	void		Graph_EndScene();
	void		Graph_GetRenderTarget(void* texture);
	void		Graph_SetCurrentShader(void* shader);
	void*		Graph_GetCurrentShader();
	void		Graph_RenderLine(float x1, float y1, float x2, float y2, UINT color);
	void		Graph_RenderRect(float x1, float y1, float x2, float y2, UINT color);
	void		Graph_RenderQuad(void* quad);
	void		Graph_RenderTriple(void* triple);
	//void		Graph_SetClipping(int x , int y , int width , int height );
	//void		Graph_SetTransform(float x , float y , float dx , float dy , float rot , float hscale, float vscale); 
	void		Graph_Snapshot(const char *filename,int format);
	void		Graph_RenderBatch(UINT primType, void* vt, int primNum,void* tex, int blend );
	void		Graph_RenderBatchIndices(UINT primType, void* vt, int vertexNum, void* indices, int primNum, void* tex, int blend);
	void		Graph_SetProjectionMatrix(void* mat);
	void*		Graph_GetProjectionMatrix();
	void		Graph_SetViewMatrix(void* mat);
	void*		Graph_GetViewMatrix();
	void		Graph_SetTransformMatrix(void* mat);
	void*		Graph_GetTransformMatrix();

	float 		Input_GetMousePosX();
	float 		Input_GetMousePosY();
	void		Input_SetMousePos(float x, float y);
	int			Input_GetMouseWheel();
	bool		Input_IsMouseOver();
	bool		Input_IsKeyPress(int key);
	bool		Input_IsKeyUp(int key);
	bool		Input_IsKeyDown(int key);
	bool		Input_IsMousePress(int key);
	bool		Input_IsMouseUp(int key);
	bool		Input_IsMouseDown(int key);
	int			Input_GetKey();
	const char*		Input_GetChar();
	const char*		Input_GetKeyName(int key);

	//bool		Video_LoadFromFile(const char *filename);
	//void		Video_Play();
	//void		Video_Pause();
	//void		Video_Stop();
	//void		Video_Render();
	//void		Video_RenderEx(float x, float y, float width, float height);
	//bool		Video_IsPlaying();
	//double		Video_GetPlayingTime();
	//void		Video_SetVolume(int volume);

	//static bool OnFrame();
	//static int ref_Frame;
};
