#pragma once
#include "common.h"
#include "ggeswapchain.h"
class LuaSwapChain
{
public:
	LuaSwapChain(int hwnd, int width, int height);
	~LuaSwapChain(void);
	bool BeginScene(UINT color);
	void EndScene(int x, int y, int width, int height);

private:
	gge::ggeSwapChain* m_obj;
};

