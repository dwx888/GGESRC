#pragma once
#include "common.h"
#include "ggetexture.h"

//using namespace std;
using namespace ELuna;
using namespace gge;
class LuaTexture
{
public:
	LuaTexture();
	LuaTexture(ggeTexture* v): m_tex(v){}
	~LuaTexture();
	bool	Texture_LoadFile(const char *filename, UINT colorKey);
	bool	Texture_LoadPtr(UINT ptr, ULONG size, UINT colorKey);
	bool	Texture_LoadUD(void* ptr, ULONG size, UINT colorKey);//userdata
	bool	Texture_LoadStr(LuaString data, UINT colorKey);
	bool	Texture_LoadWebp(LuaString data, UINT flip);
	bool	Texture_LoadWebptr(UINT data,UINT size, UINT flip);
	bool	Texture_Create(int width, int height);
	bool	Texture_CreateRenderTarget(int width, int height, int targetType);
	void*	Copy();
	void*	CopyRect(int left, int top, int width, int height);
	void*	Lock(bool bReadOnly, int left, int top, int width, int height);
	void	Unlock();
	bool	IsLock();
	void	SetPixel(int x,int y,UINT c);
	UINT	GetPixel(int x,int y);
	void	FillColor(UINT color);
	bool	SaveToFile(const char *filename,int format);
	int		GetWidth(bool b = false);
	int		GetHeight(bool b = false);
	void	Grayscale();

	void*	GetP(){return m_tex;}
	void	SetP(void* v){Release();m_tex	= (ggeTexture*)v;}
	
	int		GetRefCount();
	void	AddRef();
	bool	Release();
	const char*		__tostring(){return "ggeTexture";}
private:
	ggeTexture*	m_tex;
	UINT*		m_lock;

};
