#include "LuaSwapChain.h"


LuaSwapChain::LuaSwapChain(int hwnd, int width, int height){
	m_obj = gge::SwapChain_Create((HWND)hwnd,width,height);
}


LuaSwapChain::~LuaSwapChain(void){
	GGE_RELEASE(m_obj);
}
bool LuaSwapChain::BeginScene(UINT color){
	return m_obj->BeginScene(color);
}
void LuaSwapChain::EndScene(int x, int y, int width, int height){
	m_obj->EndScene(x,y,width,height);
}
int REG_GGE_SwapChain(lua_State *L){
	const char* gname = "__LuaRect";
	ELuna::registerClass<LuaSwapChain>(L, gname, ELuna::constructor<LuaSwapChain,int,int,int>);

	ELuna::registerMethod<LuaSwapChain>(L, "BeginScene", &LuaSwapChain::BeginScene);
	ELuna::registerMethod<LuaSwapChain>(L, "EndScene", &LuaSwapChain::EndScene);


	return 1;
}