#pragma once
#include "ggebase.h"
#include "ELuna.h"
#include "LuaMain.h"
//using namespace ELuna;

extern lua_State*		L ;
extern CRITICAL_SECTION G_CS;
extern bool				IsInit;
extern bool				IsExit;

int REG_GGE_Base(lua_State * L);
int REG_GGE_Texture(lua_State * L);
int REG_GGE_Sprite(lua_State * L);
int REG_GGE_Font(lua_State * L);
int REG_GGE_Sound(lua_State * L);
int REG_GGE_Animation(lua_State * L);
int REG_GGE_Rect(lua_State * L);
int REG_GGE_Vector(lua_State * L);
int REG_GGE_Vector3(lua_State * L);


int REG_Thread(lua_State * L);
extern "C"{
int luaopen_zlib(lua_State * L);
int luaopen_base64(lua_State * L);
int luaopen_struct(lua_State * L);
int luaopen_serialize(lua_State * L);
}
void gge_preload(lua_State *L, lua_CFunction f, const char *name);
#define GGELUA_VERSION "GGELUA 1.0 beta4"