#include "common.h"
extern "C" {
#include "lua_zlib.c"
#include "lbase64.c"
#include "struct.c"
#include "serialize.c"
}
CRITICAL_SECTION	G_CS;
bool				IsInit	= false;
bool				IsExit	= false;
void gge_preload(lua_State *L, lua_CFunction f, const char *name)
{
	lua_getglobal(L, "package");
	lua_getfield(L, -1, "preload");
	lua_pushcfunction(L, f);
	lua_setfield(L, -2, name);
	lua_pop(L, 2);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{ 
	setvbuf(stdout, NULL,_IONBF,0);  
	L = ELuna::openLua();
	gge::System_Log("GGELUA");
	gge_preload(L,REG_GGE_Base,		"gge_base");		//引擎
	gge_preload(L,REG_GGE_Texture,	"gge_texture");		//纹理
	gge_preload(L,REG_GGE_Sprite,	"gge_sprite");		//精灵
	gge_preload(L,REG_GGE_Sound,	"gge_sound");		//音效
	gge_preload(L,REG_GGE_Font,		"gge_font");		//字体
	gge_preload(L,REG_GGE_Animation,"gge_animation");	//动画
	gge_preload(L,REG_GGE_Rect,		"gge_rect");		//包围盒
	gge_preload(L,REG_GGE_Vector,	"gge_Vector");		//坐标
	gge_preload(L,REG_GGE_Vector3,	"gge_Vector3");		//坐标3

	gge_preload(L,REG_Thread,	"gge_thread");	//线程

	gge_preload(L,luaopen_zlib,"zlib");				//zlib
	gge_preload(L,luaopen_base64,"base64");			//base64
	gge_preload(L,luaopen_struct,"struct");			//struct(string.pack)
	gge_preload(L,luaopen_serialize,"serialize");

	InitializeCriticalSection(&G_CS);
	EnterCriticalSection(&G_CS);
	
	lua_newtable(L);
	//版本
	lua_pushstring(L,GGELUA_VERSION);
	lua_setfield(L,-2,"version");
	//许可证
	lua_pushlightuserdata(L,&G_CS);
	lua_setfield(L,-2,"cs");		
	//参数 命令行
	lua_pushstring(L,"command");		
	lua_newtable(L);
	for (int i=1;i<__argc;i++){
		lua_pushinteger(L,i);
		lua_pushstring(L,__argv[i]);
		lua_settable(L,-3);
	}
	lua_settable(L,-3);
	lua_setglobal(L,"__gge");
	luaL_loadbuffer(L,(char *)luaJIT_BC_main,luaJIT_BC_main_SIZE,"GGELUA_MAIN");
	//luaL_loadfile(L,"I:/GGELUA2018/GGESRC/main/main.lua");
	lua_call(L,0,0);
	LeaveCriticalSection(&G_CS);
	if (IsInit)
		gge::System_Start();
	
	//printf("lua_gettop:%d\n",lua_gettop(L));
	ELuna::closeLua(L);
	gge::Engine_Release();
	return 0;
}