#include "LuaVector.h"


float LuaVector::Dot(float x,float y,float x1,float y1){
	m_obj.x = x;m_obj.y = y;
	return m_obj.Dot(ggeVector(x1,y1));
}
float LuaVector::Normalize(float x,float y,lua_State* L){
	m_obj.x = x;m_obj.y = y;
	m_obj.Normalize();
	lua_pushnumber(L,m_obj.x);
	return m_obj.y;
}
float LuaVector::Length(float x,float y){
	m_obj.x = x;m_obj.y = y;
	return m_obj.Length();
}
float LuaVector::LengthSquared(float x,float y){
	m_obj.x = x;m_obj.y = y;
	return m_obj.LengthSquared();
}
float LuaVector::Angle(float x,float y){
	m_obj.x = x;m_obj.y = y;
	return m_obj.Angle();
}
float LuaVector::AngleTo(float x,float y,float x1,float y1){
	m_obj.x = x;m_obj.y = y;
	return m_obj.AngleTo(ggeVector(x1,y1));
}
int LuaVector::Sign(float x,float y,float x1,float y1){
	m_obj.x = x;m_obj.y = y;
	return m_obj.Sign(ggeVector(x1,y1));
}
float LuaVector::Perpendicular(float x,float y,lua_State* L){
	m_obj.x = x;m_obj.y = y;
	ggeVector r = m_obj.Perpendicular();
	lua_pushnumber(L,r.x);
	return r.y;
}
float LuaVector::Reflect(float x,float y,float x1,float y1,lua_State* L){
	m_obj.x = x;m_obj.y = y;
	ggeVector r = m_obj.Reflect(ggeVector(x1,y1));
	lua_pushnumber(L,r.x);
	return r.y;
}
float LuaVector::ToReflect(float x,float y,float x1,float y1,lua_State* L){
	m_obj.x = x;m_obj.y = y;
	ggeVector r = m_obj.ToReflect(ggeVector(x1,y1));
	lua_pushnumber(L,r.x);
	return r.y;
}
float LuaVector::Rotate(float x,float y,float radian,lua_State* L){
	m_obj.x = x;m_obj.y = y;
	m_obj.Rotate(radian);
	lua_pushnumber(L,m_obj.x);
	return m_obj.y;
}
float LuaVector::Clamp(float x,float y,float max,lua_State* L){
	m_obj.x = x;m_obj.y = y;
	m_obj.Clamp(max);
	lua_pushnumber(L,m_obj.x);
	return m_obj.y;
}
int REG_GGE_Vector(lua_State *L){
	const char* gname = "__LuaVector";
	ELuna::registerClass<LuaVector>(L, gname, ELuna::constructor<LuaVector>);

	ELuna::registerMethod<LuaVector>(L, "Dot",				&LuaVector::Dot);
	ELuna::registerMethod<LuaVector>(L, "Normalize",		&LuaVector::Normalize,2);
	ELuna::registerMethod<LuaVector>(L, "Length",			&LuaVector::Length);
	ELuna::registerMethod<LuaVector>(L, "LengthSquared",	&LuaVector::LengthSquared);
	ELuna::registerMethod<LuaVector>(L, "Angle",			&LuaVector::Angle);

	ELuna::registerMethod<LuaVector>(L, "AngleTo",			&LuaVector::AngleTo);
	ELuna::registerMethod<LuaVector>(L, "Sign",				&LuaVector::Sign);
	ELuna::registerMethod<LuaVector>(L, "Perpendicular",	&LuaVector::Perpendicular,2);
	ELuna::registerMethod<LuaVector>(L, "Reflect",			&LuaVector::Reflect,2);
	ELuna::registerMethod<LuaVector>(L, "ToReflect",		&LuaVector::ToReflect,2);

	ELuna::registerMethod<LuaVector>(L, "Rotate",			&LuaVector::Rotate,2);
	ELuna::registerMethod<LuaVector>(L, "Clamp",			&LuaVector::Clamp,2);

	ELuna::registerMethod<LuaVector>(L, "__tostring",		&LuaVector::__tostring);
	return 1;
}