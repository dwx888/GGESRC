#include "LuaFont.h"

LuaFont::LuaFont(const char *filename, int fontSize, int mode){
	m_filter = 0;
	if (fontSize){
		m_font = gge::Font_Create(filename,fontSize,mode);
	}else
		m_font = gge::Font_CreateFromImage(filename);
}
LuaFont::~LuaFont(){
	GGE_RELEASE(m_font);
}
void* LuaFont::Clone() {
	return m_font->Clone();
}
void LuaFont::Render(float x, float y, const char *str) {
	m_font->Render(x,y,str);
}
void LuaFont::SetColor(UINT color){
	m_font->SetColor(color);
}
void LuaFont::SetZ(float z){ 
	m_font->SetZ(z);
}
void LuaFont::SetBlendMode(int blend){
	m_font->SetBlendMode(blend|m_filter);
}
void LuaFont::SetLineWidth(int width){
	m_font->SetLineWidth(width);
}
void LuaFont::SetCharSpace(int width){
	m_font->SetCharSpace(width);
}
void LuaFont::SetLineSpace(int width){
	m_font->SetLineSpace(width);
}
void LuaFont::SetCharNum(int width){
	m_font->SetCharNum(width);
}
void LuaFont::SetAlign(int width){
	m_font->SetAlign(width);
}
void LuaFont::SetShadowColor(UINT width){
	m_font->SetShadowColor(width);
}
void LuaFont::SetBorderColor(UINT width){
	m_font->SetBorderColor(width);
}
UINT LuaFont::GetColor(){
	return m_font->GetColor();
}
float LuaFont::GetZ(){
	return m_font->GetZ();
}
int  LuaFont::GetBlendMode() {
	return m_font->GetBlendMode();
}
int  LuaFont::GetLineWidth() {
	return m_font->GetLineWidth();
}
int  LuaFont::GetCharSpace() {
	return m_font->GetCharSpace();
}
int  LuaFont::GetLineSpace() {
	return m_font->GetLineSpace();
}
int	LuaFont::GetFontHight() {
	return m_font->GetFontHight();
}
int	LuaFont::GetFontSize() {
	return m_font->GetFontSize();
}
int	LuaFont::GetCharNum() {
	return m_font->GetCharNum();
}
int	LuaFont::GetAlign() {
	return m_font->GetAlign();
}
UINT LuaFont::GetShadowColor() {
	return m_font->GetShadowColor();
}
UINT LuaFont::GetBorderColor() {
	return m_font->GetBorderColor();
}
float LuaFont::GetStringInfo(const char *str, int len, lua_State* L){
	gge::ggeFont::StringInfo strinfo;
	m_font->GetStringInfo(str,strinfo);
	lua_pushnumber(L,strinfo.Width);
	return strinfo.Height;
}

int	 LuaFont::GetRefCount(){ 
	return m_font->GetRefCount();
}
void LuaFont::AddRef(){ 
	return m_font->AddRef();
}
bool LuaFont::Release(){ 
	if (m_font->GetRefCount()==1){
		m_font->Release();
		m_font = NULL;
		return true;
	}
	m_font->Release();
	return false;
}
int REG_GGE_Font(lua_State * L){
	const char* gname = "__ggeFont";
	ELuna::registerClass<LuaFont>(L, gname,  ELuna::constructor<LuaFont,const char *, int , int >);

	ELuna::registerMethod<LuaFont>(L, "Clone",				&LuaFont::Clone);
	ELuna::registerMethod<LuaFont>(L, "Render",				&LuaFont::Render);
	ELuna::registerMethod<LuaFont>(L, "SetColor",			&LuaFont::SetColor);
	ELuna::registerMethod<LuaFont>(L, "SetZ",				&LuaFont::SetZ);
	ELuna::registerMethod<LuaFont>(L, "SetBlendMode",		&LuaFont::SetBlendMode);
	ELuna::registerMethod<LuaFont>(L, "SetLineWidth",		&LuaFont::SetLineWidth);
	ELuna::registerMethod<LuaFont>(L, "SetCharSpace",		&LuaFont::SetCharSpace);
	ELuna::registerMethod<LuaFont>(L, "SetLineSpace",		&LuaFont::SetLineSpace);
	ELuna::registerMethod<LuaFont>(L, "SetCharNum",			&LuaFont::SetCharNum);
	ELuna::registerMethod<LuaFont>(L, "SetAlign",			&LuaFont::SetAlign);
	ELuna::registerMethod<LuaFont>(L, "SetShadowColor",		&LuaFont::SetShadowColor);
	ELuna::registerMethod<LuaFont>(L, "SetBorderColor",		&LuaFont::SetBorderColor);

	ELuna::registerMethod<LuaFont>(L, "GetColor",			&LuaFont::GetColor);
	ELuna::registerMethod<LuaFont>(L, "GetZ",				&LuaFont::GetZ);
	ELuna::registerMethod<LuaFont>(L, "GetBlendMode",		&LuaFont::GetBlendMode);
	ELuna::registerMethod<LuaFont>(L, "GetLineWidth",		&LuaFont::GetLineWidth);
	ELuna::registerMethod<LuaFont>(L, "GetCharSpace",		&LuaFont::GetCharSpace);
	ELuna::registerMethod<LuaFont>(L, "GetLineSpace",		&LuaFont::GetLineSpace);
	ELuna::registerMethod<LuaFont>(L, "GetFontHight",		&LuaFont::GetFontHight);
	ELuna::registerMethod<LuaFont>(L, "GetFontSize",		&LuaFont::GetFontSize);
	ELuna::registerMethod<LuaFont>(L, "GetCharNum",			&LuaFont::GetCharNum);
	ELuna::registerMethod<LuaFont>(L, "GetAlign",			&LuaFont::GetAlign);
	ELuna::registerMethod<LuaFont>(L, "GetShadowColor",		&LuaFont::GetShadowColor);
	ELuna::registerMethod<LuaFont>(L, "GetBorderColor",		&LuaFont::GetBorderColor);
	ELuna::registerMethod<LuaFont>(L, "GetStringInfo",		&LuaFont::GetStringInfo,2);

	ELuna::registerMethod<LuaFont>(L, "SetP",				&LuaFont::SetP);
	ELuna::registerMethod<LuaFont>(L, "GetP",				&LuaFont::GetP);
	ELuna::registerMethod<LuaFont>(L, "GetRefCount",		&LuaFont::GetRefCount);
	ELuna::registerMethod<LuaFont>(L, "AddRef",				&LuaFont::AddRef);
	ELuna::registerMethod<LuaFont>(L, "Release",			&LuaFont::Release);
	ELuna::registerMethod<LuaFont>(L, "SetTextureFilter",	&LuaFont::SetTextureFilter);
	return 1;
}
