#include "LuaRect.h"

LuaRect::LuaRect(ggeRect* v){
	
	SetXY(v->x1,v->y1,v->x2,v->y2);
}

float LuaRect::GetXY(lua_State *L){
	lua_pushnumber(L,x1);
	lua_pushnumber(L,y1);
	lua_pushnumber(L,x2);
	return y2;
}
float LuaRect::GetWH(lua_State *L){
	lua_pushnumber(L,x1);
	lua_pushnumber(L,y1);
	lua_pushnumber(L,GetWidth());
	return GetHeight();
}
void LuaRect::SetXY(float _x1, float _y1, float _x2, float _y2){
	Clear();
	m_x = _x1;
	m_y = _y1;
	x1 = _x1 - m_hotx;//-中心
	y1 = _y1 - m_hoty;
	x2 = _x2 - m_hotx;//-中心
	y2 = _y2 - m_hoty;
}
void LuaRect::SetWH(float x, float y, float width, float height){
	Clear();
	m_x = x;
	m_y = y;
	x1 = x - m_hotx;//-中心
	y1 = y - m_hoty;

	SetWidth(width);
	SetHeight(height);
}
void LuaRect::SetRect(LuaRect v){
	Clear();
	x1 = v.x1;
	y1 = v.y1;
	x2 = v.x2;
	y2 = v.y2;
	m_x = x1;
	m_y = y1;
}
void LuaRect::Clear(){ 
	m_x		= 0;
	m_y		= 0;
	m_hotx  = 0;
	m_hoty  = 0;
	return ggeRect::Clear();
}
bool LuaRect::IsClean(){ 
	return ggeRect::IsClean();
}
void LuaRect::Move(float x, float y){
	m_x = x;
	m_y = y;
	ggeRect::Move(m_x-m_hotx,m_y-m_hoty);
}
void LuaRect::ClipWith(LuaRect rect){
	ggeRect::ClipWith(rect);
}
bool LuaRect::TestPoint(float x, float y){
	return ggeRect::TestPoint(x,y);
}
bool LuaRect::Intersect(LuaRect rect){
	return ggeRect::Intersect(rect);
}

void LuaRect::SetSize(float width, float height){
	SetWidth(width);
	SetHeight(height);
}
void LuaRect::SetHotSpot(float x, float y){
	m_hotx=x;
	m_hoty=y;
	ggeRect::Move(m_x-m_hotx,m_y-m_hoty);
}
float LuaRect::GetHotSpot(lua_State *L){
	lua_pushnumber(L,m_hotx);
	return m_hoty;
}
int REG_GGE_Rect(lua_State *L){
	const char* gname = "__LuaRect";
	ELuna::registerClass<LuaRect>(L, gname, ELuna::constructor<LuaRect>);

	ELuna::registerMethod<LuaRect>(L, "SetXY",		&LuaRect::SetXY);
	ELuna::registerMethod<LuaRect>(L, "SetWH",		&LuaRect::SetWH);
	ELuna::registerMethod<LuaRect>(L, "GetXY",		&LuaRect::GetXY,4);
	ELuna::registerMethod<LuaRect>(L, "GetWH",		&LuaRect::GetWH,4);
	ELuna::registerMethod<LuaRect>(L, "SetSize",	&LuaRect::SetSize);
	ELuna::registerMethod<LuaRect>(L, "SetHotSpot", &LuaRect::SetHotSpot);
	ELuna::registerMethod<LuaRect>(L, "GetHotSpot", &LuaRect::GetHotSpot,2);

	ELuna::registerMethod<LuaRect>(L, "SetRect",	&LuaRect::SetRect);
	ELuna::registerMethod<LuaRect>(L, "GetP",		&LuaRect::GetP);
	ELuna::registerMethod<LuaRect>(L, "Clear",		&LuaRect::Clear);
	ELuna::registerMethod<LuaRect>(L, "IsClean",	&LuaRect::IsClean);
	ELuna::registerMethod<LuaRect>(L, "TestPoint",	&LuaRect::TestPoint);
	ELuna::registerMethod<LuaRect>(L, "Intersect",	&LuaRect::Intersect);
	ELuna::registerMethod<LuaRect>(L, "ClipWith",	&LuaRect::ClipWith);
	ELuna::registerMethod<LuaRect>(L, "Move",		&LuaRect::Move);

	ELuna::registerMethod<LuaRect>(L, "SetRadius",	&ggeRect::SetRadius);
	ELuna::registerMethod<LuaRect>(L, "Encapsulate",&ggeRect::Encapsulate);
	ELuna::registerMethod<LuaRect>(L, "SetWidth",	&ggeRect::SetWidth);
	ELuna::registerMethod<LuaRect>(L, "GetWidth",	&ggeRect::GetWidth);
	ELuna::registerMethod<LuaRect>(L, "SetHeight",	&ggeRect::SetHeight);
	ELuna::registerMethod<LuaRect>(L, "GetHeight",	&ggeRect::GetHeight);

	ELuna::registerMethod<LuaRect>(L, "__tostring", &LuaRect::__tostring);

	return 1;
}