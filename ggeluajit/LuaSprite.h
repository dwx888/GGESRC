#pragma once
#include "common.h"
#include "ggesprite.h"
#include "ggemath.h"
#include "ggetexture.h"
class LuaSprite
{
public:
	LuaSprite();
	~LuaSprite();
	bool		Sprite_Create(void* texture, float x, float y, float width, float height);
	void		Sprite_Pointer(void* v){m_spr = (gge::ggeSprite *)v;};
	void		Copy(void* spr);
	void*		Clone();
	void		Render(float x, float y);
	void		RenderEx(float x, float y, float rotation, float hscale, float vscale);
	void		RenderStretch(float x1, float y1, float x2, float y2);
	void		Render4V(float x0, float y0, float x1, float y1, float x2, float y2, float x3, float y3);

	void		SetPosition2(float x, float y);
	void		SetPosition(float x, float y);
	void		SetPositionEx(float x, float y, float rotation, float hscale, float vscale);
	void		SetPositionStretch(float x1, float y1, float x2, float y2) ;
	void		SetPosition4V(float x0, float y0, float x1, float y1, float x2, float y2, float x3, float y3) ;
	void		RenderPosition();

	void		SetTexture(void* texture) ;
	void		SetTextureRect(float x, float y, float width, float height) ;
	
	void		SetColor(UINT color, int i) ;
	void		SetZ(float z, int i);
	void		SetBlendMode(int blend);
	void		SetHotSpot(float x, float y);
	void		SetFlip(bool bX, bool bY, bool bHotSpot);

	void*		GetTexture();	
	float		GetTextureRect(lua_State *L);
	
	UINT		GetColor(int i) ;
	float		GetZ(int i) ;
	int			GetBlendMode();
	float		GetHotSpot(lua_State *L);
	bool		GetFlip(lua_State *L);

	void		GetBoundingBox(void* rect);
	void		GetBoundingBoxEx(void* rect);

	float		GetWidth() ;
	float		GetHeight();
	void*		GetQuad();

	const char*		__tostring(){return "ggeSprite";}
	void*		GetP(){return m_spr;}
	void		SetP(void* v){Release();m_spr = (gge::ggeSprite*)v;}
	int			GetRefCount();
	void		AddRef();
	bool		Release();

	void		SetRotation(float v){m_rotation=v;}
	void		SetScale(float h,float v){m_hscale=h;m_vscale=v;}
	void		SetStretch(float w,float h){m_stretchw=w;m_stretchh=h;}

	void		SetTextureFilter(bool v){m_Filter=v?gge::BLEND_TEXFILTER:gge::BLEND_NOTEXFILTER;SetBlendMode(m_spr->GetBlendMode());}//����
	bool		IsTextureFilter(){return m_Filter==gge::BLEND_TEXFILTER;}
	void		SetAlphaAdd(UINT v){m_AlphaAdd=v;}//����
	bool		IsAlphaAdd(){return m_AlphaAdd!=0;}
	float		GetRotation(){return m_rotation;}
	float		GetScale(lua_State* L){lua_pushnumber(L,m_hscale);return m_vscale;}
	float		GetStretch(lua_State* L){lua_pushnumber(L,m_stretchw);return m_stretchh;}
	float		GetPosition(lua_State* L){lua_pushnumber(L,m_x);return m_y;}

private:
	gge::ggeSprite* m_spr;
	float m_x,m_y;
	float m_rotation;
	float m_hscale;
	float m_vscale;
	float m_stretchw;
	float m_stretchh;
	int m_Filter;
	UINT m_AlphaAdd;
};