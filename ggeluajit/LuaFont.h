#pragma once
#include "common.h"
#include "ggefont.h"

class LuaFont
{
public:
	LuaFont(const char *filename, int fontSize, int mode);
	~LuaFont();
	void*		Clone();
	void		Render(float x, float y, const char *str);
	//void		Print(float x, float y, const char *format, ...);
	void		SetColor(UINT color);
	void		SetZ(float z);
	void		SetBlendMode(int blend);
	void		SetLineWidth(int width);
	void		SetCharSpace(int space);
	void		SetLineSpace(int space);
	void		SetCharNum(int num);
	void		SetAlign(int align);
	void		SetShadowColor(UINT color);
	void		SetBorderColor(UINT color);
	UINT		GetColor();
	float		GetZ();
	int			GetBlendMode();
	int			GetLineWidth();
	int			GetCharSpace();
	int			GetLineSpace();
	int			GetFontHight();
	int			GetFontSize();
	int			GetCharNum();
	int			GetAlign();
	UINT		GetShadowColor();
	UINT		GetBorderColor();
	float		GetStringInfo(const char *str, int len, lua_State* L);

	const char*		__tostring(){return "ggeFont";}
	void		SetP(void* v){Release();m_font = (gge::ggeFont*)v;}
	void*		GetP(){return m_font;}
	int			GetRefCount();
	void		AddRef();
	bool		Release();
	void		SetTextureFilter(bool v){m_filter=v?8:0;SetBlendMode(m_font->GetBlendMode());}//����
private:
	gge::ggeFont* m_font;
	int m_filter;
};