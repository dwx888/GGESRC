#include "LuaTexture.h"
#include "../webp/decode.h"

LuaTexture::LuaTexture(){
	m_tex	= NULL;
	m_lock	= NULL;
}
LuaTexture::~LuaTexture(){
	Unlock();
	GGE_RELEASE(m_tex);
}

bool LuaTexture::Texture_LoadFile(const char *filename, UINT colorKey){
	Release();
	m_tex = gge::Texture_Load(filename,colorKey);
	return m_tex!=NULL;
}
bool LuaTexture::Texture_LoadPtr(UINT ptr, ULONG size, UINT colorKey){
	Release();
	m_tex = gge::Texture_Load((char *)ptr,colorKey,size);
	return m_tex!=NULL;
}
bool LuaTexture::Texture_LoadUD(void* ptr, ULONG size, UINT colorKey){
	Release();
	m_tex = gge::Texture_Load((char *)ptr,colorKey,size);
	return m_tex!=NULL;
}
bool LuaTexture::Texture_LoadStr(LuaString data, UINT colorKey){
	Release();
	m_tex = gge::Texture_Load(data.str,colorKey,data.len);
	return m_tex!=NULL;
}
bool LuaTexture::Texture_LoadWebp(LuaString data, UINT flip){
	return Texture_LoadWebptr((UINT)data.str,data.len,flip);
}
bool LuaTexture::Texture_LoadWebptr(UINT data,UINT size, UINT flip){
	Release();
	int width,height;
	WebPGetInfo((uint8_t*)data,size,&width,&height);//获取宽高
	m_tex = gge::Texture_Create(width,height);
	WebPDecoderConfig config;
	WebPDecBuffer *output=&config.output;
	WebPInitDecBuffer(output);//初始化输出
	output->colorspace    = MODE_BGRA;
	output->u.RGBA.rgba   = (uint8_t*)m_tex->Lock(false);
	output->u.RGBA.size   = width*height*sizeof(UINT);
	output->u.RGBA.stride = width*sizeof(UINT);
	output->is_external_memory = TRUE;
	WebPDecoderOptions *options=&config.options;
	memset(options, 0, sizeof(WebPDecoderOptions));//初始化配置
	options->flip = flip;//翻转
	//options->bypass_filtering = TRUE;//是否禁用滤波
	//options->no_fancy_upsampling = TRUE;//是否禁用上采样
	VP8StatusCode r = WebPDecode((uint8_t*)data,size,&config);
	m_tex->Unlock();
	return r==VP8_STATUS_OK;
}
bool LuaTexture::Texture_Create(int width, int height){
	Release();
	m_tex = gge::Texture_Create(width,height);
	return m_tex!=NULL;
}
bool LuaTexture::Texture_CreateRenderTarget(int width, int height, int targetType){
	Release();
	m_tex = gge::Texture_CreateRenderTarget(width,height,targetType);
	return m_tex!=NULL;
}
void* LuaTexture::Copy(){
	if (m_tex){
		Unlock();
		UINT* slock = m_tex->Lock();
		int width	= m_tex->GetWidth();
		int height	= m_tex->GetHeight();
		ggeTexture* tex = gge::Texture_Create(width,height);
		UINT* dlock = tex->Lock();
		memcpy(dlock,slock,width*height*4);//32位4字节
		tex->Unlock();
		m_tex->Unlock();
		return tex;
	}
	return 0;
}
void* LuaTexture::CopyRect( int x, int y, int width, int height){
	if (m_tex &&x>=0&&y>=0&& x+width<=m_tex->GetWidth(true) && y+height<=m_tex->GetHeight(true)){
		Unlock();
		UINT* slock = m_tex->Lock();
		int SrcWidth = m_tex->GetWidth();
		int DstWidth = width;
		UINT* Src = slock+y*SrcWidth+x;//读取位置
		ggeTexture* tex = gge::Texture_Create(width,height);
		UINT* Dst = tex->Lock();;//写位置
		int Size = DstWidth*4;
		for (int i =0;i<height;i++){
			memcpy(Dst,Src,Size);
			Dst += DstWidth;
			Src += SrcWidth;
		}
		tex->Unlock();
		m_tex->Unlock();
		return tex;
	}
	return 0;
}
void LuaTexture::Grayscale(){//灰度级
	if (m_tex){
		Unlock();
		UINT* slock = m_tex->Lock(false);
		unsigned char *c = (unsigned char *)slock;
		int width = m_tex->GetWidth();
		int height = m_tex->GetHeight();
		
		for (int i =0;i<width*height;i++)
		{
			unsigned char B = *c++,G = *c++,R = *c++;
			unsigned char gray  =  (R + (G<<1) + B) >> 2;
			c-=3;
			*c++ =gray;
			*c++ =gray;
			*c++ =gray;
			c++ ;
		}
		m_tex->Unlock();
	}
}
void* LuaTexture::Lock(bool bReadOnly, int left, int top, int width, int height){
	if (!m_lock){
		m_lock = m_tex->Lock(bReadOnly,left,top,width,height);
	}
	return m_lock;
}
void LuaTexture::Unlock(){ 
	if (m_tex && m_lock){
		m_tex->Unlock();
		m_lock = NULL;
	}
}
bool LuaTexture::IsLock(){ 
	return m_lock!=0;
}
void LuaTexture::SetPixel(int x,int y,UINT c){
	if (m_tex)
	{
		if (!m_lock)
			m_lock = m_tex->Lock(false);
		int Width = GetWidth();
		m_lock[y * Width + x] = c;
	}

}
UINT LuaTexture::GetPixel(int x,int y){
	if (m_tex)
	{
		if (!m_lock)
			m_lock = m_tex->Lock();
		int Width = GetWidth();
		if (x>=0 && y>=0 && x<Width && y<GetHeight())
			return m_lock[y * Width + x];
	}

	return 0;
}
void LuaTexture::FillColor(UINT color) {
	if (m_tex)
		m_tex->FillColor(color);
}
bool LuaTexture::SaveToFile(const char *filename,int format){
	if (m_tex)
		return m_tex->SaveToFile(filename,GGE_IMAGE_FORMAT(format));
	return false;
}
int LuaTexture::GetWidth(bool b){
	if (m_tex)
		return m_tex->GetWidth(b);
	return 0;
}
int LuaTexture::GetHeight(bool b) {
	if (m_tex)
		return m_tex->GetHeight(b);
	return 0;
}
//=============================================
int	LuaTexture::GetRefCount() {
	if (m_tex)
		return m_tex->GetRefCount();
	return 0;
}
void	LuaTexture::AddRef() {
	if (m_tex)
		m_tex->AddRef();
}

bool	LuaTexture::Release() {
	if (m_tex)
	{
		if (m_tex->GetRefCount()==1){
			Unlock();
			m_tex->Release();
			m_tex = NULL;
			return true;
		}
		m_tex->Release();
	}
	return false;
}
int REG_GGE_Texture(lua_State * L){
	const char* gname = "__ggeTexture";
	ELuna::registerClass<LuaTexture>(L, gname,  ELuna::constructor<LuaTexture>);

	ELuna::registerMethod<LuaTexture>(L, "Texture_LoadFile",			&LuaTexture::Texture_LoadFile);
	ELuna::registerMethod<LuaTexture>(L, "Texture_LoadUD",				&LuaTexture::Texture_LoadUD);
	ELuna::registerMethod<LuaTexture>(L, "Texture_LoadPtr",				&LuaTexture::Texture_LoadPtr);
	ELuna::registerMethod<LuaTexture>(L, "Texture_LoadStr",				&LuaTexture::Texture_LoadStr);
	ELuna::registerMethod<LuaTexture>(L, "Texture_LoadWebp",			&LuaTexture::Texture_LoadWebp);
	ELuna::registerMethod<LuaTexture>(L, "Texture_LoadWebptr",			&LuaTexture::Texture_LoadWebptr);
	ELuna::registerMethod<LuaTexture>(L, "Texture_Create",				&LuaTexture::Texture_Create);
	ELuna::registerMethod<LuaTexture>(L, "Texture_CreateRenderTarget",	&LuaTexture::Texture_CreateRenderTarget);
	
	ELuna::registerMethod<LuaTexture>(L, "Lock",						&LuaTexture::Lock);
	ELuna::registerMethod<LuaTexture>(L, "Unlock",						&LuaTexture::Unlock);
	ELuna::registerMethod<LuaTexture>(L, "IsLock",						&LuaTexture::IsLock);
	ELuna::registerMethod<LuaTexture>(L, "SetPixel",					&LuaTexture::SetPixel);
	ELuna::registerMethod<LuaTexture>(L, "GetPixel",					&LuaTexture::GetPixel);
	ELuna::registerMethod<LuaTexture>(L, "GetWidth",					&LuaTexture::GetWidth);
	ELuna::registerMethod<LuaTexture>(L, "GetHeight",					&LuaTexture::GetHeight);
	ELuna::registerMethod<LuaTexture>(L, "FillColor",					&LuaTexture::FillColor);
	ELuna::registerMethod<LuaTexture>(L, "SaveToFile",					&LuaTexture::SaveToFile);

	ELuna::registerMethod<LuaTexture>(L, "Copy",						&LuaTexture::Copy);
	ELuna::registerMethod<LuaTexture>(L, "CopyRect",					&LuaTexture::CopyRect);
	ELuna::registerMethod<LuaTexture>(L, "Grayscale",					&LuaTexture::Grayscale);

	ELuna::registerMethod<LuaTexture>(L, "GetP",						&LuaTexture::GetP);
	ELuna::registerMethod<LuaTexture>(L, "SetP",						&LuaTexture::SetP);
	
	ELuna::registerMethod<LuaTexture>(L, "GetRefCount",					&LuaTexture::GetRefCount);
	ELuna::registerMethod<LuaTexture>(L, "AddRef",						&LuaTexture::AddRef);
	ELuna::registerMethod<LuaTexture>(L, "Release",						&LuaTexture::Release);
	ELuna::registerMethod<LuaTexture>(L, "__tostring",					&LuaTexture::__tostring);

	return 1;
}