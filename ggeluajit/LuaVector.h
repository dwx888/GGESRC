#pragma once
#include "common.h"
#include "ggemath.h"

using namespace gge;
class LuaVector //:public ggeVector
{
public:
	LuaVector() {}
	//~LuaVector(void);
	float		Dot(float x,float y,float x1,float y1);
	float		Normalize(float x,float y,lua_State* L);
	float		Length(float x,float y);
	float		LengthSquared(float x,float y);
	float		Angle(float x,float y);
	float		AngleTo(float x,float y,float x1,float y1);
	int			Sign(float x,float y,float x1,float y1);
	float		Perpendicular(float x,float y,lua_State* L);
	float		Reflect(float x,float y,float x1,float y1,lua_State* L);
	float		ToReflect(float x,float y,float x1,float y1,lua_State* L);
	float		Rotate(float x,float y,float radian,lua_State* L);
	float		Clamp(float x,float y,float max,lua_State* L);
	const char*		__tostring(){return "ggeVector";}
	//void		print(){printf("%f,%f",x,y);}

private:
	ggeVector m_obj;
};

