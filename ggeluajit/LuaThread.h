#pragma once
#include "common.h"
#include <string>

using namespace std;
struct tqueue
{
	bool ok;
	string str;
	void * ptr;
};
class LuaThread
{
public:
	LuaThread(const char* MainPath, const char* PackPath);
	~LuaThread(void);
	bool		Start(int v);
	void		Stop();
	bool		IsStop();
	bool		Send(string v,void* p);
	void		SetSleep(UINT v){m_sleep = v;}

private:
	static int			Return(lua_State *L);
	static DWORD WINAPI ThreadProc(void *pParam);

	HANDLE				m_hThread;
	lua_State *			m_L;
	CRITICAL_SECTION	m_cs;
	int					m_sleep;
	int					m_ref;//进程回调
	int					m_tref;//线程回调

	tqueue*				m_list;
	int					m_maxlist;
};

