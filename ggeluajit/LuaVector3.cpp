#include "LuaVector3.h"


LuaVector3::LuaVector3(ggeVector3 v){
	x=v.x;
	y=v.y;
	z=v.z;
}

float LuaVector3::Dot(LuaVector3 v){
	return ggeVector3::Dot(v);
}
LuaVector3 LuaVector3::Cross(LuaVector3 v){
	return ggeVector3::Cross(v);
}
float LuaVector3::Length(){
	return ggeVector3::Length();
}
float LuaVector3::LengthSquared(){
	return ggeVector3::LengthSquared();
}
float LuaVector3::Angle(LuaVector3 v){
	return ggeVector3::Angle(v);
}
int REG_GGE_Vector3(lua_State *L){
	const char* gname = "__LuaVector3";
	ELuna::registerClass<LuaVector3>(L, gname, ELuna::constructor<LuaVector3,float,float,float>);

	ELuna::registerMethod<LuaVector3>(L, "Cross",			&LuaVector3::Cross);

	ELuna::registerMethod<LuaVector3>(L, "Length",			&LuaVector3::Length);
	ELuna::registerMethod<LuaVector3>(L, "LengthSquared",	&LuaVector3::LengthSquared);
	ELuna::registerMethod<LuaVector3>(L, "Angle",			&LuaVector3::Angle);

	ELuna::registerMethod<LuaVector3>(L, "Normalize",		&ggeVector3::Normalize);
	ELuna::registerMethod<LuaVector3>(L, "Clamp",			&ggeVector3::Clamp);

	ELuna::registerMethod<LuaVector3>(L, "print",			&LuaVector3::print);
	ELuna::registerMethod<LuaVector3>(L, "__tostring",		&LuaVector3::__tostring);
	return 1;
}