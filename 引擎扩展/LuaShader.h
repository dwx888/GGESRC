#pragma once
#include "ELuna.h"
#include "ggeshader.h"
class LuaShader
{
public:
	LuaShader(void);
	~LuaShader(void);
	bool Shader_Load(const char *filename, const char *function, int psVersion);
	bool Shader_Create(const char *shaderstr, const char *function, int psVersion);
	bool SetTexture(const char *name, void* tex,UINT texState, UINT borderColor);
	bool SetFloat(const char *name, float f);
	bool SetFloatArray(const char *name, void* pf, const UINT count);
	bool SetTextureEx(UINT startRegister, void *tex, UINT texState, UINT borderColor);
	bool SetFloatEx(UINT startRegister, float f);
	bool SetFloatArrayEx(UINT startRegister, void *pf, UINT count);

	void*		GetP(){return m_shader;}
	void		SetP(void* v){m_shader = (gge::ggeShader*)v;}
	int			GetRefCount();
	void		AddRef();
	bool		Release();
private:
	gge::ggeShader* m_shader;
};

int REG_GGE_Shader(lua_State *L);