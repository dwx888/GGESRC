#include "LuaVideo.h"


LuaVideo::LuaVideo(const char *filename)
{
	m_obj = gge::Video_Load(filename);
}


LuaVideo::~LuaVideo(void)
{
	GGE_RELEASE(m_obj);
}
void	LuaVideo::Render(float x, float y){
	m_obj->Render(x,y);
}
void	LuaVideo::RenderEx(float x, float y, float rotation, float hscale, float vscale){
	m_obj->RenderEx(x,y,rotation,hscale,vscale);
}
void	LuaVideo::RenderStretch(float x1, float y1, float x2, float y2){
	m_obj->RenderStretch(x1,y1,x2,y2);
}
void	LuaVideo::Render4V(float x0, float y0, float x1, float y1, float x2, float y2, float x3, float y3){
	m_obj->Render4V(x0,y0,x1,y1,x2,y2,x3,y3);
}
void	LuaVideo::Play(){
	m_obj->Play();
}
void	LuaVideo::Pause(){
	m_obj->Pause();
}
void	LuaVideo::Resume(){
	m_obj->Resume();
}
void	LuaVideo::Stop(){
	m_obj->Stop();
}
bool	LuaVideo::IsPlaying(){
	return m_obj->IsPlaying();
}
float	LuaVideo::GetPlayingTime(){
	return m_obj->GetPlayingTime();
}
void	LuaVideo::SetLoop(bool bLoop){
	m_obj->SetLoop(bLoop);
}
bool	LuaVideo::IsLoop(){
	return m_obj->IsLoop();
}
void	LuaVideo::SetVolume(float volume){
	m_obj->SetVolume(volume);
}
float	LuaVideo::GetVolume(){
	return m_obj->GetVolume();
}
void	LuaVideo::SetZ(float z){
	m_obj->SetZ(z);
}
float	LuaVideo::GetZ(){
	return m_obj->GetZ();
}
void	LuaVideo::SetBlendMode(int blend){
	m_obj->SetBlendMode(blend);
}
int		LuaVideo::GetBlendMode(){
	return m_obj->GetBlendMode();
}
void	LuaVideo::SetHotSpot(float x, float y){
	m_obj->SetHotSpot(x,y);
}
float	LuaVideo::GetHotSpot(lua_State* L){
	float x,y;
	m_obj->GetHotSpot(&x,&y);
	lua_pushnumber(L,x);
	return y;
}
int		LuaVideo::GetWidth(){
	return m_obj->GetWidth();
}
int		LuaVideo::GetHeight(){
	return m_obj->GetHeight();
}
int REG_GGE_Video(lua_State *L){
	const char* gname = "__LuaVideo";
	ELuna::registerClass<LuaVideo>(L, gname, ELuna::constructor<LuaVideo,const char *>);

	ELuna::registerMethod<LuaVideo>(L, "Render", &LuaVideo::Render);
	ELuna::registerMethod<LuaVideo>(L, "RenderEx", &LuaVideo::RenderEx);
	ELuna::registerMethod<LuaVideo>(L, "RenderStretch", &LuaVideo::RenderStretch);
	ELuna::registerMethod<LuaVideo>(L, "Render4V", &LuaVideo::Render4V);
	ELuna::registerMethod<LuaVideo>(L, "Play", &LuaVideo::Play);
	ELuna::registerMethod<LuaVideo>(L, "Pause", &LuaVideo::Pause);
	ELuna::registerMethod<LuaVideo>(L, "Resume", &LuaVideo::Resume);
	ELuna::registerMethod<LuaVideo>(L, "Stop", &LuaVideo::Stop);
	ELuna::registerMethod<LuaVideo>(L, "IsPlaying", &LuaVideo::IsPlaying);
	ELuna::registerMethod<LuaVideo>(L, "GetPlayingTime", &LuaVideo::GetPlayingTime);
	ELuna::registerMethod<LuaVideo>(L, "SetLoop", &LuaVideo::SetLoop);
	ELuna::registerMethod<LuaVideo>(L, "IsLoop", &LuaVideo::IsLoop);
	ELuna::registerMethod<LuaVideo>(L, "SetVolume", &LuaVideo::SetVolume);
	ELuna::registerMethod<LuaVideo>(L, "GetVolume", &LuaVideo::GetVolume);
	ELuna::registerMethod<LuaVideo>(L, "SetZ", &LuaVideo::SetZ);
	ELuna::registerMethod<LuaVideo>(L, "GetZ", &LuaVideo::GetZ);
	
	ELuna::registerMethod<LuaVideo>(L, "SetBlendMode", &LuaVideo::SetBlendMode);
	ELuna::registerMethod<LuaVideo>(L, "GetBlendMode", &LuaVideo::GetBlendMode);
	ELuna::registerMethod<LuaVideo>(L, "SetHotSpot", &LuaVideo::SetHotSpot);
	ELuna::registerMethod<LuaVideo>(L, "GetHotSpot", &LuaVideo::GetHotSpot);
	ELuna::registerMethod<LuaVideo>(L, "GetWidth", &LuaVideo::GetWidth);
	ELuna::registerMethod<LuaVideo>(L, "GetHeight", &LuaVideo::GetHeight);
	return 1;
}
