#include "LuaShader.h"


LuaShader::LuaShader(void){
	m_shader = NULL;
}


LuaShader::~LuaShader(void)
{
	GGE_RELEASE(m_shader);
}
bool LuaShader::Shader_Load(const char *filename, const char *function, int psVersion)
{
	m_shader = gge::Shader_Load(filename,function,gge::PIXEL_SHADER_VERSION(psVersion));
	return !m_shader;
}
bool LuaShader::Shader_Create(const char *shaderstr, const char *function, int psVersion)
{
	m_shader = gge::Shader_Create(shaderstr,function,gge::PIXEL_SHADER_VERSION(psVersion));
	return !m_shader;
}
bool LuaShader::SetTexture(const char *name, void* tex,UINT texState, UINT borderColo)
{
	return m_shader->SetTexture(name,(gge::ggeTexture*)tex,texState,borderColo);
}
bool LuaShader::SetFloat(const char *name, float f)
{
	return m_shader->SetFloat(name,f);
}
bool LuaShader::SetFloatArray(const char *name, void* pf,const UINT count)
{
	return m_shader->SetFloatArray(name,(float*)pf,count);
}
bool LuaShader::SetTextureEx(UINT startRegister, void *tex, UINT texState, UINT borderColor)
{
	return m_shader->SetTextureEx(startRegister,(gge::ggeTexture*)tex,texState,borderColor);
}
bool LuaShader::SetFloatEx(UINT startRegister, float f)
{
	return m_shader->SetFloatEx(startRegister,f);
}
bool LuaShader::SetFloatArrayEx(UINT startRegister, void *pf, UINT count)
{
	return m_shader->SetFloatArrayEx(startRegister,(float*)pf,count);
}
//------------------------------------------------------------------------

int LuaShader::GetRefCount(){ 
	return m_shader->GetRefCount();
}
void LuaShader::AddRef(){ 
	m_shader->AddRef();
}
bool LuaShader::Release(){ 
	if (m_shader)
	{
		if (m_shader->GetRefCount()==1){
			m_shader->Release();
			m_shader = NULL;
			return true;
		}
		m_shader->Release();
	}

	return false;
}
int REG_GGE_Shader(lua_State *L){
	const char* gname = "__LuaShader";
	ELuna::registerClass<LuaShader>(L, gname, ELuna::constructor<LuaShader>);

	ELuna::registerMethod<LuaShader>(L, "Shader_Load", &LuaShader::Shader_Load);
	ELuna::registerMethod<LuaShader>(L, "Shader_Create", &LuaShader::Shader_Create);
	ELuna::registerMethod<LuaShader>(L, "SetTexture", &LuaShader::SetTexture);
	ELuna::registerMethod<LuaShader>(L, "SetFloat", &LuaShader::SetFloat);
	ELuna::registerMethod<LuaShader>(L, "SetFloatArray", &LuaShader::SetFloatArray);
	ELuna::registerMethod<LuaShader>(L, "SetTextureEx", &LuaShader::SetTextureEx);
	ELuna::registerMethod<LuaShader>(L, "SetFloatEx", &LuaShader::SetFloatEx);
	ELuna::registerMethod<LuaShader>(L, "SetFloatArrayEx", &LuaShader::SetFloatArrayEx);

	ELuna::registerMethod<LuaShader>(L, "GetP", &LuaShader::GetP);
	ELuna::registerMethod<LuaShader>(L, "SetP", &LuaShader::SetP);
	ELuna::registerMethod<LuaShader>(L, "GetRefCount", &LuaShader::GetRefCount);
	ELuna::registerMethod<LuaShader>(L, "AddRef", &LuaShader::AddRef);
	ELuna::registerMethod<LuaShader>(L, "Release", &LuaShader::Release);

	return 1;
}