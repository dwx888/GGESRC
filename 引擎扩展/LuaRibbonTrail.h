#pragma once
#include "ELuna.h"
#include "ggeribbontrail.h"
class LuaRibbonTrail
{
public:
	LuaRibbonTrail(UINT startCol, UINT endColor, float maxW, float minW, float time, UINT maxNodes, UINT nodeLen);
	~LuaRibbonTrail(void);
	//void				Copy(UINT trail);
	void*				Clone();
	void				Update(float dt);
	void				Render();
	void				Stop();
	void				MoveTo(float x, float y, bool bMoveNodes);
	float				GetPosition(lua_State* L);
	void				Transpose(float x, float y);
	float				GetTransposition(lua_State* L);
	void				TrackBoundingBox(bool bTrack);
	void				SetMaxNodeNum(UINT num);
	UINT				GetMaxNodeNum();
	void				SetLifeTime(float time);
	float				GetLifeTime();
	void				SetStartColor(UINT color);
	UINT				GetStartColor();
	void				SetEndColor(UINT color);
	UINT				GetEndColor();
	void				SetMaxWidth(float width);
	float				GetMaxWidth();
	void				SetMinWidth(float width);
	float				GetMinWidth();
	void				SetNodeLength(UINT len);
	UINT				GetNodeLength();
	void				TrackMovingDirection(bool b);
	bool				IsTrackMovingDirection();
	void				SetDirection(float angle);
	float				GetDirection();
	void				SetTexture(void* texture);
	void*				GetTexture();
	void				SetTextureRect(float x, float y, float width, float height);
	float				GetTextureRect(lua_State* L);
	void				SetZ(float z);
	float				GetZ();
	void				SetBlendMode(int blend);
	int					GetBlendMode();
	void				GetBoundingBox(void* rect);

	void		SetTextureFilter(bool b){m_Filter = b?gge::BLEND_TEXFILTER:gge::BLEND_NOTEXFILTER;}
	bool		IsTextureFilter(){return m_Filter==gge::BLEND_TEXFILTER;}
	void*		GetP(){return m_trail;}
	void		SetP(void* v){m_trail = (gge::ggeRibbonTrail *)v;}
	int			GetRefCount(){return m_trail->GetRefCount();}
	void		AddRef(){m_trail->AddRef();}
	bool		Release();
private:
	gge::ggeRibbonTrail *m_trail;
	int m_Filter;
};

int REG_GGE_RibbonTrail(lua_State *L);

