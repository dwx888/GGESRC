#include "LuaRibbonTrail.h"


LuaRibbonTrail::LuaRibbonTrail(UINT startCol, UINT endColor, float maxW, float minW, float time, UINT maxNodes, UINT nodeLen)
{
	m_trail = gge::RibbonTrail_Create(startCol,endColor,maxW,minW,time,maxNodes,nodeLen);
}
LuaRibbonTrail::~LuaRibbonTrail(void)
{
	GGE_RELEASE(m_trail);
}
void* LuaRibbonTrail::Clone(){ 
	return m_trail->Clone();
}
void LuaRibbonTrail::Update(float dt){ 
	m_trail->Update(dt);
}
void LuaRibbonTrail::Render(){ 
	m_trail->Render();
}
void LuaRibbonTrail::Stop(){ 
	m_trail->Stop();
}
void LuaRibbonTrail::MoveTo(float x, float y, bool bMoveNodes){ 
	m_trail->MoveTo(x,y,bMoveNodes);
}
float LuaRibbonTrail::GetPosition(lua_State* L){ 
	float x,y;
	m_trail->GetPosition(&x,&y);
	lua_pushnumber(L,x);
	return y;
}
void LuaRibbonTrail::Transpose(float x, float y){ 
	m_trail->Transpose(x,y);
}
void LuaRibbonTrail::TrackBoundingBox(bool bTrack){ 
	m_trail->TrackBoundingBox(bTrack);
}
float LuaRibbonTrail::GetTransposition(lua_State* L){ 
	float x,y;
	m_trail->GetTransposition(&x,&y);
	lua_pushnumber(L,x);
	return y;
}
void LuaRibbonTrail::SetMaxNodeNum(UINT num){ 
	m_trail->SetMaxNodeNum(num);
}
void LuaRibbonTrail::SetLifeTime(float time){ 
	m_trail->SetLifeTime(time);
}
void LuaRibbonTrail::SetStartColor(UINT color){ 
	m_trail->SetStartColor(color);
}
void LuaRibbonTrail::SetEndColor(UINT color){ 
	m_trail->SetEndColor(color);
}
void LuaRibbonTrail::SetMaxWidth(float width){ 
	m_trail->SetMaxWidth(width);
}
void LuaRibbonTrail::SetMinWidth(float width){ 
	m_trail->SetMinWidth(width);
}
void LuaRibbonTrail::SetNodeLength(UINT len){ 
	m_trail->SetNodeLength(len);
}
void LuaRibbonTrail::TrackMovingDirection(bool b){ 
	m_trail->TrackMovingDirection(b);
}
void LuaRibbonTrail::SetDirection(float angle){ 
	m_trail->SetDirection(angle);
}

void LuaRibbonTrail::SetTexture(void* texture){ 
	m_trail->SetTexture((gge::ggeTexture*)texture);
}
void LuaRibbonTrail::SetTextureRect(float x, float y, float width, float height){ 
	m_trail->SetTextureRect(x,y,width,height);
}
void LuaRibbonTrail::SetZ(float z){ 
	m_trail->SetZ(z);
}

void LuaRibbonTrail::SetBlendMode(int blend){ 
	m_trail->SetBlendMode(blend);
}
UINT LuaRibbonTrail::GetMaxNodeNum(){ 
	return m_trail->GetMaxNodeNum();
}
float LuaRibbonTrail::GetLifeTime(){ 
	return m_trail->GetLifeTime();
}
UINT LuaRibbonTrail::GetStartColor(){ 
	return m_trail->GetStartColor();
}
UINT LuaRibbonTrail::GetEndColor(){ 
	return m_trail->GetEndColor();
}
float LuaRibbonTrail::GetMaxWidth(){ 
	return m_trail->GetMaxWidth();
}
float LuaRibbonTrail::GetMinWidth(){ 
	return m_trail->GetMinWidth();
}
UINT LuaRibbonTrail::GetNodeLength(){ 
	return m_trail->GetNodeLength();
}
bool LuaRibbonTrail::IsTrackMovingDirection(){ 
	return m_trail->IsTrackMovingDirection();
}
float LuaRibbonTrail::GetDirection(){ 
	return m_trail->GetDirection();
}
void* LuaRibbonTrail::GetTexture(){ 
	return m_trail->GetTexture();
}
float LuaRibbonTrail::GetTextureRect(lua_State* L){ 
	float x,y,width,height;
	m_trail->GetTextureRect(&x,&y,&width,&height);
	lua_pushnumber(L,x);
	lua_pushnumber(L,y);
	lua_pushnumber(L,width);
	return height;
}
float LuaRibbonTrail::GetZ(){ 
	return m_trail->GetZ();
}

int LuaRibbonTrail::GetBlendMode(){ 
	return m_trail->GetBlendMode();
}

void LuaRibbonTrail::GetBoundingBox(void* rect){ 
	m_trail->GetBoundingBox((gge::ggeRect*)rect);
}


bool LuaRibbonTrail::Release(){ 
	
	if (m_trail->GetRefCount()==1){
		m_trail->Release();
		m_trail = NULL;
		return false;
	}
	m_trail->Release();
	return true;
}
int REG_GGE_RibbonTrail(lua_State *L){
	const char* gname = "__LuaRibbonTrail";
	ELuna::registerClass<LuaRibbonTrail>(L, gname, ELuna::constructor<LuaRibbonTrail,UINT, UINT, float, float ,float ,UINT ,UINT>);

	//ELuna::registerMethod<LuaRibbonTrail>(L, "Copy", &LuaRibbonTrail::Copy);
	ELuna::registerMethod<LuaRibbonTrail>(L, "Clone", &LuaRibbonTrail::Clone);
	ELuna::registerMethod<LuaRibbonTrail>(L, "Update", &LuaRibbonTrail::Update);
	ELuna::registerMethod<LuaRibbonTrail>(L, "Render", &LuaRibbonTrail::Render);
	ELuna::registerMethod<LuaRibbonTrail>(L, "Stop", &LuaRibbonTrail::Stop);
	ELuna::registerMethod<LuaRibbonTrail>(L, "MoveTo", &LuaRibbonTrail::MoveTo);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetPosition", &LuaRibbonTrail::GetPosition,2);
	ELuna::registerMethod<LuaRibbonTrail>(L, "Transpose", &LuaRibbonTrail::Transpose);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetTransposition", &LuaRibbonTrail::GetTransposition,2);
	ELuna::registerMethod<LuaRibbonTrail>(L, "TrackBoundingBox", &LuaRibbonTrail::TrackBoundingBox);
	ELuna::registerMethod<LuaRibbonTrail>(L, "SetMaxNodeNum", &LuaRibbonTrail::SetMaxNodeNum);
	ELuna::registerMethod<LuaRibbonTrail>(L, "SetLifeTime", &LuaRibbonTrail::SetLifeTime);
	ELuna::registerMethod<LuaRibbonTrail>(L, "SetStartColor", &LuaRibbonTrail::SetStartColor);
	ELuna::registerMethod<LuaRibbonTrail>(L, "SetEndColor", &LuaRibbonTrail::SetEndColor);
	ELuna::registerMethod<LuaRibbonTrail>(L, "SetMaxWidth", &LuaRibbonTrail::SetMaxWidth);
	ELuna::registerMethod<LuaRibbonTrail>(L, "SetMinWidth", &LuaRibbonTrail::SetMinWidth);
	ELuna::registerMethod<LuaRibbonTrail>(L, "SetNodeLength", &LuaRibbonTrail::SetNodeLength);
	ELuna::registerMethod<LuaRibbonTrail>(L, "TrackMovingDirection", &LuaRibbonTrail::TrackMovingDirection);

	ELuna::registerMethod<LuaRibbonTrail>(L, "SetDirection", &LuaRibbonTrail::SetDirection);
	ELuna::registerMethod<LuaRibbonTrail>(L, "SetTexture", &LuaRibbonTrail::SetTexture);
	ELuna::registerMethod<LuaRibbonTrail>(L, "SetTextureRect", &LuaRibbonTrail::SetTextureRect);
	ELuna::registerMethod<LuaRibbonTrail>(L, "SetZ", &LuaRibbonTrail::SetZ);
	
	ELuna::registerMethod<LuaRibbonTrail>(L, "SetBlendMode", &LuaRibbonTrail::SetBlendMode);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetMaxNodeNum", &LuaRibbonTrail::GetMaxNodeNum);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetLifeTime", &LuaRibbonTrail::GetLifeTime);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetStartColor", &LuaRibbonTrail::GetStartColor);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetEndColor", &LuaRibbonTrail::GetEndColor);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetMaxWidth", &LuaRibbonTrail::GetMaxWidth);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetMinWidth", &LuaRibbonTrail::GetMinWidth);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetNodeLength", &LuaRibbonTrail::GetNodeLength);
	ELuna::registerMethod<LuaRibbonTrail>(L, "IsTrackMovingDirection", &LuaRibbonTrail::IsTrackMovingDirection);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetDirection", &LuaRibbonTrail::GetDirection);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetTexture", &LuaRibbonTrail::GetTexture);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetTextureRect", &LuaRibbonTrail::GetTextureRect,4);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetZ", &LuaRibbonTrail::GetZ);
	
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetBlendMode", &LuaRibbonTrail::GetBlendMode);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetBoundingBox", &LuaRibbonTrail::GetBoundingBox);

	ELuna::registerMethod<LuaRibbonTrail>(L, "SetTextureFilter", &LuaRibbonTrail::SetTextureFilter);
	ELuna::registerMethod<LuaRibbonTrail>(L, "IsTextureFilter", &LuaRibbonTrail::IsTextureFilter);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetP", &LuaRibbonTrail::GetP);
	ELuna::registerMethod<LuaRibbonTrail>(L, "SetP", &LuaRibbonTrail::SetP);
	ELuna::registerMethod<LuaRibbonTrail>(L, "GetRefCount", &LuaRibbonTrail::GetRefCount);
	ELuna::registerMethod<LuaRibbonTrail>(L, "AddRef", &LuaRibbonTrail::AddRef);
	ELuna::registerMethod<LuaRibbonTrail>(L, "Release", &LuaRibbonTrail::Release);

	return 1;
}