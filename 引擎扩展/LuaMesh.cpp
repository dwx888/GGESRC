#include "LuaMesh.h"


LuaMesh::LuaMesh(int cols, int rows){
	m_obj = gge::Mesh_Create(cols, rows);
}


LuaMesh::~LuaMesh(void){
	GGE_RELEASE(m_obj);
}

void* LuaMesh::Clone(){
	return m_obj->Clone();
}
void LuaMesh::Render(float x, float y){
	m_obj->Render(x,y);
}
void LuaMesh::Clear(UINT color, float z){
	m_obj->Clear(color,z);
}
void LuaMesh::SetTexture(void *texture){
	m_obj->SetTexture((gge::ggeTexture*)texture);
}
void* LuaMesh::GetTexture(){
	return m_obj->GetTexture();
}
void LuaMesh::SetTextureRect(float x, float y, float width, float height){
	m_obj->SetTextureRect(x,y,width,height);
}
float LuaMesh::GetTextureRect(lua_State* L){
	float x,y,w,h;
	m_obj->GetTextureRect(&x,&y,&w,&h);
	lua_pushnumber(L,x);
	lua_pushnumber(L,y);
	lua_pushnumber(L,w);
	return h;
}
void LuaMesh::SetBlendMode(int blend){
	m_obj->SetBlendMode(blend);
}
int LuaMesh::GetBlendMode(){
	return m_obj->GetBlendMode();
}
void LuaMesh::SetZ(int col, int row, float z){
	m_obj->SetZ(col,row,z);
}
float LuaMesh::GetZ(int col, int row){
	return m_obj->GetZ(col,row);
}
void LuaMesh::SetColor(int col, int row, UINT color){
	m_obj->SetColor(col,row,color);
}
UINT LuaMesh::GetColor(int col, int row){
	return m_obj->GetColor(col,row);
}
void LuaMesh::SetDisplacement(int col, int row, float dx, float dy, int ref){
	m_obj->SetDisplacement(col,row,dx,dy,ref);
}
float LuaMesh::GetDisplacement(int col, int row, int ref,lua_State* L){
	float dx,dy;
	m_obj->GetDisplacement(col,row,&dx,&dy,ref);
	lua_pushnumber(L,dx);
	return dy;
}
int LuaMesh::GetRows(){
	return m_obj->GetRows();
}
int LuaMesh::GetCols(){
	return m_obj->GetCols();
}
void* LuaMesh::GetNodeList(){
	return m_obj->GetNodeList();
}

int REG_GGE_Mesh(lua_State* L){
	const char* gname = "__ggeMesh";
	ELuna::registerClass<LuaMesh>(L, gname,  ELuna::constructor<LuaMesh,int,int>);

	//ELuna::registerMethod<LuaMesh>(L, "Copy", &LuaMesh::Copy);
	ELuna::registerMethod<LuaMesh>(L, "Clone", &LuaMesh::Clone);
	ELuna::registerMethod<LuaMesh>(L, "Render", &LuaMesh::Render);
	ELuna::registerMethod<LuaMesh>(L, "Clear", &LuaMesh::Clear);
	ELuna::registerMethod<LuaMesh>(L, "SetTexture", &LuaMesh::SetTexture);
	ELuna::registerMethod<LuaMesh>(L, "GetTexture", &LuaMesh::GetTexture);
	ELuna::registerMethod<LuaMesh>(L, "SetTextureRect", &LuaMesh::SetTextureRect);
	ELuna::registerMethod<LuaMesh>(L, "GetTextureRect", &LuaMesh::GetTextureRect,4);
	ELuna::registerMethod<LuaMesh>(L, "SetBlendMode", &LuaMesh::SetBlendMode);
	ELuna::registerMethod<LuaMesh>(L, "GetBlendMode", &LuaMesh::GetBlendMode);
	ELuna::registerMethod<LuaMesh>(L, "SetZ", &LuaMesh::SetZ);
	ELuna::registerMethod<LuaMesh>(L, "GetZ", &LuaMesh::GetZ);
	ELuna::registerMethod<LuaMesh>(L, "SetColor", &LuaMesh::SetColor);
	ELuna::registerMethod<LuaMesh>(L, "GetColor", &LuaMesh::GetColor);
	ELuna::registerMethod<LuaMesh>(L, "SetDisplacement", &LuaMesh::SetDisplacement);
	ELuna::registerMethod<LuaMesh>(L, "GetDisplacement", &LuaMesh::GetDisplacement,2);
	ELuna::registerMethod<LuaMesh>(L, "GetRows", &LuaMesh::GetRows);
	ELuna::registerMethod<LuaMesh>(L, "GetCols", &LuaMesh::GetCols);
	ELuna::registerMethod<LuaMesh>(L, "GetNodeList", &LuaMesh::GetNodeList);
	return 1;
}
