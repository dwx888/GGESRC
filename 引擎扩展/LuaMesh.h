#pragma once
#include "ELuna.h"
#include "ggemesh.h"
class LuaMesh
{
public:
	LuaMesh(int cols, int rows);
	~LuaMesh(void);
	void*		Clone();
	void		Render(float x, float y);
	void		Clear(UINT color, float z);
	void		SetTexture(void *texture);
	void*		GetTexture();
	void		SetTextureRect(float x, float y, float width, float height);
	float		GetTextureRect(lua_State* L);
	void		SetBlendMode(int blend);
	int			GetBlendMode();
	void		SetZ(int col, int row, float z);
	float		GetZ(int col, int row);
	void		SetColor(int col, int row, UINT color);
	UINT		GetColor(int col, int row);
	void		SetDisplacement(int col, int row, float dx, float dy, int ref);
	float		GetDisplacement(int col, int row, int ref,lua_State* L);
	int			GetRows();
	int			GetCols();
	void*		GetNodeList();
private:
	gge::ggeMesh* m_obj;
};

int REG_GGE_Mesh(lua_State* L);