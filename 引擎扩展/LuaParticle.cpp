#include "LuaParticle.h"


LuaParticle::LuaParticle(void):m_par(NULL){}
LuaParticle::~LuaParticle(void)
{
	GGE_RELEASE(m_par);
}
//void LuaParticle::Copy(gUInt par){
//	m_par->Copy((gge::ggeParticle*)par);
//}
void* LuaParticle::Clone(){
	return m_par->Clone();
}
void LuaParticle::Update(float dt){
	m_par->Update(dt);
}
void LuaParticle::Render(){
	m_par->Render();
}
void LuaParticle::FireAt(float x, float y){
	m_par->FireAt(x,y);
}
void LuaParticle::Fire(){
	m_par->Fire();
}
void LuaParticle::Stop(bool bKillParticles){
	m_par->Stop(bKillParticles);
}
void LuaParticle::MoveTo(float x, float y, bool bMoveParticles){
	m_par->MoveTo(x,y,bMoveParticles);
}
float LuaParticle::GetPosition(lua_State* L){
	float x,y;
	m_par->GetPosition(&x,&y);
	lua_pushnumber(L,x);
	return y;
}
void LuaParticle::Transpose(float x, float y){
	m_par->Transpose(x,y);
}
float LuaParticle::GetTransposition(lua_State* L){
	float x,y;
	m_par->GetTransposition(&x,&y);
	lua_pushnumber(L,x);
	return y;
}
void LuaParticle::TrackBoundingBox(bool bTrack){
	m_par->TrackBoundingBox(bTrack);
}
void LuaParticle::SetParticleInfo(void* info){
	gge::ggeParticleInfo* pinfo = (gge::ggeParticleInfo*)info;
	m_par->SetParticleInfo(*pinfo);
}
void* LuaParticle::GetParticleInfo(){
	return &m_par->GetParticleInfo();
}
void LuaParticle::SetSprite(void* sprite){
	m_par->SetSprite((gge::ggeSprite*)sprite);
}
void* LuaParticle::GetSprite(){
	return m_par->GetSprite();
}
void LuaParticle::SetAnimation(void *ani){
	m_par->SetAnimation((gge::ggeAnimation*)ani);
}
void* LuaParticle::GetAnimation(){
	return m_par->GetAnimation();
}
int LuaParticle::GetParticleCount(){
	return m_par->GetParticleCount();
}
int LuaParticle::GetParticlesAlive(){
	return m_par->GetParticlesAlive();
}
float LuaParticle::GetAge(){
	return m_par->GetAge();
}


void LuaParticle::GetBoundingBox(void* rect){
	m_par->GetBoundingBox((gge::ggeRect*)rect);
}


void LuaParticle::SetP(void* p){
	m_par = (gge::ggeParticle*)p;
}

int REG_GGE_Particle(lua_State * L){
	const char* gname = "__ggeParticle";
	ELuna::registerClass<LuaParticle>(L, gname, ELuna::constructor<LuaParticle>);

	//ELuna::registerMethod<LuaParticle>(L, "Copy", &LuaParticle::Copy);
	ELuna::registerMethod<LuaParticle>(L, "Clone", &LuaParticle::Clone);
	ELuna::registerMethod<LuaParticle>(L, "Update", &LuaParticle::Update);
	ELuna::registerMethod<LuaParticle>(L, "Render", &LuaParticle::Render);
	ELuna::registerMethod<LuaParticle>(L, "FireAt", &LuaParticle::FireAt);
	ELuna::registerMethod<LuaParticle>(L, "Fire", &LuaParticle::Fire);
	ELuna::registerMethod<LuaParticle>(L, "Stop", &LuaParticle::Stop);
	ELuna::registerMethod<LuaParticle>(L, "MoveTo", &LuaParticle::MoveTo);
	
	ELuna::registerMethod<LuaParticle>(L, "Transpose", &LuaParticle::Transpose);
	ELuna::registerMethod<LuaParticle>(L, "TrackBoundingBox", &LuaParticle::TrackBoundingBox);
	ELuna::registerMethod<LuaParticle>(L, "SetParticleInfo", &LuaParticle::SetParticleInfo);
	ELuna::registerMethod<LuaParticle>(L, "SetSprite", &LuaParticle::SetSprite);
	ELuna::registerMethod<LuaParticle>(L, "GetParticleCount", &LuaParticle::GetParticleCount);
	ELuna::registerMethod<LuaParticle>(L, "GetParticlesAlive", &LuaParticle::GetParticlesAlive);
	ELuna::registerMethod<LuaParticle>(L, "GetAge", &LuaParticle::GetAge);
	ELuna::registerMethod<LuaParticle>(L, "SetAnimation", &LuaParticle::SetAnimation);
	ELuna::registerMethod<LuaParticle>(L, "GetAnimation", &LuaParticle::GetAnimation);
	ELuna::registerMethod<LuaParticle>(L, "GetPosition", &LuaParticle::GetPosition,2);
	ELuna::registerMethod<LuaParticle>(L, "GetTransposition", &LuaParticle::GetTransposition,2);
	ELuna::registerMethod<LuaParticle>(L, "GetBoundingBox", &LuaParticle::GetBoundingBox);
	ELuna::registerMethod<LuaParticle>(L, "GetParticleInfo", &LuaParticle::GetParticleInfo);
	ELuna::registerMethod<LuaParticle>(L, "GetSprite", &LuaParticle::GetSprite);

	ELuna::registerMethod<LuaParticle>(L, "SetP", &LuaParticle::SetP);

	return 1;
}