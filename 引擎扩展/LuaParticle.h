#pragma once
#include "ELuna.h"
#include "ggeparticle.h"

class LuaParticle
{
public:
	LuaParticle(void);
	~LuaParticle(void);
	//void				Copy(gUInt par);
	void*		Clone();
	void		Update(float dt);
	void		Render();
	void		FireAt(float x, float y);
	void		Fire();
	void		Stop(bool bKillParticles);
	void		MoveTo(float x, float y, bool bMoveParticles);
	float		GetPosition(lua_State* L);
	void		Transpose(float x, float y);
	float		GetTransposition(lua_State* L);
	void		TrackBoundingBox(bool bTrack);
	void		SetParticleInfo(void* info);
	void*		GetParticleInfo();
	void		SetSprite(void* sprite);
	void*		GetSprite();
	void		SetAnimation(void *ani);
	void*		GetAnimation();
	int			GetParticleCount();
	int			GetParticlesAlive();
	float		GetAge();

	
	void		GetBoundingBox(void* rect);
	
	
	void		SetP(void* p);

private:
	gge::ggeParticle* m_par;
};

int REG_GGE_Particle(lua_State * L);