#pragma once
#include "ELuna.h"
#include "ggevideo.h"
class LuaVideo
{
public:
	LuaVideo(const char *filename);
	~LuaVideo(void);
	void	Render(float x, float y);
	void	RenderEx(float x, float y, float rotation, float hscale, float vscale);
	void	RenderStretch(float x1, float y1, float x2, float y2);
	void	Render4V(float x0, float y0, float x1, float y1, float x2, float y2, float x3, float y3);
	void	Play();
	void	Pause();
	void	Resume();
	void	Stop();
	bool	IsPlaying();
	float	GetPlayingTime();
	void	SetLoop(bool bLoop);
	bool	IsLoop();
	void	SetVolume(float volume);
	float	GetVolume();
	void	SetZ(float z);
	float	GetZ();
	void	SetBlendMode(int blend);
	int		GetBlendMode();
	void	SetHotSpot(float x, float y);
	float	GetHotSpot(lua_State* L);
	int		GetWidth();
	int		GetHeight();
private:
	gge::ggeVideo* m_obj;
};

int REG_GGE_Video(lua_State *L);