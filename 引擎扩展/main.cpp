#include "ELuna.h"
#include "LuaShader.h"
#include "LuaVideo.h"
#include "LuaMesh.h"
#include "LuaParticle.h"
#include "LuaRibbonTrail.h"
#pragma comment(lib, "../lib/lua51")
#pragma comment(lib, "../lib/galaxy2d")

#define GGEEXP extern "C" __declspec(dllexport)



GGEEXP int luaopen_gge_shader(lua_State* L) 
{
	REG_GGE_Shader(L);
	return 1;
}
GGEEXP int luaopen_gge_video(lua_State* L) 
{
	REG_GGE_Video(L);
	return 1;
}
GGEEXP int luaopen_gge_mesh(lua_State* L) 
{
	REG_GGE_Mesh(L);
	return 1;
}
GGEEXP int luaopen_gge_particle(lua_State* L) 
{
	REG_GGE_Particle(L);
	return 1;
}
GGEEXP int luaopen_gge_ribbontrail(lua_State* L) 
{
	REG_GGE_RibbonTrail(L);
	return 1;
}