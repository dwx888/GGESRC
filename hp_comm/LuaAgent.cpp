
#include "LuaAgent.h"


hpagent::hpagent(void* cs,lua_State* L)
{
	m_ref	= 0;
	m_pull	= NULL;
	m_pack	= NULL;
	m_cs	= (LPCRITICAL_SECTION)cs;
	m_L	= L;
}

hpagent::~hpagent(void)
{
	if (HasStarted())
		Stop();
	if(m_ref)
		luaL_unref(m_L, LUA_REGISTRYINDEX, m_ref);
	if(m_pull)
		HP_Destroy_TcpPullAgent(m_pull);
	else if(m_pack)
		HP_Destroy_TcpPackAgent(m_pack);
}
//TcpAgent
void hpagent::Create_TcpAgent()
{
	m_ref	= luaL_ref(m_L, LUA_REGISTRYINDEX);//lua回调地址
	m_ita	= HP_Create_TcpAgent(this);
	m_ics	= (IComplexSocket*)m_ita; 
}
//TcpPackAgent
void hpagent::Create_TcpPackAgent()
{
	m_ref	= luaL_ref(m_L, LUA_REGISTRYINDEX);//lua回调地址
	m_pack	= HP_Create_TcpPackAgent(this);
	m_ita	= (ITcpAgent*)m_pack;
	m_ics	= (IComplexSocket*)m_pack; 
}
BOOL hpagent::SendPack(CONNID dwConnID,ELuna::LuaString Data)
{
	return m_pack->Send(dwConnID,(BYTE *)Data.str,Data.len);
}
void hpagent::SetMaxPackSize(DWORD dwMaxPackSize)
{
	m_pack->SetMaxPackSize(dwMaxPackSize);
}
void hpagent::SetPackHeaderFlag(USHORT usPackHeaderFlag)
{
	m_pack->SetPackHeaderFlag(usPackHeaderFlag);
}
DWORD hpagent::GetMaxPackSize()
{
	return m_pack->GetMaxPackSize();
}
USHORT hpagent::GetPackHeaderFlag()
{
	return m_pack->GetPackHeaderFlag();
}
//TcpPullAgent
void hpagent::Create_TcpPullAgent()
{
	m_ref	= luaL_ref(m_L, LUA_REGISTRYINDEX);//lua回调地址
	m_pull	= HP_Create_TcpPullAgent(this);
	m_ita	= (ITcpAgent*)m_pull;
	m_ics	= (IComplexSocket*)m_pull; 
}
int hpagent::Fetch(CONNID dwConnID, DWORD pBuffer, int iLength)
{
	return (int)m_pull->Fetch(dwConnID,(BYTE *)pBuffer,iLength);
}
//ITcpAgent
void hpagent::SetReuseAddress	(BOOL bReuseAddress){
	m_ita->SetReuseAddress(bReuseAddress);
}
BOOL hpagent::IsReuseAddress		()	{
	return m_ita->IsReuseAddress();
}
void hpagent::SetSocketBufferSize(DWORD v)
{
	m_ita->SetSocketBufferSize(v);
}
void hpagent::SetKeepAliveTime(DWORD v)
{
	m_ita->SetKeepAliveTime(v);
}
void hpagent::SetKeepAliveInterval(DWORD v)
{
	m_ita->SetKeepAliveInterval(v);
}
DWORD hpagent::GetSocketBufferSize()
{
	return m_ita->GetSocketBufferSize();
}
DWORD hpagent::GetKeepAliveTime()
{
	return m_ita->GetKeepAliveTime();
}
DWORD hpagent::GetKeepAliveInterval()
{
	return m_ita->GetKeepAliveInterval();
}
//IAgent
BOOL hpagent::Start(LPCTSTR lpszBindAddress, BOOL bAsyncConnect)
{
	return m_ita->Start(lpszBindAddress,bAsyncConnect);	
}
BOOL hpagent::Connect(LPCTSTR lpszRemoteAddress, USHORT usPort)
{
	//CONNID pdwConnID;
	return m_ita->Connect(lpszRemoteAddress,usPort);
}
//PULL
EnHandleResult hpagent::OnReceive(ITcpAgent* pSender,CONNID dwConnID, int iLength)
{
	//printf("55\n");
	if (m_pull)
	{
		EnterCriticalSection(m_cs);
		lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
		lua_getfield(m_L,-1,"OnReceive");
		lua_pushvalue(m_L,-2);
		lua_pushinteger(m_L,dwConnID);
		lua_pushinteger(m_L,iLength);
		lua_call(m_L,3,1);
		int r = lua_tointeger(m_L,-1);
		lua_pop(m_L,2);
		LeaveCriticalSection(m_cs);
		return EnHandleResult(r);
	}
	return HR_IGNORE;
}
//PACK
EnHandleResult hpagent::OnReceive(ITcpAgent* pSender,CONNID dwConnID, const BYTE* pData, int iLength)
{
	if (!m_pull)
	{
		EnterCriticalSection(m_cs);
		lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
		lua_getfield(m_L,-1,"OnReceive");
		lua_pushvalue(m_L,-2);
		lua_pushinteger(m_L,dwConnID);
		if (m_pack){
			lua_pushlstring(m_L,(const char*)pData,iLength);
			lua_call(m_L,3,1);
		} else{
			lua_pushlightuserdata(m_L,(void*)pData);
			lua_pushinteger(m_L,iLength);
			lua_call(m_L,4,1);
		}
		int r = lua_tointeger(m_L,-1);
		lua_pop(m_L,2);
		LeaveCriticalSection(m_cs);
		return EnHandleResult(r);
	}
	return HR_IGNORE;
}
EnHandleResult hpagent::OnSend(ITcpAgent* pSender,CONNID dwConnID, const BYTE* pData, int iLength)
{
	if (HasStarted()){
		EnterCriticalSection(m_cs);
		lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
		lua_getfield(m_L,-1,"OnSend");
		lua_pushvalue(m_L,-2);
		lua_pushinteger(m_L,dwConnID);
		lua_pushlightuserdata(m_L,(void*)pData);
		lua_pushinteger(m_L,iLength);
		lua_call(m_L,4,1);
		int r = lua_tointeger(m_L,-1);
		lua_pop(m_L,2);
		LeaveCriticalSection(m_cs);
		return EnHandleResult(r);
	}
	return HR_OK;
}
EnHandleResult hpagent::OnPrepareConnect(ITcpAgent* pSender,CONNID dwConnID, SOCKET socket)
{
	EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnPrepareConnect");
	lua_pushvalue(m_L,-2);
	lua_pushinteger(m_L,dwConnID);
	lua_pushinteger(m_L,socket);
	lua_call(m_L,3,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	LeaveCriticalSection(m_cs);
	return EnHandleResult(r);
}
EnHandleResult hpagent::OnConnect(ITcpAgent* pSender,CONNID dwConnID)
{
	EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnConnect");
	lua_pushvalue(m_L,-2);
	lua_pushinteger(m_L,dwConnID);
	lua_call(m_L,2,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	LeaveCriticalSection(m_cs);
	return EnHandleResult(r);
}
EnHandleResult hpagent::OnShutdown(ITcpAgent* pSender)
{
	EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnShutdown");
	lua_pushvalue(m_L,-2);
	lua_call(m_L,1,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	LeaveCriticalSection(m_cs);
	return EnHandleResult(r);
}
EnHandleResult hpagent::OnClose(ITcpAgent* pSender,CONNID dwConnID, EnSocketOperation enOperation, int iErrorCode)
{
	if (HasStarted())//Stop阻塞
		EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnClose");
	lua_pushvalue(m_L,-2);
	lua_pushinteger(m_L,dwConnID);
	lua_pushinteger(m_L,enOperation);
	lua_pushinteger(m_L,iErrorCode);
	lua_call(m_L,4,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	if (HasStarted())
		LeaveCriticalSection(m_cs);
	return EnHandleResult(r);
}
int REG_HPAgent(lua_State * L){
	const char* gname = "__HPAgent";
	ELuna::registerClass<hpagent>(L, gname, ELuna::constructor<hpagent,void*,lua_State*>);
	//TcpPullAgent------------------------------------------------------------------------------------------------------------
	ELuna::registerMethod<hpagent>(L, "Create_TcpPullAgent",			&hpagent::Create_TcpPullAgent);
	ELuna::registerMethod<hpagent>(L, "Fetch",							&hpagent::Fetch);
	//TcpPackAgent------------------------------------------------------------------------------------------------------------
	ELuna::registerMethod<hpagent>(L, "SendPack",						&hpagent::SendPack);
	ELuna::registerMethod<hpagent>(L, "Create_TcpPackAgent",			&hpagent::Create_TcpPackAgent);
	ELuna::registerMethod<hpagent>(L, "SetMaxPackSize",					&hpagent::SetMaxPackSize);
	ELuna::registerMethod<hpagent>(L, "SetPackHeaderFlag",				&hpagent::SetPackHeaderFlag);
	ELuna::registerMethod<hpagent>(L, "GetMaxPackSize",					&hpagent::GetMaxPackSize);
	ELuna::registerMethod<hpagent>(L, "GetPackHeaderFlag",				&hpagent::GetPackHeaderFlag);
	//TcpAgent
	ELuna::registerMethod<hpagent>(L, "Create_TcpAgent",				&hpagent::Create_TcpAgent);
	//ITcpAgent------------------------------------------------------------------------------------------------------------
	ELuna::registerMethod<hpagent>(L, "SetReuseAddress",				&hpagent::SetReuseAddress);
	ELuna::registerMethod<hpagent>(L, "IsReuseAddress",					&hpagent::IsReuseAddress);
	ELuna::registerMethod<hpagent>(L, "SetSocketBufferSize",			&hpagent::SetSocketBufferSize);
	ELuna::registerMethod<hpagent>(L, "SetKeepAliveTime",				&hpagent::SetKeepAliveTime);
	ELuna::registerMethod<hpagent>(L, "SetKeepAliveInterval",			&hpagent::SetKeepAliveInterval);
	ELuna::registerMethod<hpagent>(L, "GetSocketBufferSize",			&hpagent::GetSocketBufferSize);
	ELuna::registerMethod<hpagent>(L, "GetKeepAliveTime",				&hpagent::GetKeepAliveTime);
	ELuna::registerMethod<hpagent>(L, "GetKeepAliveInterval",			&hpagent::GetKeepAliveInterval);
	//IAgent------------------------------------------------------------------------------------------------------------
	ELuna::registerMethod<hpagent>(L, "Start",							&hpagent::Start);
	ELuna::registerMethod<hpagent>(L, "Connect",						&hpagent::Connect);
	//IComplexSocket
	ELuna::registerMethod<hpagent>(L, "Stop",							&hpsocket::Stop);
	ELuna::registerMethod<hpagent>(L, "Send",							&hpsocket::Send);
	ELuna::registerMethod<hpagent>(L, "Disconnect",						&hpsocket::Disconnect);
	ELuna::registerMethod<hpagent>(L, "DisconnectLongConnections",		&hpsocket::DisconnectLongConnections);
	ELuna::registerMethod<hpagent>(L, "DisconnectSilenceConnections",	&hpsocket::DisconnectSilenceConnections);
	ELuna::registerMethod<hpagent>(L, "HasStarted",						&hpsocket::HasStarted);
	ELuna::registerMethod<hpagent>(L, "GetState",						&hpsocket::GetState);

	ELuna::registerMethod<hpagent>(L, "GetConnectionCount",				&hpsocket::GetConnectionCount);
	ELuna::registerMethod<hpagent>(L, "GetConnectPeriod",				&hpsocket::GetConnectPeriod);
	ELuna::registerMethod<hpagent>(L, "GetSilencePeriod",				&hpsocket::GetSilencePeriod);
	ELuna::registerMethod<hpagent>(L, "GetLocalAddress",				&hpsocket::GetLocalAddress,2);
	ELuna::registerMethod<hpagent>(L, "GetRemoteAddress",				&hpsocket::GetRemoteAddress,2);
	ELuna::registerMethod<hpagent>(L, "GetLastError",					&hpsocket::GetLastError);
	ELuna::registerMethod<hpagent>(L, "GetLastErrorDesc",				&hpsocket::GetLastErrorDesc);
	ELuna::registerMethod<hpagent>(L, "GetPendingDataLength",			&hpsocket::GetPendingDataLength);

	ELuna::registerMethod<hpagent>(L, "SetSendPolicy",					&hpsocket::SetSendPolicy);
	ELuna::registerMethod<hpagent>(L, "SetFreeSocketObjLockTime",		&hpsocket::SetFreeSocketObjLockTime);
	ELuna::registerMethod<hpagent>(L, "SetFreeSocketObjPool",			&hpsocket::SetFreeSocketObjPool);
	ELuna::registerMethod<hpagent>(L, "SetFreeBufferObjPool",			&hpsocket::SetFreeBufferObjPool);
	ELuna::registerMethod<hpagent>(L, "SetFreeSocketObjHold",			&hpsocket::SetFreeSocketObjHold);
	ELuna::registerMethod<hpagent>(L, "SetFreeBufferObjHold",			&hpsocket::SetFreeBufferObjHold);
	ELuna::registerMethod<hpagent>(L, "SetWorkerThreadCount",			&hpsocket::SetWorkerThreadCount);
	ELuna::registerMethod<hpagent>(L, "SetMarkSilence",					&hpsocket::SetMarkSilence);

	ELuna::registerMethod<hpagent>(L, "GetSendPolicy",					&hpsocket::GetSendPolicy);
	ELuna::registerMethod<hpagent>(L, "GetFreeSocketObjLockTime",		&hpsocket::GetFreeSocketObjLockTime);
	ELuna::registerMethod<hpagent>(L, "GetFreeSocketObjPool",			&hpsocket::GetFreeSocketObjPool);
	ELuna::registerMethod<hpagent>(L, "GetFreeBufferObjPool",			&hpsocket::GetFreeBufferObjPool);
	ELuna::registerMethod<hpagent>(L, "GetFreeSocketObjHold",			&hpsocket::GetFreeSocketObjHold);
	ELuna::registerMethod<hpagent>(L, "GetFreeBufferObjHold",			&hpsocket::GetFreeBufferObjHold);
	ELuna::registerMethod<hpagent>(L, "GetWorkerThreadCount",			&hpsocket::GetWorkerThreadCount);

	return 1;
}