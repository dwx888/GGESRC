#pragma once
#include "LuaSocket.h"
#include "ELuna.h"
//#include <iostream>
//using namespace ELuna;

class hpserver :
	public hpsocket,//IComplexSocket
	public CTcpServerListener
{
public:
	hpserver(void* cs,lua_State* L);
	~hpserver(void);
public:
	void		Create_TcpServer		();
	//TcpPullServer
	void		Create_TcpPullServer	();
	int			Fetch					(DWORD dwConnID, DWORD pBuffer, int iLength);
	int			Peek					(DWORD dwConnID, DWORD pBuffer, int iLength);
	//TcpPackServer
	BOOL		SendPack				(CONNID dwConnID,ELuna::LuaString Data);
	void		Create_TcpPackServer	();
	void		SetMaxPackSize			(DWORD dwMaxPackSize);
	void		SetPackHeaderFlag		(USHORT usPackHeaderFlag);
	DWORD		GetMaxPackSize			();
	USHORT		GetPackHeaderFlag		();
	//ITcpServer
	//BOOL		SendSmallFile	(CONNID dwConnID, LPCTSTR lpszFileName, const LPWSABUF pHead = nullptr, const LPWSABUF pTail = nullptr)
	void		SetAcceptSocketCount	(DWORD dwAcceptSocketCount);
	void		SetSocketBufferSize		(DWORD dwSocketBufferSize);
	void		SetSocketListenQueue	(DWORD dwSocketListenQueue);
	void		SetKeepAliveTime		(DWORD dwKeepAliveTime);
	void		SetKeepAliveInterval	(DWORD dwKeepAliveInterval);
	DWORD		GetAcceptSocketCount	();
	DWORD		GetSocketBufferSize		();
	DWORD		GetSocketListenQueue	();
	DWORD		GetKeepAliveTime		();
	DWORD		GetKeepAliveInterval	();
	//IServer
	BOOL		Start					(const char* pszBindAddress, USHORT usPort);
	USHORT	GetListenAddress			(lua_State* L);

private:
	virtual EnHandleResult OnPrepareListen	(ITcpServer* pSender, SOCKET soListen);
	virtual EnHandleResult OnAccept			(ITcpServer* pSender, CONNID dwConnID, SOCKET soClient);
	//virtual EnHandleResult OnHandShake(ITcpServer* pSender, CONNID dwConnID)
	virtual EnHandleResult OnSend			(ITcpServer* pSender, CONNID dwConnID, const BYTE* pData, int iLength);
	virtual EnHandleResult OnReceive		(ITcpServer* pSender, CONNID dwConnID, int iLength);//PULL
	virtual EnHandleResult OnReceive		(ITcpServer* pSender, CONNID dwConnID, const BYTE* pData, int iLength);//PUSH,PACK
	virtual EnHandleResult OnClose			(ITcpServer* pSender, CONNID dwConnID, EnSocketOperation enOperation, int iErrorCode);
	virtual EnHandleResult OnShutdown		(ITcpServer* pSender);
private:
	ITcpServer*		m_isvr;
	ITcpPullServer* m_pull;
	ITcpPackServer* m_pack;
	
	//int				m_ref;
	//lua_State *		m_L;
	//LPCRITICAL_SECTION m_cs;
	//const char *	m_rname;//OnReceive
};

int REG_HPServer(lua_State * L);