
#include "LuaSocket.h"



BOOL hpsocket::Stop()
{
	return m_ics->Stop();
}
BOOL hpsocket::Send(CONNID dwConnID, DWORD pBuffer, int iLength, int iOffset)
{
	return m_ics->Send(dwConnID,(BYTE *)pBuffer,iLength,iOffset);
}
BOOL hpsocket::PauseReceive(CONNID dwConnID, BOOL bPause)
{
	return m_ics->PauseReceive(dwConnID,bPause);
}
BOOL hpsocket::Disconnect(CONNID dwConnID, BOOL bForce){
	return m_ics->Disconnect(dwConnID,bForce);	
}
BOOL hpsocket::DisconnectLongConnections(DWORD dwPeriod, BOOL bForce){
	return m_ics->DisconnectLongConnections(dwPeriod,bForce);	
}
BOOL hpsocket::DisconnectSilenceConnections(DWORD dwPeriod, BOOL bForce ){
	return m_ics->DisconnectSilenceConnections(dwPeriod,bForce);	
}



BOOL hpsocket::HasStarted(){
	return m_ics->HasStarted();	
}
int hpsocket::GetState(){
	return m_ics->GetState();
}
DWORD hpsocket::GetConnectionCount(){
	return m_ics->GetConnectionCount();
}
ELuna::LuaTable hpsocket::GetAllConnectionIDs(lua_State* L){
	ELuna::LuaTable r(L);
	CONNID* dwConnID = new CONNID[GetConnectionCount()];
	DWORD dwCount;
	m_ics->GetAllConnectionIDs(dwConnID,dwCount);
	for (DWORD i=0;i<dwCount;i++)
	{
		r.set(i,dwConnID[i]);
	}
	delete []dwConnID;
	return r;
}
DWORD hpsocket::GetConnectPeriod(CONNID dwConnID){
	CONNID dwPeriod;
	m_ics->GetConnectPeriod(dwConnID,dwPeriod);
	return dwPeriod;
}
DWORD hpsocket::GetSilencePeriod(CONNID dwConnID){
	CONNID dwPeriod;
	m_ics->GetSilencePeriod(dwConnID,dwPeriod);
	return dwPeriod;
}
USHORT hpsocket::GetLocalAddress(DWORD dwConnID,lua_State* L){
	TCHAR szAddress[40];
	memset(szAddress,0,40);
	int iAddressLen = sizeof(szAddress) / sizeof(TCHAR);
	USHORT usPort;
	m_ics->GetLocalAddress(dwConnID, szAddress, iAddressLen, usPort);
	lua_pushstring(L,szAddress);
	return usPort;
}
USHORT hpsocket::GetRemoteAddress(DWORD dwConnID,lua_State* L){
	TCHAR szAddress[40];
	memset(szAddress,0,40);
	int iAddressLen = sizeof(szAddress) / sizeof(TCHAR);
	USHORT usPort;
	m_ics->GetRemoteAddress(dwConnID, szAddress, iAddressLen, usPort);
	lua_pushstring(L,szAddress);
	return usPort;
}
int hpsocket::GetLastError	(){
	return m_ics->GetLastError();	
}
const char* hpsocket::GetLastErrorDesc()
{
	return m_ics->GetLastErrorDesc();	
}
BOOL hpsocket::GetPendingDataLength(CONNID dwConnID)
{
	int iPending;
	m_ics->GetPendingDataLength(dwConnID,iPending);	
	return iPending;
}
BOOL hpsocket::IsPauseReceive(CONNID dwConnID)
{
	BOOL bPaused;
	m_ics->IsPauseReceive(dwConnID,bPaused);	
	return bPaused;
}



void hpsocket::SetSendPolicy(int enSendPolicy){
	m_ics->SetSendPolicy(EnSendPolicy(enSendPolicy));
}
void hpsocket::SetMaxConnectionCount(DWORD dwMaxConnectionCount){
	m_ics->SetMaxConnectionCount(dwMaxConnectionCount);
}
void hpsocket::SetFreeSocketObjLockTime	(DWORD dwFreeSocketObjLockTime){
	m_ics->SetFreeSocketObjLockTime(dwFreeSocketObjLockTime);
}
void hpsocket::SetFreeSocketObjPool		(DWORD z){
	m_ics->SetFreeSocketObjPool(z);
}
void hpsocket::SetFreeBufferObjPool		(DWORD z){
	m_ics->SetFreeBufferObjPool(z);
}
void hpsocket::SetFreeSocketObjHold		(DWORD z){
	m_ics->SetFreeSocketObjHold(z);
}
void hpsocket::SetFreeBufferObjHold		(DWORD z){
	m_ics->SetFreeBufferObjHold(z);
}
void hpsocket::SetWorkerThreadCount		(DWORD z){
	m_ics->SetWorkerThreadCount(z);
}
void hpsocket::SetMarkSilence		(BOOL z){
	m_ics->SetMarkSilence(z);
}


int hpsocket::GetSendPolicy		(){
	return m_ics->GetSendPolicy();
}
DWORD hpsocket::GetMaxConnectionCount		(){
	return m_ics->GetMaxConnectionCount();
}
DWORD hpsocket::GetFreeSocketObjLockTime		(){
	return m_ics->GetFreeSocketObjLockTime();
}
DWORD hpsocket::GetFreeSocketObjPool		(){
	return m_ics->GetFreeSocketObjPool();
}
DWORD hpsocket::GetFreeBufferObjPool		(){
	return m_ics->GetFreeBufferObjPool();
}
DWORD hpsocket::GetFreeSocketObjHold		(){
	return m_ics->GetFreeSocketObjHold();
}
DWORD hpsocket::GetFreeBufferObjHold		(){
	return m_ics->GetFreeBufferObjHold();
}
DWORD hpsocket::GetWorkerThreadCount		(){
	return m_ics->GetWorkerThreadCount();
}