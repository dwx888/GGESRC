#include "LuaServer.h"

hpserver::hpserver(void* cs,lua_State* L){
	m_ref	= 0;
	m_pull	= NULL;
	m_pack	= NULL;
	m_cs	= (LPCRITICAL_SECTION)cs;
	m_L	= L;
}
hpserver::~hpserver(void){
	if (HasStarted())
		Stop();
	if(m_ref)
		luaL_unref(m_L, LUA_REGISTRYINDEX, m_ref);

	if(m_pull)
		HP_Destroy_TcpPullServer(m_pull);
	else if(m_pack)
		HP_Destroy_TcpPackServer(m_pack);
	else if(m_isvr)
		HP_Destroy_TcpServer(m_isvr);
}

//====================================================================

void hpserver::Create_TcpPullServer()
{
	m_ref	= luaL_ref(m_L, LUA_REGISTRYINDEX);//lua回调地址
	m_pull	= HP_Create_TcpPullServer(this);
	m_isvr	= (ITcpServer*)m_pull;
	m_ics	= (IComplexSocket*)m_pull;
}
void hpserver::Create_TcpPackServer()
{
	m_ref = luaL_ref(m_L, LUA_REGISTRYINDEX);//lua回调地址
	m_pack = HP_Create_TcpPackServer((ITcpServerListener *)this);
	m_isvr = (ITcpServer*)m_pack;
	m_ics	= (IComplexSocket*)m_pack;
}
void hpserver::Create_TcpServer()
{
	m_ref = luaL_ref(m_L, LUA_REGISTRYINDEX);//lua回调地址
	m_isvr = HP_Create_TcpServer((ITcpServerListener *)this);
}
//====================================================================
//PULL
int hpserver::Fetch(CONNID dwConnID, DWORD pBuffer, int iLength)
{
	return (int)m_pull->Fetch(dwConnID,(BYTE *)pBuffer,iLength);
}
//PACK
BOOL hpserver::SendPack(CONNID dwConnID,ELuna::LuaString Data)
{
	return m_pack->Send(dwConnID,(BYTE *)Data.str,Data.len);
}
void hpserver::SetMaxPackSize(DWORD dwMaxPackSize)
{
	m_pack->SetMaxPackSize(dwMaxPackSize);
}
void hpserver::SetPackHeaderFlag(USHORT usPackHeaderFlag)
{
	m_pack->SetPackHeaderFlag(usPackHeaderFlag);
}
DWORD hpserver::GetMaxPackSize()
{
	return m_pack->GetMaxPackSize();
}
USHORT hpserver::GetPackHeaderFlag()
{
	return m_pack->GetPackHeaderFlag();
}
//ITcpServer====================================================================
void hpserver::SetAcceptSocketCount(DWORD z)
{
	m_isvr->SetAcceptSocketCount(z);
}
void hpserver::SetSocketBufferSize(DWORD z)
{
	m_isvr->SetSocketBufferSize(z);
}
void hpserver::SetSocketListenQueue(DWORD z)
{
	m_isvr->SetSocketListenQueue(z);
}
void hpserver::SetKeepAliveTime(DWORD z)
{
	m_isvr->SetKeepAliveTime(z);
}
void hpserver::SetKeepAliveInterval(DWORD z)
{
	m_isvr->SetKeepAliveInterval(z);
}
//GET--------------------------------------------------
DWORD hpserver::GetAcceptSocketCount()
{
	return m_isvr->GetAcceptSocketCount();
}
DWORD hpserver::GetSocketBufferSize()
{
	return m_isvr->GetSocketBufferSize();
}
DWORD hpserver::GetSocketListenQueue()
{
	return m_isvr->GetSocketListenQueue();
}
DWORD hpserver::GetKeepAliveTime()
{
	return m_isvr->GetKeepAliveTime();
}
DWORD hpserver::GetKeepAliveInterval()
{
	return m_isvr->GetKeepAliveInterval();
}
//====================================================================
BOOL hpserver::Start(const char * pszBindAddress, USHORT usPort)
{	
	return m_isvr->Start(pszBindAddress,usPort);
}
USHORT hpserver::GetListenAddress(lua_State* L)
{
	TCHAR szAddress[40];
	memset(szAddress,0,40);
	int iAddressLen = sizeof(szAddress) / sizeof(TCHAR);
	USHORT usPort;
	m_isvr->GetListenAddress(szAddress, iAddressLen, usPort);
	lua_pushstring(L,szAddress);
	return usPort;
}
//====================================================================
EnHandleResult hpserver::OnPrepareListen(ITcpServer* pSender,SOCKET soListen)
{
	EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnPrepareListen");
	lua_pushvalue(m_L,-2);
	lua_pushinteger(m_L,soListen);
	lua_call(m_L,2,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	LeaveCriticalSection(m_cs);
	return EnHandleResult(r);
}


EnHandleResult hpserver::OnSend(ITcpServer* pSender,CONNID dwConnID, const BYTE* pData, int iLength)
{
	if (HasStarted()){
		EnterCriticalSection(m_cs);
		lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
		lua_getfield(m_L,-1,"OnSend");
		lua_pushvalue(m_L,-2);
		lua_pushinteger(m_L,dwConnID);
		lua_pushlightuserdata(m_L,(void*)pData);
		lua_pushinteger(m_L,iLength);
		lua_call(m_L,4,1);
		int r = lua_tointeger(m_L,-1);
		lua_pop(m_L,2);
		LeaveCriticalSection(m_cs);
		return EnHandleResult(r);
	}
	return HR_OK;
}

EnHandleResult hpserver::OnAccept(ITcpServer* pSender,CONNID dwConnID, SOCKET soClient)
{
	EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnAccept");
	lua_pushvalue(m_L,-2);
	lua_pushinteger(m_L,dwConnID);
	lua_pushinteger(m_L,soClient);
	lua_call(m_L,3,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	LeaveCriticalSection(m_cs);
	return EnHandleResult(r);
}
//PULL
EnHandleResult hpserver::OnReceive(ITcpServer* pSender,CONNID dwConnID, int iLength)
{
	if (m_pull)
	{
		EnterCriticalSection(m_cs);
		lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
		lua_getfield(m_L,-1,"OnReceive");
		lua_pushvalue(m_L,-2);
		lua_pushinteger(m_L,dwConnID);
		lua_pushinteger(m_L,iLength);
		lua_call(m_L,3,1);
		int r = lua_tointeger(m_L,-1);
		lua_pop(m_L,2);
		LeaveCriticalSection(m_cs);
		return EnHandleResult(r);
	}
	return HR_IGNORE;
}
//PACK
EnHandleResult hpserver::OnReceive(ITcpServer* pSender,CONNID dwConnID, const BYTE* pData, int iLength)
{
	if (!m_pull)
	{
		EnterCriticalSection(m_cs);
		lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
		lua_getfield(m_L,-1,"OnReceive");
		lua_pushvalue(m_L,-2);
		lua_pushinteger(m_L,dwConnID);
		if (m_pack){
			lua_pushlstring(m_L,(const char*)pData,iLength);
			lua_call(m_L,3,1);
		} else{//Tcp
			lua_pushlightuserdata(m_L,(void*)pData);
			lua_pushinteger(m_L,iLength);
			lua_call(m_L,4,1);
		}
		int r = lua_tointeger(m_L,-1);
		lua_pop(m_L,2);
		LeaveCriticalSection(m_cs);
		return EnHandleResult(r);
	}
	return HR_IGNORE;
}
EnHandleResult hpserver::OnClose(ITcpServer* pSender,CONNID dwConnID, EnSocketOperation enOperation, int iErrorCode)
{
	if (HasStarted())//Stop阻塞
		EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnClose");
	lua_pushvalue(m_L,-2);
	lua_pushinteger(m_L,dwConnID);
	lua_pushinteger(m_L,enOperation);
	lua_pushinteger(m_L,iErrorCode);
	lua_call(m_L,4,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	if (HasStarted())//Stop阻塞
		LeaveCriticalSection(m_cs);
	return EnHandleResult(r);
}
EnHandleResult hpserver::OnShutdown(ITcpServer* pSender)
{
	EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnShutdown");
	lua_pushvalue(m_L,-2);
	lua_call(m_L,1,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	LeaveCriticalSection(m_cs);
	return EnHandleResult(r);
}
int REG_HPServer(lua_State * L){
	const char* gname = "__HPServer";
	ELuna::registerClass<hpserver>(L, gname, ELuna::constructor<hpserver,void*,lua_State*>);
	//TcpPullServer------------------------------------------------------------------------------------------------------------
	ELuna::registerMethod<hpserver>(L, "Create_TcpPullServer",	&hpserver::Create_TcpPullServer);
	ELuna::registerMethod<hpserver>(L, "Fetch",					&hpserver::Fetch);
	//TcpPackServer------------------------------------------------------------------------------------------------------------
	ELuna::registerMethod<hpserver>(L, "Create_TcpPackServer",	&hpserver::Create_TcpPackServer);
	ELuna::registerMethod<hpserver>(L, "SendPack",				&hpserver::SendPack);
	ELuna::registerMethod<hpserver>(L, "SetMaxPackSize",		&hpserver::SetMaxPackSize);
	ELuna::registerMethod<hpserver>(L, "SetPackHeaderFlag",		&hpserver::SetPackHeaderFlag);
	ELuna::registerMethod<hpserver>(L, "GetMaxPackSize",		&hpserver::GetMaxPackSize);
	ELuna::registerMethod<hpserver>(L, "GetPackHeaderFlag",		&hpserver::GetPackHeaderFlag);

	//ITcpServer------------------------------------------------------------------------------------------------------------
	ELuna::registerMethod<hpserver>(L, "SetAcceptSocketCount",	&hpserver::SetAcceptSocketCount);
	ELuna::registerMethod<hpserver>(L, "SetSocketBufferSize",	&hpserver::SetSocketBufferSize);
	ELuna::registerMethod<hpserver>(L, "SetSocketListenQueue",	&hpserver::SetSocketListenQueue);
	ELuna::registerMethod<hpserver>(L, "SetKeepAliveTime",		&hpserver::SetKeepAliveTime);
	ELuna::registerMethod<hpserver>(L, "SetKeepAliveInterval",	&hpserver::SetKeepAliveInterval);

	ELuna::registerMethod<hpserver>(L, "GetAcceptSocketCount",	&hpserver::GetAcceptSocketCount);
	ELuna::registerMethod<hpserver>(L, "GetSocketBufferSize",	&hpserver::GetSocketBufferSize);
	ELuna::registerMethod<hpserver>(L, "GetSocketListenQueue",	&hpserver::GetSocketListenQueue);
	ELuna::registerMethod<hpserver>(L, "GetKeepAliveTime",		&hpserver::GetKeepAliveTime);
	ELuna::registerMethod<hpserver>(L, "GetKeepAliveInterval",	&hpserver::GetKeepAliveInterval);
	//IServer------------------------------------------------------------------------------------------------------------
	ELuna::registerMethod<hpserver>(L, "Start",					&hpserver::Start);
	ELuna::registerMethod<hpserver>(L, "GetListenAddress",		&hpserver::GetListenAddress,2);
	//IComplexSocket------------------------------------------------------------------------------------------------------------
	ELuna::registerMethod<hpserver>(L, "Stop",							&hpsocket::Stop);
	ELuna::registerMethod<hpserver>(L, "Send",							&hpsocket::Send);
	ELuna::registerMethod<hpserver>(L, "PauseReceive",					&hpsocket::PauseReceive);
	ELuna::registerMethod<hpserver>(L, "Disconnect",					&hpsocket::Disconnect);
	ELuna::registerMethod<hpserver>(L, "DisconnectLongConnections",		&hpsocket::DisconnectLongConnections);
	ELuna::registerMethod<hpserver>(L, "DisconnectSilenceConnections",	&hpsocket::DisconnectSilenceConnections);
	ELuna::registerMethod<hpserver>(L, "HasStarted",					&hpsocket::HasStarted);
	ELuna::registerMethod<hpserver>(L, "GetState",						&hpsocket::GetState);

	ELuna::registerMethod<hpserver>(L, "GetConnectionCount",			&hpsocket::GetConnectionCount);
	ELuna::registerMethod<hpserver>(L, "GetAllConnectionIDs",			&hpsocket::GetAllConnectionIDs);
	ELuna::registerMethod<hpserver>(L, "GetConnectPeriod",				&hpsocket::GetConnectPeriod);
	ELuna::registerMethod<hpserver>(L, "GetSilencePeriod",				&hpsocket::GetSilencePeriod);
	ELuna::registerMethod<hpserver>(L, "GetLocalAddress",				&hpsocket::GetLocalAddress,2);
	ELuna::registerMethod<hpserver>(L, "GetRemoteAddress",				&hpsocket::GetRemoteAddress,2);
	ELuna::registerMethod<hpserver>(L, "GetLastError",					&hpsocket::GetLastError);
	ELuna::registerMethod<hpserver>(L, "GetLastErrorDesc",				&hpsocket::GetLastErrorDesc);
	ELuna::registerMethod<hpserver>(L, "GetPendingDataLength",			&hpsocket::GetPendingDataLength);
	ELuna::registerMethod<hpserver>(L, "IsPauseReceive",				&hpsocket::IsPauseReceive);

	ELuna::registerMethod<hpserver>(L, "SetSendPolicy",					&hpsocket::SetSendPolicy);
	ELuna::registerMethod<hpserver>(L, "SetMaxConnectionCount",			&hpsocket::SetMaxConnectionCount);
	ELuna::registerMethod<hpserver>(L, "SetFreeSocketObjLockTime",		&hpsocket::SetFreeSocketObjLockTime);
	ELuna::registerMethod<hpserver>(L, "SetFreeSocketObjPool",			&hpsocket::SetFreeSocketObjPool);
	ELuna::registerMethod<hpserver>(L, "SetFreeBufferObjPool",			&hpsocket::SetFreeBufferObjPool);
	ELuna::registerMethod<hpserver>(L, "SetFreeSocketObjHold",			&hpsocket::SetFreeSocketObjHold);
	ELuna::registerMethod<hpserver>(L, "SetFreeBufferObjHold",			&hpsocket::SetFreeBufferObjHold);
	ELuna::registerMethod<hpserver>(L, "SetWorkerThreadCount",			&hpsocket::SetWorkerThreadCount);
	ELuna::registerMethod<hpserver>(L, "SetMarkSilence",				&hpsocket::SetMarkSilence);

	ELuna::registerMethod<hpserver>(L, "GetSendPolicy",					&hpsocket::GetSendPolicy);
	ELuna::registerMethod<hpserver>(L, "GetMaxConnectionCount",			&hpsocket::GetMaxConnectionCount);
	ELuna::registerMethod<hpserver>(L, "GetFreeSocketObjLockTime",		&hpsocket::GetFreeSocketObjLockTime);
	ELuna::registerMethod<hpserver>(L, "GetFreeSocketObjPool",			&hpsocket::GetFreeSocketObjPool);
	ELuna::registerMethod<hpserver>(L, "GetFreeBufferObjPool",			&hpsocket::GetFreeBufferObjPool);
	ELuna::registerMethod<hpserver>(L, "GetFreeSocketObjHold",			&hpsocket::GetFreeSocketObjHold);
	ELuna::registerMethod<hpserver>(L, "GetFreeBufferObjHold",			&hpsocket::GetFreeBufferObjHold);
	ELuna::registerMethod<hpserver>(L, "GetWorkerThreadCount",			&hpsocket::GetWorkerThreadCount);

	return 1;
}