#pragma once
#include "HPSocket.h"
#include "ELuna.h"
//using namespace ELuna;


class hpsocket//IComplexSocket
{
public:

	BOOL			Stop						();
	BOOL			Send						(CONNID dwConnID,DWORD pBuffer, int iLength, int iOffset);
	//BOOL SendPackets							(CONNID dwConnID, const WSABUF pBuffers[], int iCount);
	BOOL			PauseReceive				(CONNID dwConnID, BOOL bPause);
	BOOL			Disconnect					(CONNID dwConnID, BOOL bForce);
	BOOL			DisconnectLongConnections	(DWORD dwPeriod, BOOL bForce);
	BOOL			DisconnectSilenceConnections(DWORD dwPeriod, BOOL bForce );
	//BOOL			SetConnectionExtra			(CONNID dwConnID, PVOID pExtra);
	//BOOL			GetConnectionExtra			(CONNID dwConnID, PVOID* ppExtra);
	BOOL			HasStarted					();
	int				GetState					();
	DWORD			GetConnectionCount			();
	ELuna::LuaTable	GetAllConnectionIDs			(lua_State* L);
	DWORD			GetConnectPeriod			(CONNID dwConnID);
	DWORD			GetSilencePeriod			(CONNID dwConnID);
	USHORT			GetLocalAddress				(CONNID dwConnID,lua_State* L);
	USHORT			GetRemoteAddress			(CONNID dwConnID,lua_State* L);
	int				GetLastError				();
	const char*		GetLastErrorDesc			();
	BOOL			GetPendingDataLength		(CONNID dwConnID);
	BOOL			IsPauseReceive				(CONNID dwConnID);
	//BOOL IsConnected			(CONNID dwConnID)

	void			SetSendPolicy				(int enSendPolicy);
	void			SetMaxConnectionCount		(DWORD dwMaxConnectionCount);
	void			SetFreeSocketObjLockTime	(DWORD dwFreeSocketObjLockTime)	;
	void			SetFreeSocketObjPool		(DWORD dwFreeSocketObjPool);
	void			SetFreeBufferObjPool		(DWORD dwFreeBufferObjPool);
	void			SetFreeSocketObjHold		(DWORD dwFreeSocketObjHold);
	void			SetFreeBufferObjHold		(DWORD dwFreeBufferObjHold);
	void			SetWorkerThreadCount		(DWORD dwWorkerThreadCount);
	void			SetMarkSilence				(BOOL bMarkSilence)	;

	int				GetSendPolicy				();
	DWORD			GetMaxConnectionCount		();
	DWORD			GetFreeSocketObjLockTime	();
	DWORD			GetFreeSocketObjPool		();
	DWORD			GetFreeBufferObjPool		();
	DWORD			GetFreeSocketObjHold		();
	DWORD			GetFreeBufferObjHold		();
	DWORD			GetWorkerThreadCount		();
	BOOL			IsMarkSilence				();												;
public:
	IComplexSocket		*m_ics;
	lua_State			*m_L;
	int					m_ref;
	LPCRITICAL_SECTION	m_cs;
};
