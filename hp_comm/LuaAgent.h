#pragma once
#include "LuaSocket.h"
#include "ELuna.h"
//using namespace ELuna;

class hpagent:
	public hpsocket,//IComplexSocket
	public CTcpAgentListener
{
public:
	hpagent(void* cs,lua_State* L);
	~hpagent(void);
	//TcpPackAgent
	BOOL			SendPack					(CONNID dwConnID,ELuna::LuaString Data);
	void			Create_TcpPackAgent			();
	void			SetMaxPackSize				(DWORD dwMaxPackSize);
	void			SetPackHeaderFlag			(USHORT usPackHeaderFlag);
	DWORD			GetMaxPackSize				();
	USHORT			GetPackHeaderFlag			();
	//TcpPullAgent
	void			Create_TcpPullAgent			();
	int				Fetch						(CONNID dwConnID,DWORD pData, int iLength);
	//ITcpAgent
	void			Create_TcpAgent				();
	//BOOL SendSmallFile	(CONNID dwConnID, LPCTSTR lpszFileName, const LPWSABUF pHead = nullptr, const LPWSABUF pTail = nullptr);
	void			SetReuseAddress				(BOOL bReuseAddress);
	BOOL			IsReuseAddress				()	;
	void			SetSocketBufferSize			(DWORD dwSocketBufferSize);
	void			SetKeepAliveTime			(DWORD dwKeepAliveTime);
	void			SetKeepAliveInterval		(DWORD dwKeepAliveInterval);
	DWORD			GetSocketBufferSize			();
	DWORD			GetKeepAliveTime			();
	DWORD			GetKeepAliveInterval		();
	//IAgent
	BOOL			Start						(LPCTSTR lpszBindAddress, BOOL bAsyncConnect);
	BOOL			Connect						(LPCTSTR lpszRemoteAddress, USHORT usPort);

private:
	virtual EnHandleResult OnPrepareConnect	(ITcpAgent* pSender, CONNID dwConnID, SOCKET socket);
	virtual EnHandleResult OnConnect		(ITcpAgent* pSender, CONNID dwConnID);

	virtual EnHandleResult OnReceive		(ITcpAgent* pSender, CONNID dwConnID, int iLength);//PULL
	virtual EnHandleResult OnSend			(ITcpAgent* pSender, CONNID dwConnID, const BYTE* pData, int iLength);
	virtual EnHandleResult OnShutdown		(ITcpAgent* pSender);
	virtual EnHandleResult OnClose			(ITcpAgent* pSender,CONNID dwConnID, EnSocketOperation enOperation, int iErrorCode);
	virtual EnHandleResult OnReceive		(ITcpAgent* pSender, CONNID dwConnID, const BYTE* pData, int iLength);
	ITcpPackAgent	*m_pack;
	ITcpPullAgent	*m_pull;
	ITcpAgent		*m_ita;

};

int REG_HPAgent(lua_State * L);