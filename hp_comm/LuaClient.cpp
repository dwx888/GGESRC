#include "LuaClient.h"


hpclient::hpclient(void* cs,lua_State* L){
	m_pull	= NULL;
	m_pack	= NULL;
	m_udp	= NULL;
	m_ref	= 0;
	m_cs	= (LPCRITICAL_SECTION)cs;
	m_L		= L;
}

hpclient::~hpclient(void){
	if (HasStarted())
		Stop();
	if (m_ref)
		luaL_unref(m_L, LUA_REGISTRYINDEX, m_ref);
	if (m_pull)
		HP_Destroy_TcpPullClient(m_pull);
	else if(m_pack)
		HP_Destroy_TcpPackClient(m_pack);
	else if(m_udp)
		HP_Destroy_UdpCast(m_udp);
	else if (m_itc)
		HP_Destroy_TcpClient(m_itc);
}

void hpclient::Create_TcpPullClient()//pull
{
	m_ref	= luaL_ref(m_L, LUA_REGISTRYINDEX);
	m_pull	= HP_Create_TcpPullClient(this);
	m_itc	= (ITcpClient*)m_pull;
	m_ic	= (IClient*)m_pull; 
}
void hpclient::Create_TcpPackClient()//pack
{
	m_ref	= luaL_ref(m_L, LUA_REGISTRYINDEX);
	m_pack	= HP_Create_TcpPackClient(this);
	m_itc	= (ITcpClient*)m_pack; 
	m_ic	= (IClient*)m_pack; 
}
void hpclient::Create_UdpCast()//udp
{
	m_ref = luaL_ref(m_L, LUA_REGISTRYINDEX);
	m_udp = HP_Create_UdpCast(this);
	m_ic  = (IClient*)m_udp; 
}
void hpclient::Create_TcpClient()
{
	m_ref	= luaL_ref(m_L, LUA_REGISTRYINDEX);
	m_itc	=  HP_Create_TcpClient(this);
	m_ic	= (ITcpClient*)m_itc; 
}
////////////////////////////////////////////////////////////////UDP

void hpclient::SetMaxDatagramSize	(DWORD dwMaxDatagramSize){
	m_udp->SetMaxDatagramSize(dwMaxDatagramSize);
}
DWORD hpclient::GetMaxDatagramSize(){
	return m_udp->GetMaxDatagramSize();
}
void hpclient::SetReuseAddress	(BOOL bReuseAddress){
	m_udp->SetReuseAddress(bReuseAddress);
}
BOOL hpclient::IsReuseAddress		()	{
	return m_udp->IsReuseAddress();
}
void hpclient::SetCastMode		(int enCastMode){
	m_udp->SetCastMode(EnCastMode(enCastMode));
}
int	 hpclient::GetCastMode	()	{
	return m_udp->GetCastMode();
}
void hpclient::SetMultiCastTtl	(int iMCTtl){
	m_udp->SetMultiCastTtl(iMCTtl);
}
int	 hpclient::GetMultiCastTtl		(){
	return m_udp->GetMultiCastTtl();
}
void hpclient::SetMultiCastLoop	(BOOL bMCLoop){
	m_udp->SetMultiCastLoop(bMCLoop);
}
BOOL hpclient::IsMultiCastLoop	(){
	return m_udp->IsMultiCastLoop();
}
USHORT hpclient::GetRemoteAddress	(lua_State* L){
	TCHAR lpszAddress[40];
	memset(lpszAddress,0,40);
	int iAddressLen = sizeof(lpszAddress) / sizeof(TCHAR);
	USHORT usPort;
	m_udp->GetRemoteAddress(lpszAddress,iAddressLen,usPort);
	lua_pushstring(L,lpszAddress);
	return usPort;	
}
////////////////////////////////////////////////////////////////PULL
BOOL hpclient::Fetch(DWORD pData, int iLength)
{
	return m_pull->Fetch((BYTE*)pData,iLength);	
}
////////////////////////////////////////////////////////////////PACK
BOOL hpclient::SendPack(LuaString Data)
{
	return m_pack->Send((BYTE *)Data.str,Data.len);
}
void hpclient::SetMaxPackSize(DWORD dwMaxPackSize)
{
	m_pack->SetMaxPackSize(dwMaxPackSize);
}
void hpclient::SetPackHeaderFlag(USHORT usPackHeaderFlag)
{
	m_pack->SetPackHeaderFlag(usPackHeaderFlag);
}
DWORD hpclient::GetMaxPackSize()
{
	return m_pack->GetMaxPackSize();
}
USHORT hpclient::GetPackHeaderFlag()
{
	return m_pack->GetPackHeaderFlag();
}
//////////////////////////////////////////////////////////////TCP
BOOL hpclient::Start(const char * pszBindAddress, USHORT usPort, BOOL bAsyncConnect,const char * lpszBindAddress)
{
	if (m_udp)
		return m_ic->Start(pszBindAddress,usPort,bAsyncConnect,lpszBindAddress);
	else
		return m_ic->Start(pszBindAddress,usPort,bAsyncConnect);	
}
BOOL hpclient::Stop()
{
	return m_ic->Stop();	
}
BOOL hpclient::HasStarted()
{
	return m_ic->HasStarted();	
}
int hpclient::GetState()
{
	return m_ic->GetState();	
}
int hpclient::GetLastError()
{
	return m_ic->GetLastError();	
}
const char* hpclient::GetLastErrorDesc()
{
	return m_ic->GetLastErrorDesc();	
}
DWORD hpclient::GetConnectionID()
{
	return m_ic->GetConnectionID();	
}
USHORT hpclient::GetLocalAddress(lua_State* L)
{
	TCHAR szAddress[40];
	memset(szAddress,0,40);
	int iAddressLen = sizeof(szAddress) / sizeof(TCHAR);
	USHORT usPort;
	m_ic->GetLocalAddress(szAddress, iAddressLen, usPort);
	lua_pushstring(L,szAddress);
	return usPort;	
}
int hpclient::GetPendingDataLength()
{
	int l =0;
	m_ic->GetPendingDataLength(l);
	return l;	
}
void hpclient::SetFreeBufferPoolSize(DWORD v)
{
	m_ic->SetFreeBufferPoolSize(v);
}
void hpclient::SetFreeBufferPoolHold(DWORD v)
{
	m_ic->SetFreeBufferPoolHold(v);
}
DWORD hpclient::GetFreeBufferPoolSize()
{
	return m_ic->GetFreeBufferPoolSize();	
}
DWORD hpclient::GetFreeBufferPoolHold()
{
	return m_ic->GetFreeBufferPoolHold();	
}
BOOL hpclient::Send(DWORD pBuffer, int iLength, int iOffset)
{
	return m_ic->Send((BYTE*)pBuffer,iLength,iOffset);	
}
/////////////////////////////////////////////////////////////////////////
void hpclient::SetSocketBufferSize(DWORD v)
{
	m_itc->SetSocketBufferSize(v);
}
void hpclient::SetKeepAliveTime(DWORD v)
{
	m_itc->SetKeepAliveTime(v);
}
void hpclient::SetKeepAliveInterval(DWORD v)
{
	m_itc->SetKeepAliveInterval(v);
}
DWORD hpclient::GetSocketBufferSize()
{
	return m_itc->GetSocketBufferSize();
}
DWORD hpclient::GetKeepAliveTime()
{
	return m_itc->GetKeepAliveTime();
}
DWORD hpclient::GetKeepAliveInterval()
{
	return m_itc->GetKeepAliveInterval();
}

EnHandleResult hpclient::OnPrepareConnect(ITcpClient* pSender,CONNID dwConnID, SOCKET socket)
{
	EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnPrepareConnect");
	lua_pushvalue(m_L,-2);
	//lua_pushinteger(m_L,dwConnID);
	lua_pushinteger(m_L,socket);
	lua_call(m_L,2,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	LeaveCriticalSection(m_cs);
	return EnHandleResult(r);
}
EnHandleResult hpclient::OnConnect(ITcpClient* pSender, CONNID dwConnID)
{
	EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnConnect");
	lua_pushvalue(m_L,-2);
	lua_call(m_L,1,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	LeaveCriticalSection(m_cs);
	
	return EnHandleResult(r);
}

EnHandleResult hpclient::OnSend(ITcpClient* pSender, CONNID dwConnID, const BYTE* pData, int iLength)
{
	EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnSend");
	lua_pushvalue(m_L,-2);
	lua_pushlightuserdata(m_L,(void*)pData);
	lua_pushinteger(m_L,iLength);
	lua_call(m_L,3,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	LeaveCriticalSection(m_cs);
	return EnHandleResult(r);
}
//PULL
EnHandleResult hpclient::OnReceive(ITcpClient* pSender, CONNID dwConnID, int iLength)
{
	if (m_pull)
	{
		EnterCriticalSection(m_cs);
		lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
		lua_getfield(m_L,-1,"OnReceive");
		lua_pushvalue(m_L,-2);
		lua_pushinteger(m_L,iLength);
		lua_call(m_L,2,1);
		int r = lua_tointeger(m_L,-1);
		lua_pop(m_L,2);
		LeaveCriticalSection(m_cs);
		return EnHandleResult(r);
	}
	return HR_IGNORE;
}

//Tcp&Pack
EnHandleResult hpclient::OnReceive(ITcpClient* pSender, CONNID dwConnID, const BYTE* pData, int iLength)
{
	if (!m_pull)
	{
		EnterCriticalSection(m_cs);
		lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
		lua_getfield(m_L,-1,"OnReceive");
		lua_pushvalue(m_L,-2);
		if (m_pack){
			lua_pushlstring(m_L,(char*)pData,iLength);
			lua_call(m_L,2,1);
		} else{
			lua_pushlightuserdata(m_L,(void*)pData);
			lua_pushinteger(m_L,iLength);
			lua_call(m_L,3,1);
		}
		int r = lua_tointeger(m_L,-1);
		lua_pop(m_L,2);
		LeaveCriticalSection(m_cs);
	}
	return HR_IGNORE;
}
EnHandleResult hpclient::OnClose(ITcpClient* pSender, CONNID dwConnID, EnSocketOperation enOperation, int iErrorCode)
{
	EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnClose");
	lua_pushvalue(m_L,-2);
	lua_pushinteger(m_L,enOperation);
	lua_pushinteger(m_L,iErrorCode);
	lua_call(m_L,3,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	LeaveCriticalSection(m_cs);

	return EnHandleResult(r);
}
//UDP
EnHandleResult hpclient::OnReceive(IUdpCast* pSender, CONNID dwConnID, const BYTE* pData, int iLength)
{
	if (m_udp)
	{
		EnterCriticalSection(m_cs);
		lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
		lua_getfield(m_L,-1,"OnReceive");
		lua_pushvalue(m_L,-2);
		lua_pushlstring(m_L,(const char*)pData,iLength);
		lua_call(m_L,2,1);
		int r = lua_tointeger(m_L,-1);
		lua_pop(m_L,2);
		LeaveCriticalSection(m_cs);
		return EnHandleResult(r);
	}
	return HR_IGNORE;
}
EnHandleResult hpclient::OnClose(IUdpCast* pSender, CONNID dwConnID, EnSocketOperation enOperation, int iErrorCode)
{
	if (enOperation = SO_CLOSE)
		return HR_OK;
	EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnClose");
	lua_pushvalue(m_L,-2);
	lua_pushinteger(m_L,enOperation);
	lua_pushinteger(m_L,iErrorCode);
	lua_call(m_L,3,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	LeaveCriticalSection(m_cs);

	return EnHandleResult(r);
}
EnHandleResult hpclient::OnConnect(IUdpCast* pSender, CONNID dwConnID)
{
	EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnConnect");
	lua_pushvalue(m_L,-2);
	lua_call(m_L,1,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	LeaveCriticalSection(m_cs);
	return EnHandleResult(r);
}
EnHandleResult hpclient::OnSend(IUdpCast* pSender, CONNID dwConnID, const BYTE* pData, int iLength)
{
	if (HasStarted()){
		EnterCriticalSection(m_cs);
		lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
		lua_getfield(m_L,-1,"OnSend");
		lua_pushvalue(m_L,-2);
		lua_pushlightuserdata(m_L,(void*)pData);
		lua_pushinteger(m_L,iLength);
		lua_call(m_L,3,1);
		int r = lua_tointeger(m_L,-1);
		lua_pop(m_L,2);
		LeaveCriticalSection(m_cs);
		return EnHandleResult(r);
	}
	return HR_OK;
}
EnHandleResult hpclient::OnPrepareConnect(IUdpCast* pSender,CONNID dwConnID, SOCKET socket)
{
	if (HasStarted())//Stop����
		EnterCriticalSection(m_cs);
	lua_rawgeti(m_L, LUA_REGISTRYINDEX, m_ref);
	lua_getfield(m_L,-1,"OnPrepareConnect");
	lua_pushvalue(m_L,-2);
	//lua_pushinteger(m_L,dwConnID);
	lua_pushinteger(m_L,socket);
	lua_call(m_L,2,1);
	int r = lua_tointeger(m_L,-1);
	lua_pop(m_L,2);
	if (HasStarted())
		LeaveCriticalSection(m_cs);
	return EnHandleResult(r);
}
int REG_HPClient(lua_State* L){

	const char* gname = "__HPClient";
	ELuna::registerClass<hpclient>(L, gname, ELuna::constructor<hpclient,void*,lua_State*>);
	ELuna::registerMethod<hpclient>(L, "Create_TcpClient", &hpclient::Create_TcpClient);
	//PULL------------------------------------------------------------------------------------------------------------
	ELuna::registerMethod<hpclient>(L, "Create_TcpPullClient", &hpclient::Create_TcpPullClient);
	ELuna::registerMethod<hpclient>(L, "Fetch", &hpclient::Fetch);
	//PACK------------------------------------------------------------------------------------------------------------
	ELuna::registerMethod<hpclient>(L, "Create_TcpPackClient", &hpclient::Create_TcpPackClient);
	ELuna::registerMethod<hpclient>(L, "SendPack", &hpclient::SendPack);
	ELuna::registerMethod<hpclient>(L, "SetMaxPackSize", &hpclient::SetMaxPackSize);
	ELuna::registerMethod<hpclient>(L, "SetPackHeaderFlag", &hpclient::SetPackHeaderFlag);
	ELuna::registerMethod<hpclient>(L, "GetMaxPackSize", &hpclient::GetMaxPackSize);
	ELuna::registerMethod<hpclient>(L, "GetPackHeaderFlag", &hpclient::GetPackHeaderFlag);
	//UDP------------------------------------------------------------------------------------------------------------
	ELuna::registerMethod<hpclient>(L, "Create_UdpCast", &hpclient::Create_UdpCast);
	ELuna::registerMethod<hpclient>(L, "SetMaxDatagramSize", &hpclient::SetMaxDatagramSize);
	ELuna::registerMethod<hpclient>(L, "GetMaxDatagramSize", &hpclient::GetMaxDatagramSize);
	ELuna::registerMethod<hpclient>(L, "SetReuseAddress", &hpclient::SetReuseAddress);
	ELuna::registerMethod<hpclient>(L, "IsReuseAddress", &hpclient::IsReuseAddress);
	ELuna::registerMethod<hpclient>(L, "SetCastMode", &hpclient::SetCastMode);
	ELuna::registerMethod<hpclient>(L, "GetCastMode", &hpclient::GetCastMode);
	ELuna::registerMethod<hpclient>(L, "SetMultiCastTtl", &hpclient::SetMultiCastTtl);
	ELuna::registerMethod<hpclient>(L, "GetMultiCastTtl", &hpclient::GetMultiCastTtl);
	ELuna::registerMethod<hpclient>(L, "SetMultiCastLoop", &hpclient::SetMultiCastLoop);
	ELuna::registerMethod<hpclient>(L, "IsMultiCastLoop", &hpclient::IsMultiCastLoop);
	ELuna::registerMethod<hpclient>(L, "GetRemoteAddress", &hpclient::GetRemoteAddress,2);//2
	//------------------------------------------------------------------------------------------------------------
	ELuna::registerMethod<hpclient>(L, "Start", &hpclient::Start);
	ELuna::registerMethod<hpclient>(L, "Stop", &hpclient::Stop);
	ELuna::registerMethod<hpclient>(L, "Send", &hpclient::Send);
	
	ELuna::registerMethod<hpclient>(L, "SetSocketBufferSize", &hpclient::SetSocketBufferSize);
	ELuna::registerMethod<hpclient>(L, "SetKeepAliveTime", &hpclient::SetKeepAliveTime);
	ELuna::registerMethod<hpclient>(L, "SetKeepAliveInterval", &hpclient::SetKeepAliveInterval);
	ELuna::registerMethod<hpclient>(L, "GetSocketBufferSize", &hpclient::GetSocketBufferSize);
	ELuna::registerMethod<hpclient>(L, "GetKeepAliveTime", &hpclient::GetKeepAliveTime);
	ELuna::registerMethod<hpclient>(L, "GetKeepAliveInterval", &hpclient::GetKeepAliveInterval);

	ELuna::registerMethod<hpclient>(L, "HasStarted", &hpclient::HasStarted);
	ELuna::registerMethod<hpclient>(L, "GetState", &hpclient::GetState);
	ELuna::registerMethod<hpclient>(L, "GetLastError", &hpclient::GetLastError);
	ELuna::registerMethod<hpclient>(L, "GetLastErrorDesc", &hpclient::GetLastErrorDesc);
	ELuna::registerMethod<hpclient>(L, "GetConnectionID", &hpclient::GetConnectionID);
	ELuna::registerMethod<hpclient>(L, "GetLocalAddress", &hpclient::GetLocalAddress,2);//2

	ELuna::registerMethod<hpclient>(L, "GetPendingDataLength", &hpclient::GetPendingDataLength);
	ELuna::registerMethod<hpclient>(L, "SetFreeBufferPoolSize", &hpclient::SetFreeBufferPoolSize);
	ELuna::registerMethod<hpclient>(L, "SetFreeBufferPoolHold", &hpclient::SetFreeBufferPoolHold);
	ELuna::registerMethod<hpclient>(L, "GetFreeBufferPoolSize", &hpclient::GetFreeBufferPoolSize);
	ELuna::registerMethod<hpclient>(L, "GetFreeBufferPoolHold", &hpclient::GetFreeBufferPoolHold);

	return 1;
}