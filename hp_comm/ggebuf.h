
#pragma once

#include "ELuna.h"
using namespace ELuna;

struct GGEHeader 
{
	unsigned short flag;
	unsigned short qty;
	unsigned int len;
};
class ggebuf
{
public:
	ggebuf(unsigned int L, unsigned short flag ,unsigned int len);
	~ggebuf(void);
	void Reset();
	bool AddNumber(int v);
	bool AddString(LuaString v);
	void Finish();
	unsigned int GetHeaderLen();
	unsigned int CheckHeader();
	LuaTable GetData();
	unsigned int GetPtr();
	unsigned int GetLen();
	void SetCode(char* psd,int len);
	void SetComp(bool v);
private:
	lua_State *		m_L;
	unsigned short m_flag;
	unsigned int m_bodylen;
	unsigned int m_buflen;
	unsigned int m_len;
	unsigned short m_qty;
	unsigned char* m_data;
	GGEHeader* m_head;
	int* m_wint;
	int* m_rint;
	bool m_lock;
	int m_i;
	unsigned char* m_code;
	int m_codelen;
	bool m_comp;
};

int REG_GGEBUF(lua_State * L);