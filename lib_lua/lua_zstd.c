
#include <lua.h>
#include <lauxlib.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#define ZSTD_STATIC_LINKING_ONLY
#include <zstd.h>

/*
 * ** compatibility with Lua 5.2
 * */
#if (LUA_VERSION_NUM >= 502)
#undef luaL_register
#define luaL_register(L,n,f) \
               { if ((n) == NULL) luaL_setfuncs(L,f,0); else luaL_newlib(L,f); }

#endif

#if (LUA_VERSION_NUM >= 503)
#undef luaL_optint
#define luaL_optint(L,n,d)  ((int)luaL_optinteger(L,(n),(d)))
#endif


/* ====================================================================== */

static int lzstd_compress(lua_State *L) {
    size_t buffInSize;
    const char *buffIn = luaL_checklstring(L, 1, &buffInSize);
    int cLevel = (int) luaL_optinteger(L, 2, 1);

	size_t ret;
    luaL_Buffer b;
	luaL_buffinit(L, &b);

	ZSTD_CStream* cstream = ZSTD_createCStream();
	if (cstream==NULL)
		luaL_error(L,"ZSTD_createCStream() error \n");

    size_t initResult = ZSTD_initCStream(cstream, cLevel);
	if (ZSTD_isError(initResult))
		luaL_error(L,"ZSTD_initCStream() error : %s \n",ZSTD_getErrorName(initResult));

	ZSTD_inBuffer input = { buffIn, buffInSize, 0 };
	ZSTD_outBuffer output;
	while (input.pos < input.size) {
		output.dst	= luaL_prepbuffer(&b);
		output.size = LUAL_BUFFERSIZE;
		output.pos	= 0;
		ret = ZSTD_compressStream(cstream, &output , &input);
		if (ZSTD_isError(ret)) 
			luaL_error(L,"ZSTD_compressStream() error : %s \n",ZSTD_getErrorName(ret));
		luaL_addsize(&b, output.pos);
	}
	output.dst	= luaL_prepbuffer(&b);
	output.size = LUAL_BUFFERSIZE;
	output.pos	= 0;
	ret = ZSTD_endStream(cstream, &output);
	luaL_addsize(&b, output.pos);
	ZSTD_freeCStream(cstream);

    luaL_pushresult(&b);
    return 1;
}

static int lzstd_decompress(lua_State *L)
{
    size_t buffInSize;
    const char *buffIn = luaL_checklstring(L, 1, &buffInSize);

    size_t ret;
    luaL_Buffer b;
    luaL_buffinit(L, &b);

	ZSTD_DStream* dstream = ZSTD_createDStream();
	if (dstream==NULL)
		luaL_error(L,"ZSTD_createCStream() error \n");

	size_t initResult = ZSTD_initDStream(dstream);
	if (ZSTD_isError(initResult))
		luaL_error(L,"ZSTD_initCStream() error : %s \n",ZSTD_getErrorName(initResult));

	ZSTD_inBuffer input = { buffIn, buffInSize, 0 };
	ZSTD_outBuffer output;
	while (input.pos < input.size) {
		output.dst	= luaL_prepbuffer(&b);
		output.size = LUAL_BUFFERSIZE;
		output.pos	= 0;
		ret = ZSTD_decompressStream(dstream, &output , &input);
		if (ZSTD_isError(ret)) 
			luaL_error(L,"ZSTD_decompressStream() error : %s \n",ZSTD_getErrorName(ret));
		luaL_addsize(&b, output.pos);
	}
	ZSTD_freeDStream(dstream);

    luaL_pushresult(&b);
    return 1;
}

static int lzstd_version(lua_State *L) {
	lua_pushnumber(L,ZSTD_versionNumber());
    return 1;
}

static int lzstd_uncompress(lua_State *L){
	void * dst = (void *)lua_tointeger(L,1);
	size_t dstLen = (size_t)lua_tointeger(L,2);
	void * cSrc = (void *)lua_tointeger(L,3);
	size_t cSize = (size_t)lua_tointeger(L,4);
	size_t dstCapacity = (size_t)ZSTD_findDecompressedSize(cSrc, cSize);//取原始尺寸
	if (dstCapacity==ZSTD_CONTENTSIZE_ERROR) {
		luaL_error(L,"数据错误。");
	} else if (dstCapacity==ZSTD_CONTENTSIZE_UNKNOWN) {
		//luaL_error(L,"原始长度未知。");
		dstCapacity = dstLen;
	}
	if (dstLen<dstCapacity)
		luaL_error(L,"解压内存不足。");
	size_t dSize = ZSTD_decompress(dst,dstCapacity,cSrc,cSize);
	//if (dSize != dstCapacity)
	//	luaL_error(L,"解压错误:%s。",ZSTD_getErrorName(dSize));
	//else
		lua_pushnumber(L,dSize);
	return 1;
}

static const luaL_Reg zstd_functions[] = {
	{ "uncompress", lzstd_uncompress },
    { "compress",   lzstd_compress   },
    { "decompress", lzstd_decompress },
    { "version",	lzstd_version    },
    { NULL,      NULL           }
};


LUALIB_API int luaopen_zstd(lua_State * L) {

	luaL_newlib(L,zstd_functions);

    return 1;
}
