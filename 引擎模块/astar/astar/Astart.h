#pragma once
#include <Windows.h>

//#define 不可通过 1
#define 可通过 0
#define 未知 0
#define 待检 1
#define 关闭 2

#define 分配内存(字节数)					VirtualAlloc(NULL,字节数,MEM_COMMIT|MEM_RESERVE,PAGE_READWRITE)
#define 释放内存(内存地址)					VirtualFree(内存地址,0,MEM_RELEASE)
#define 内存清零(内存地址,字节数)			SecureZeroMemory(内存地址,字节数)


typedef struct _节点
{
	BYTE	状态;
	DWORD	总距离;
	DWORD	距起点;
	DWORD	距终点;
	POINT	父坐标;
}节点结构;

typedef struct _地图
{
	BYTE*		数据;
	节点结构*	节点;
	DWORD		宽度;
	DWORD		高度;
	DWORD		大小;
}地图结构;

地图结构* WINAPI MapCreate(DWORD 宽度,DWORD 高度,BYTE* 点阵数据);
VOID WINAPI MapDestroy(地图结构* 地图);
地图结构* WINAPI MapCreate2(地图结构* 地图,DWORD 宽度,DWORD 高度,BYTE* 点阵数据);
VOID WINAPI MapDestroy2(地图结构*	地图);
BOOL WINAPI FindPath(地图结构* 地图数据,POINT* 起点坐标,POINT*	终点坐标,BOOL 搜索模式=0);
BOOL WINAPI NextPath(地图结构* 地图,POINT* 坐标);
