#include "lua.hpp"
#include "Astart.h"

static int astar_gc(lua_State *L)
{
	地图结构 *map = (地图结构*)luaL_checkudata(L, 1, "gge_astar");
	delete []map->数据;
	MapDestroy2(map);
	return 0;
}
static int astar_CheckPoint(lua_State *L)
{
	int x,y,w,h;
	地图结构 *map = (地图结构*)luaL_checkudata(L, 1, "gge_astar");
	x = luaL_checkinteger(L,2);
	y = luaL_checkinteger(L,3);
	w = map->宽度;h = map->高度;
	if (x>=0 && x<w && y>=0 && y<h)
	{
		lua_pushboolean(L,map->数据[w*y+x]);
	}else
		lua_pushboolean(L,1);

	return 1;
}
static int astar_SetPoint(lua_State *L)
{
	int x,y,w,h,v;
	地图结构 *map = (地图结构*)luaL_checkudata(L, 1, "gge_astar");
	x = luaL_checkinteger(L,2);
	y = luaL_checkinteger(L,3);
	v = lua_toboolean(L,4);
	w = map->宽度;h = map->高度;
	if (x>=0 && x<w && y>=0 && y<h)
	{
		map->数据[w*y+x] = v;
	}
	return 0;
}

static int astar_GetPath(lua_State *L)
{
	地图结构 *map = (地图结构*)luaL_checkudata(L, 1, "gge_astar");
	POINT nodeStart,nodeEnd,nodeCur;
	nodeStart.x = luaL_checkinteger(L, 2);
	nodeStart.y = luaL_checkinteger(L, 3);
	nodeEnd.x   = luaL_checkinteger(L, 4);
	nodeEnd.y   = luaL_checkinteger(L, 5);
	BOOL mode	= luaL_optinteger(L,6,0);
	nodeCur = nodeStart;

	lua_newtable(L);
	if (FindPath(map, &nodeStart, &nodeEnd,mode))
	{
		int i = 1;
		for( ;; )
		{
			if (NextPath(map,&nodeCur))
			{
				lua_newtable(L);
				lua_pushinteger(L, nodeCur.x);
				lua_setfield(L,-2,"x");
				lua_pushinteger(L, nodeCur.y);
				lua_setfield(L,-2,"y");
				lua_rawseti(L, -2, i++);
				if( nodeCur.x==nodeEnd.x && nodeCur.y==nodeEnd.y ) break;
			}else
				break;
		};
	}
	return 1;
}
static int astar_new(lua_State *L)
{
	int w,h,data,len;
	w = luaL_checkinteger(L, 1);
	h = luaL_checkinteger(L, 2);
	地图结构* map = (地图结构*)lua_newuserdata(L,sizeof(地图结构));
	MapCreate2(map,w,h,NULL);
	map->数据 = new BYTE[map->大小];
	memset(map->数据,0,map->大小);
	if (lua_isnumber(L,3) && lua_isnumber(L,4)){
		data = luaL_checkinteger(L, 3);
		len  = luaL_checkinteger(L, 4);
		if (len<=map->大小){
			memcpy(map->数据,(void*)data,len);
		}else
			luaL_error(L,"数据错误。");
	}
	luaL_getmetatable(L, "gge_astar");
	lua_setmetatable(L, -2);
	return 1;
}
extern "C"
__declspec(dllexport)int luaopen_astar(lua_State* L)
{
	luaL_Reg methods[] = {
		{"GetPath", astar_GetPath},
		{"CheckPoint", astar_CheckPoint},
		{"SetPoint", astar_SetPoint},
		{NULL, NULL},
	};
	luaL_newmetatable(L, "gge_astar");
	luaL_newlib(L, methods);
	lua_setfield(L, -2, "__index");
	lua_pushcfunction (L, astar_gc);
	lua_setfield (L, -2, "__gc");
	lua_pop(L,1);

	lua_pushcfunction(L,astar_new);
	return 1;
}

