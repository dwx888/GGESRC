#include <hge.h>
#include "lua.hpp"
HGE * pHGE=NULL;
hgeQuad dquad;
int texlen,pwidth;
RECT prect,srect,rect;
HWND phwnd,shwnd;
static int _Create(lua_State *L){
	if (pHGE)return 0;
	phwnd = (HWND)lua_tointeger(L,1);
	int width  = lua_tointeger(L,2);
	int height = lua_tointeger(L,3);
	int width1 = lua_tointeger(L,4);
	const char* title = luaL_optstring(L,5,"聊天窗口");
	pwidth = width;
	GetWindowRect(phwnd,&prect);//父窗口位置
	pHGE = hgeCreate(HGE_VERSION);
	pHGE->System_SetState(HGE_HWNDPARENT, phwnd);
	pHGE->System_SetState(HGE_SCREENWIDTH, width1);
	pHGE->System_SetState(HGE_SCREENHEIGHT, height);
	pHGE->System_SetState(HGE_TITLE, title);
	bool r = pHGE->System_Initiate();
	lua_pushboolean(L,r);
	dquad.tex = NULL;
	if (r)
	{
		shwnd = pHGE->System_GetState(HGE_HWND);
		GetWindowRect(shwnd,&srect);//子窗口位置
		//创建纹理
		texlen = width*height*4;
		dquad.tex = pHGE->Texture_Create(width,height);
		dquad.blend=BLEND_DEFAULT;

		for(int i=0; i<4; i++) {
			dquad.v[i].z=0.5f;
			dquad.v[i].col=0xFFFFFFFF;
		}

		dquad.v[0].tx=0.0f;
		dquad.v[0].ty=0.0f;
		dquad.v[1].tx=1.0f;
		dquad.v[1].ty=0.0f;
		dquad.v[2].tx=1.0f;
		dquad.v[2].ty=1.0f;
		dquad.v[3].tx=0.0f;
		dquad.v[3].ty=1.0f;

		dquad.v[0].x=0.0;//左上
		dquad.v[0].y=0.0;
		dquad.v[1].x=width;//右上
		dquad.v[1].y=0.0;
		dquad.v[2].x=width;//右下
		dquad.v[2].y=height;
		dquad.v[3].x=0.0;
		dquad.v[3].y=height;//左下
	}
	return 1;
}
static int _Release(lua_State *L){
	if (pHGE)
	{
		pHGE->Texture_Free(dquad.tex);
		pHGE->Release();
		pHGE = NULL;
	}

	return 0;
}
static int _Rende(lua_State *L){
	static MSG     msg;
	if (PeekMessage(&msg,NULL,0,0,PM_REMOVE)) {
		if (msg.message == WM_QUIT) {
			
		}
		// TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	if (pHGE)
	{
		pHGE->Gfx_BeginScene();
		pHGE->Gfx_Clear(0);
		pHGE->Gfx_RenderQuad(&dquad);
		pHGE->Gfx_EndScene();
	}


	return 0;
}
static int _Update(lua_State *L){
	if (pHGE)
	{
		hgeU32 * src = (hgeU32 *)lua_topointer(L,1);
		hgeU32 * dst = pHGE->Texture_Lock(dquad.tex,false);
		if (dst)
		{
			memcpy(dst,src,texlen);
			pHGE->Texture_Unlock(dquad.tex);
		}
		int width = pHGE->System_GetState(HGE_SCREENWIDTH);
		int height = pHGE->System_GetState(HGE_SCREENHEIGHT);
		GetWindowRect(phwnd,&rect);//父窗口位置是否变动
		if (rect.left != prect.left || rect.top != prect.top)
		{
			prect = rect;
			SetRect(&rect,0,0,width,height);
			AdjustWindowRect(&rect,348389376,false);
			MoveWindow(shwnd,prect.right,prect.top,
				rect.right-rect.left,srect.bottom-srect.top,TRUE);
			GetWindowRect(shwnd,&srect);//子窗口位置
		}
		GetWindowRect(shwnd,&rect);//子窗口位置是否变动
		if (rect.left != srect.left || rect.top != srect.top 
			|| rect.bottom != srect.bottom || width>pwidth)
		{
			if (width>pwidth)
				width = pwidth;
			SetRect(&rect,0,0,width,height);
			AdjustWindowRect(&rect,348389376,false);
			MoveWindow(shwnd,prect.right,prect.top,
				width>pwidth?pwidth:rect.right-rect.left,srect.bottom-srect.top,TRUE);
			GetWindowRect(shwnd,&srect);//子窗口位置
		}
		lua_pushnumber(L,width);
	}

	return 1;
}
static int _ClearQueue(lua_State *L){//清除键值状态
	if (pHGE)
	{
		pHGE->Input_ClearQueue();
	}
	
	return 0;
}
static int _GetMousePos(lua_State *L){
	if (pHGE)
	{
		static float x,y;
		pHGE->Input_GetMousePos(&x,&y);
		lua_pushnumber(L,x);
		lua_pushnumber(L,y);
		return 2;
	}
	return 0;
}
static int _GetMouseWheel(lua_State *L){
	if (pHGE)
	{
		lua_pushinteger(L,pHGE->Input_GetMouseWheel());
		return 1;
	}
	
	return 0;
}
static int _IsMouseOver(lua_State *L){
	if (pHGE)
	{
		lua_pushboolean(L,pHGE->Input_IsMouseOver());
		return 1;
	}
	return 0;
}
static int _KeyDown(lua_State *L){
	if (pHGE)
	{
		int key = lua_tointeger(L,1);
		lua_pushboolean(L,pHGE->Input_KeyDown(key));
		return 1;
	}
	return 0;
}
static int _KeyUp(lua_State *L){
	if (pHGE)
	{
		int key = lua_tointeger(L,1);
		lua_pushboolean(L,pHGE->Input_KeyUp(key));
		return 1;
	}
	return 0;
}
static int _GetKeyState(lua_State *L){
	if (pHGE)
	{
		int key = lua_tointeger(L,1);
		lua_pushboolean(L,pHGE->Input_GetKeyState(key));
		return 1;
	}
	return 0;
}
static int _GetKey(lua_State *L){
	if (pHGE)
	{
		lua_pushinteger(L,pHGE->Input_GetKey());
		return 1;
	}
	return 0;
}
static const luaL_Reg _lib[] = {
	{"Create", _Create},
	{"Rende", _Rende},
	{"Update", _Update},
	{"ClearQueue", _ClearQueue},
	{"Release", _Release},
	{"GetMousePos", _GetMousePos},
	{"GetMouseWheel", _GetMouseWheel},
	{"IsMouseOver", _IsMouseOver},
	{"KeyDown", _KeyDown},
	{"KeyUp", _KeyUp},
	{"GetKeyState", _GetKeyState},
	{"GetKey", _GetKey},
	{NULL, NULL},
};
extern "C"
__declspec(dllexport) int luaopen_gsub(lua_State* L) 
{
	luaL_register(L, "gsub", _lib);
	return 1;
}
