@echo off
cd

del /F/S/Q "*.sdf"
del /F/S/Q "*.ipch"

del /F/S/Q "ip2region/Release"
del /F/S/Q "ip2region/ip2region/Release"

del /F/S/Q "astar/Release"
del /F/S/Q "astar/astar/Release"

del /F/S/Q "gsub/Release"
del /F/S/Q "gsub/gsub/Release"

del /F/S/Q "lsqlite3/Release"
del /F/S/Q "lsqlite3/lsqlite3/Release"