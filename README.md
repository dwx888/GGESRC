### GGELUA的源码

[Galaxy2D官网](http://www.cnblogs.com/jianguhan/)
[GGELUA](https://gitee.com/baidwwy/GGELUA)

 **1.0b3**
- 更新 Galaxy2D到4.2.14
- 修复 黑屏问题
- 封装 动画
- 修复 线程
- 集成云风的 serialize
- 其它修复和优化

- 增加 扩展
 **1.0b2**
- 更新 Galaxy2D到4.2.13
- 完善 包围盒
- 增加 旧版的过渡接口
- 修复 不能传递cdata问题
- 优化 集成库

**1.0b1**
- 更新 LUAJIT到2.1 beta3
- 更新 Galaxy2D到4.2.12
- 封装 包围盒
- 封装 向量
- 封装 坐标
- 封装 文字
- 封装 精灵
- 封装 纹理
- 封装 音效
- 添加 线程

 **1.0b**
- 更新 LUAJIT到2.1 beta3
- 更新 Galaxy2D到4.2.12
- 封装 包围盒
- 封装 向量
- 封装 坐标
- 封装 文字
- 封装 精灵
- 封装 纹理
- 封装 音效
- 添加 线程

- 集成 zlib   库
- 集成 base64 库
- 集成 struct 库
